function shopWildbPluginIdentifyOrders(){
    
	var orders = []
    
	$('#order-list .order').each(function(index, elem){
        orders.push($(elem).data('order-id'))
    });
    
	$.post('?plugin=wildb&module=backend&action=identifyOrders', {orders: orders}, function(data){
        
		$.each(data.data.orders, function(index, elem){
            
			if($('#order-list .order[data-order-id="'+ elem.order_id +'"] .wb_order_info').length == 0) {
                $('#order-list li.order[data-order-id="' + elem.order_id + '"] .details').append('<div class="wb_order_info"><span>'+elem.value+'</span></div>')
                $('#order-list tr.order[data-order-id="' + elem.order_id + '"] td:eq(5)').append('<div class="wb_order_info"><span>'+elem.value+'</span></div>')
            }
        })
    });
}

function shopWildbCopyId(e){
	
	let $elem = $(e.target).parent().parent()
	let wb_id = $elem.data('id')
	
	if(navigator.clipboard !== undefined) {
		navigator.clipboard.writeText(wb_id)
	} else {
		
		let $input = $("<input>");
		$input.appendTo("body").val(wb_id).select()
		document.execCommand("copy")
		$input.remove()
	}
	
	$elem.find('.js-wb-tooltip').text("Скопировано")
}

$(document).ready(function(){
	$('#maincontent').one('append_order_list', '#order-list', function (e) {
		shopWildbOrderInfoSectionInit()
		shopWildbPluginIdentifyOrders()
	})
});

function shopWildbOrderInfoSectionInit(){
	$('#s-order').on('click', '.js-wb-order-complete-button', shopWildbOpenSuppliesManager)
	$('#s-order').on('click', '.js-wb-trbx-complete-button', shopWildbOpenTrbxSelectForm)
	$('#s-order').on('click', '.js-wb-trbx-remove-button', shopWildbRemoveOrderFromTrbx)
	$('#s-order').on('click', '.js-wb-order-sticker-button', shopWildbGetOrderSticker)
	$('#s-order').on('click', '.js-wb-supply-sticker-button', shopWildbOrderSupplySticker)
	$('#s-order').on('click', '.js-wb-supply-complete-button', shopWildbOrdersCompleteSupply)
	$('#s-order').on('click', '.js-wb-trbx-sticker-button', shopWildbPrintTrbxSticker)
	$('#s-order').on('click', '.js-wb-trbx-select-check', shopWildbSelectTrbx)
	$('#s-order').on('click', '.js-wb-copy-wb-id', shopWildbCopyId)
	$('#s-order').on('mouseout', '.js-wb-copy-wb-id', () => {$('.js-wb-tooltip').text("Скопировать")})
}

function shopWildbOrderSupplySticker(e){
	
	e.preventDefault()
	
	window.open("?plugin=wildb&module=assembly&action=getSupplySticker&supply_id=" + $('.js-wb-order-supply-check:checked').data('supply-id'), '_blank')
}

function shopWildbSelectTrbx(e){
	$(".js-wb-trbx-select-check").not(e.target).attr("checked", false)
	$(".js-wb-trbx-selector-move").attr("disabled", $(".js-wb-trbx-select-check:checked").length === 0)
	$(".js-wb-trbx-selector-remove").attr("disabled", $(".js-wb-trbx-select-check:checked").length === 0)
}

function shopWildbAddOrdersTrbx($trbxSelector){
	
	let supply_id = $('.js-wb-order-supply-check:checked').data('supply-id');
	let trbx_id = $trbxSelector.find('.js-wb-trbx-select-check:checked').data('trbx-id');
	let $checked_items = $(".js-wb-order-item-check:checked")
	let orders = [];
	
	for (let i = 0; i < $checked_items.length; i++){
		orders.push($($checked_items[i]).data("order-id"))
	}
	$.post('?plugin=wildb&module=backend&action=addOrdersTrbx',{orders: orders, trbx_id: trbx_id, supply_id: supply_id}, (result) => {
		if(result.data.result){
			$trbxSelector.trigger("close")
			$trbxSelector.remove()
			shopWildbLoadInfoSection()
		}
	})
}

function shopWildbLoadInfoSection(){
	$.get('?plugin=wildb&module=backend&action=getOrderInfoSection&order_id=' + $('#order-list .order.selected').data('order-id'), (result) => {
		if(result.data.result){
			$('.wb-order-section').html($(result.data.order_info).html())
		}else{
			$('.wb-order-section').html('<div class="wb-assembly-error">Ошибка загрузки информации о заказе</div>')
		}
	})
}

function shopWildbOrdersCompleteSupply(e){
	$.post('?plugin=wildb&module=backend&action=completeSupply',{supply_id: $(".js-wb-order-supply-check:checked").data('supply-id')}, shopWildbLoadInfoSection)
}

function shopWildbOpenTrbxSelectForm(e){
	
	e.preventDefault()
	
	$.get('?plugin=wildb&module=assembly&action=trbxSelectForm&supply_id='+$('.js-wb-order-supply-check:checked').data('supply-id'),(result) => {
		
		let $trbxSelector = $(result).waDialog()
		
		$trbxSelector.find(".dialog-window").css('padding','50px 0 55px').css('top','10%').css('bottom','10%')
		
		$trbxSelector.find(".dialog-background, .js-wb-trbx-selector-close").click(() => {
			$trbxSelector.trigger("close")
			$trbxSelector.remove()
		})
		
		$trbxSelector.find(".js-wb-trbx-selector-move").click(() => {
			shopWildbAddOrdersTrbx($trbxSelector)
		})
		
		$trbxSelector.find(".js-wb-trbx-selector-add").click(() => {
			shopWildbTrbxAdd($trbxSelector)
		})
		
		$trbxSelector.find(".js-wb-trbx-selector-remove").click(shopWildbTrbxRemove)
		
		$trbxSelector.find(".dialog-buttons").remove()
		$trbxSelector.find('.js-wb-trbx-select-check').click(shopWildbSelectTrbx)
	})
}

function shopWildbRemoveOrderFromTrbx(e){
	
	e.preventDefault()
	
	let $checked_items = $(".js-wb-order-item-check:checked")
	let orders = [];
	
	for (let i = 0; i < $checked_items.length; i++){
		orders.push($($checked_items[i]).data("order-id"))
	}
	
	$.post('?plugin=wildb&module=backend&action=removeFromTrbx',{orders: orders}, (result) => {
		if(result.data.result){
			shopWildbLoadInfoSection()
		}
	})
}

function shopWildbPrintTrbxSticker(e){
	
	e.preventDefault()
	
	let trbx_id = $('.js-wb-order-trbx-check:checked').data('trbx-id')
	let supply_id = $('.js-wb-order-supply-check:checked').data('supply-id')
	
	window.open("?plugin=wildb&module=assembly&action=getTrbxSticker&trbx_id=" + trbx_id + "&supply_id=" + supply_id, '_blank')
}

function shopWildbTrbxAdd($trbxSelector){
	
	$.post('?plugin=wildb&module=backend&action=createTrbx',{supply_id: $('[data-supply-id]').data('supply-id')}, (result) => {
		if(result.data.result){
			for(let i = 0; i < result.data.trbx.length; i++){
				
				let $row = $('<div class="table-emulate__row"></div>').appendTo($trbxSelector.find('.js-wb-trbx-items').find('.table-emulate'))
				let $cell = $('<div class="table-emulate__cell"></div>').appendTo($row)
				$('<input type="checkbox" class="wb-item-checkbox js-wb-trbx-select-check" data-trbx-id="'+result.data.trbx[i]+'">').click(shopWildbSelectTrbx).appendTo($cell)
				$row.append('<div class="table-emulate__cell"><span class="table-emulate__field-value">'+result.data.trbx[i]+'</span></div>')
			}
		}
	})
}

function shopWildbTrbxRemove(){
	
	let data = {
		trbx_id: $('.js-wb-trbx-select-check:checked').data('trbx-id'),
		supply_id: $('.js-wb-order-supply-check:checked').data('supply-id'),
	}
	
	$.post('?plugin=wildb&module=backend&action=deleteTrbx', data, (result) => {
		$('.js-wb-trbx-select-check:checked').parent().parent().remove()
	})
}

function shopWildbGetOrderSticker(e){
	
	e.preventDefault()
	
	let orders = []
	
	for(let i = 0; i < $('.js-wb-item-check:checked').length; i++){
		orders.push($('.js-wb-item-check:checked').eq(i).data('order-id'))
	}
	
	window.open("?plugin=wildb&module=assembly&action=getOrderSticker&order_id=" + $('.js-wb-order-item-check:checked').data('order-id'), '_blank')
}
