$(document).ready(function(){
	
	$('.js-wb-menu-item').on('click', function(){
		
		let item = $(this).attr('data-item');
		
		if(typeof timer_id !== 'undefined') clearTimeout(timer_id);
		
		shopWildbShowMenu(item);
		
		$(this).addClass('wb-menu__item--active');
		
		if($(this).parent().hasClass('wb-menu__subitems')){
			
			let $d_box = $(this).parent().parent();
			
			$d_box.parent().addClass('wb-menu__item--active');
			$d_box.mouseout(function (){setTimeout(function (){$d_box.removeAttr('style').off('mouseout')},300)}).hide();
		}
	});
	
	$('.wb-content').html('<div class="wb-content__content wb-content__content--loading"><i class="icon16 loading"></i></div>').load("?plugin=wildb&module=settings&action=api", function() {
		shopWildbInitBlock();
	});
});

function shopWildbInitBlock(){
	
	$('.js-select-stocks').change(function(){shopWildbShowStocksId(this)});
	shopWildbShowStocksId('.js-select-stocks');
	
	$('.js-wb-save-settings').change(function(){shopWildbSaveSettings()});
	
	$('.js-wb-input-cron').focus(function(){$(this).select()});
	
	$('.js-wb-add-stock-rule').click(function () {
		
		let $rule = $("<div class='wb-stock-rule js-wb-stock-rule'></div>").appendTo('.js-wb-stock-rules');
		$("<span>Если наличие</span>").appendTo($rule);
		
		let $rule_operand = $("<select name='operand' class='wb-content__select_stock_rule js-wb-save-settings'></select>").change(function(){shopWildbSaveSettings()}).appendTo($rule);;
		$rule_operand.append("<option value='>'>Больше</option>");
		$rule_operand.append("<option value='<'>Меньше</option>");
		$rule_operand.append("<option value='='>Равно</option>");
		
		$("<input type='number' min='0' name='right_argument' class='wb-content__input--small js-wb-save-settings'>").change(function(){shopWildbSaveSettings()}).appendTo($rule);
		$("<span>, то отправить</span>").appendTo($rule);
		$("<input type='number' min='0' name='result' class='wb-content__input--small js-wb-save-settings'>").change(function(){shopWildbSaveSettings()}).appendTo($rule);
		$("<a class='js-wb-delete-stock-rule'><i class='icon16 delete'></i></a>").click(function(){shopWildbRemoveStockRule(this)}).appendTo($rule);
		
		shopWildbSaveSettings();
	});
	
	$('.js-wb-delete-stock-rule').click(function(){shopWildbRemoveStockRule(this)});
	
	$('.js-wb-add-price-rule').click(function () {
		
		let rule_type = $(this).attr('data-price-rule');
		
		let rule_num = $("." + rule_type).find(".js-wb-price-rule").length;
		
		let $rule = $("<div class='wb-price-rule js-wb-price-rule'></div>").appendTo("." + rule_type);
		
		let $col1 = $("<div class='wb-price-rule__column wb-price-rule__column--sortable'></div>").appendTo($rule);
		let $row = $("<div class='wb-price-rule__row wb-price-rule__row--icon'></div>").appendTo($col1);
		$("<a title='Переместить правило'><i class='icon16 sort stock-rows-handle'></i></a>").appendTo($row);
		
		let $col2 = $("<div class='wb-price-rule__column js-rule-connector'></div>").appendTo($rule);
		$row = $("<div class='wb-price-rule__row'></div>").appendTo($col2);
		let $connector = $("<select name='connector' class='js-wb-save-settings'></select>").change(function(){shopWildbSaveSettings()}).appendTo($row);
		$connector.append("<option value='or'>ИЛИ</option>");
		$connector.append("<option value='and'>И</option>");
		$row = $("<div class='wb-price-rule__row wb-price-rule__row--icon'>").appendTo($col2);
		let $add_condition_button = $("<a class='js-wb-add-price-condition' title='Добавить условие'><i class='icon16 add'></i></a>").appendTo($row);
		
		let $col3 = $("<div class='wb-price-rule__column js-rule-conditions'></div>").appendTo($rule);
		
		$("<label for='conditions'"+rule_num+" class='wb-price-rule__label'>Если</label>").appendTo($col3);
		shopWildbAddPriceRuleCondition($col3);
		$add_condition_button.click(function(){shopWildbAddPriceRuleCondition($col3)});
		
		let $col4 = $("<div class='wb-price-rule__column'></div>").appendTo($rule);
		$("<label for='rule'"+rule_num+" class='wb-price-rule__label'>Тогда</label>").appendTo($col4);
		$row = $("<div class='wb-price-rule__row' data-markup='markup1'></div>").appendTo($col4);
			
		if(rule_type == 'js-wb-old-price-rules'){
			$("<span>+</span>").appendTo($row);
			$("<input type='number' min='0' name='value' class='wb-content__input--small js-wb-save-settings'>").change(function(){shopWildbSaveSettings()}).appendTo($row);
			let $type = $("<select name='type' class='js-wb-save-settings'></select>").change(function(){shopWildbSaveSettings()}).appendTo($row);
			$type.append("<option value='percent'>%</option>");
			$type.append("<option value='fix'>руб.</option>");
			$row.clone(true).attr('data-markup','markup2').appendTo($col4);
		}else if(rule_type == 'js-wb-price-rules'){
			$("<span>-</span>").appendTo($row);
			$("<input type='number' min='0' name='discount' class='wb-content__input--small js-wb-save-settings'>").change(function(){shopWildbSaveSettings()}).appendTo($row);
			$("<span>%</span>").appendTo($row);
		}
		
		let $col5 = $("<div class='wb-price-rule__column wb-delete-price-rule'></div>").appendTo($rule);
		$row = $("<div class='wb-price-rule__row'></div>").appendTo($col5);
		$("<a class='js-wb-delete-price-rule'><i class='icon16 delete'></i></a>").click(function(){shopWildbRemovePriceRule(this)}).appendTo($row);
		
		$("." + rule_type).sortable({ update: function(event, ui){ shopWildbSaveSettings()}});
		
		shopWildbSaveSettings();
	});
	
	$('.js-wb-add-price-condition').click(function () {
		let $rule_conditions = $(this).parent().parent().parent().find(".js-rule-conditions");
		shopWildbAddPriceRuleCondition($rule_conditions);
	});
	
	$('.js-wb-delete-price-rule').click(function(){shopWildbRemovePriceRule(this)});
	$('.js-wb-delete-price-condition').click(function(){shopWildbRemovePriceCondition(this)});
	
	$(".js-wb-render-inputs").change(function () {
		shopWildbRenderConditionsInputs($(this));
	});
	
	$('#wb-products-1').click(function(){$('.js-select-products-type, .js-select-products-categories, .js-select-products-sets').hide(300)});
	
	$('#wb-products-2').click(function(){
		
		let $type = $('.js-select-products-type');
		
		$type.show(300);
		
		if($type.find("input[name='shop_products_type']:checked").val() == 1) {
			$('.js-select-products-categories').show(300);
		}else if($type.find("input[name='shop_products_type']:checked").val() == 2){
			$('.js-select-products-sets').show(300);
		}
	});
	
	$('#wb-products-type-1').click(function(){
		$('.js-select-products-categories').show(300);
		$('.js-select-products-sets').hide(300);
	});
	
	$('#wb-products-type-2').click(function(){
		$('.js-select-products-categories').hide(300);
		$('.js-select-products-sets').show(300);
	});
	
	$('.js-wb-expand').click(function(e){
		e.preventDefault();
		shopWildbExpandTreeBranch($(this));
	});
	
	$('.js-wb-select').click(function(e){
		
		e.preventDefault();
		
		shopWildbSelectTreeBranch($(this))
		
		values = [];
		
		$(".js-wb-tree li").find(".js-wb-select.fa-circle-check").each(function(i,e){
			values.push($(e).parent().attr("data-category-id"));
		});
		
		$.ajax({
			type: 'post',
			url: '?module=plugins&id=wildb&action=save',
			data: {
				shop_wildb: {shop_products_categories:values},
			}
		});
	});
	
	$(".js-wb-open-rules-categories").click(function () {
		shopWildbRenderTree($(this));
	});
	
	$(".select2_multi").select2({width: "100%",	multiple: true,});
	
	$('.js-wb-update').click(function() {
		
		let type = $(this).data('type');
		let $message = $('.js-wb-update-result');
		
		$message.html('<i class="icon16 loading"></i> Идет выгрузка...');
		
		$.ajax({
			type: 'post',
			url: '?plugin=wildb&action=sendApi',
			data: {type: type},
			success: function (data) {
				if (data && data.data.status) {
					$message.html('Успешно выгружено');
				} else if (data && data.data.error) {
					$message.html('При выгрузке произошла ошибка: ' + data.data.error);
				} else {
					$message.html('При выгрузке произошла ошибка: Server error');
				}
			},
			error: function (jqXHR, textStatus, error) {
				$message.html('При выгрузке произошла ошибка: ' + textStatus + ' - ' + error);
			}
		});
	});
	
	let wildb_options = { update: function(event, ui){ shopWildbSaveSettings()}};
	
	$(".js-wb-old-price-rules").sortable(wildb_options);
	$(".js-wb-price-rules").sortable(wildb_options);
	
	$('.js-api-task-switch').click(function () {
		shopWildbShowApiLog($(this).prev().val());
	})
	
	$('.js-api-change-page').click(function (e) {
		e.preventDefault()
		shopWildbShowApiLog($(this).data('task'), $(this).data('page'))
	})
}

function shopWildbExpandTreeBranch($branch){
	
	if($branch.hasClass('fa-caret-down')) {
		$branch.removeClass('fa-caret-down').addClass('fa-caret-right');
		$branch.parent().find(".js-wb-tree").eq(0).slideUp(300);
	} else if($branch.hasClass('fa-caret-right')) {
		$branch.removeClass('fa-caret-right').addClass('fa-caret-down');
		$branch.parent().find(".js-wb-tree").eq(0).slideDown(300);
	}
}

function shopWildbSelectTreeBranch($branch){
	
	if($branch.hasClass('fa-circle')) {
		
		$branch.removeClass('fa-regular fa-circle').addClass('fa-solid fa-circle-check');
		$subbranchs = $branch.parent().find(".js-wb-tree li");
		$subbranchs.find(".js-wb-select").removeClass('fa-regular fa-circle').addClass('fa-solid fa-circle-check');
		
	} else if($branch.hasClass('fa-circle-check')) {
		$branch.removeClass('fa-solid fa-circle-check').addClass('fa-regular fa-circle');
		$subbranchs = $branch.parent().find(".js-wb-tree li");
		$subbranchs.find(".js-wb-select").removeClass('fa-solid fa-circle-check').addClass('fa-regular fa-circle');
	}
}

function shopWildbRenderTree($button){
	
	let $input = $button.parent().find("input[name='value']");
	let condition_categories = $input.val().split(',');
	let url = "?plugin=wildb&module=settings&action=categoriesTree&categories=" + condition_categories;

	$(".js-wb-tree-overlay").slideDown(100);
	$(".js-wb-popup-rules-categories").html('<i class="icon16 loading"></i>').load(url,function () {
		
		$('body').css('overflow-y','hidden');
		
		$(".js-wb-close-tree").click(function () {
			
			$('body').css('overflow-y','auto');
			
			shopWildbSaveSettings();
			
			$(".js-wb-popup-rules-categories").slideUp(300);
			$(".js-wb-tree-overlay").slideUp(500);
		});
		
		$('.js-wb-expand').click(function(e){
			e.preventDefault();
			shopWildbExpandTreeBranch($(this));
		});
		
		$(".js-wb-clear-tree").click(function () {
			
			$(".js-wb-tree li").find(".js-wb-select.fa-circle-check").each(function(i,e){
				$(e).removeClass("fa-solid").removeClass("fa-circle-check").addClass("fa-regular").addClass("fa-circle");
			});
			
			$input.val('');
		});
		
		$('.js-wb-select').click(function(e){
			
			e.preventDefault();
			
			shopWildbSelectTreeBranch($(this));
			
			values = [];
			
			$(".js-wb-tree li").find(".js-wb-select.fa-circle-check").each(function(i,e){
				values.push($(e).parent().attr("data-category-id"));
			});
			
			$input.val(values.join(','));
		});
	}).slideDown(300);
}

function shopWildbAddPriceRuleCondition($rule_conditions){
	
	if(!$rule_conditions.hasClass('js-rule-conditions')) return false;
	
	let $row = $("<div class='wb-price-rule__row js-rule-condition'></div>").appendTo($rule_conditions);
	let $feature = $("<select name='feature' class='js-wb-save-settings js-wb-render-inputs'>").change(function(){
		shopWildbRenderConditionsInputs($(this));
		shopWildbSaveSettings();
	}).appendTo($row);
	$feature.append("<option value='price'>Цена</option>");
	$feature.append("<option value='weight'>Вес</option>");
	$feature.append("<option value='category_id'>Категория</option>");
	
	let $operand = $("<select name='operand' class='wb-content__select_stock_rule js-wb-save-settings'></select>").change(function(){shopWildbSaveSettings()}).appendTo($row);
	$operand.append("<option value='>'>Больше</option>");
	$operand.append("<option value='<'>Меньше</option>");
	$operand.append("<option value='='>Равно</option>");
	
	$("<input type='number' min='0' name='value' class='wb-content__input--small js-wb-save-settings'>").change(function(){shopWildbSaveSettings()}).appendTo($row);
	
	let $button = $("<div class='wb-price-rule_icon'></div>").appendTo($row);
	$("<a class='js-wb-delete-price-condition' title='Удалить условие'><i class='icon16 delete'></i></a>").click(function(){shopWildbRemovePriceCondition(this)}).appendTo($button);
	
	if($rule_conditions.children().length > 2 && $rule_conditions.parent().find('.wb-price-rule__row--conditions-icon').length === 0){
		$("<div class='wb-price-rule__row wb-price-rule__row--conditions-icon'></div>").appendTo($rule_conditions.parent().find(".js-rule-connector"));
	}
	
	shopWildbSaveSettings();
}

function shopWildbRemovePriceCondition(ob){
	
	let $price_condition = $(ob).parent().parent();
	let $price_rule = $price_condition.parent().parent();
	
	$price_condition.remove();
	
	if($price_rule.find(".js-rule-condition").length === 1){
		$price_rule.find(".wb-price-rule__row--conditions-icon").remove();
	}else if($price_rule.find(".js-rule-condition").length === 0) {
		$price_rule.remove();
	}
	
	shopWildbSaveSettings();
}

function shopWildbRemovePriceRule(ob){
	$(ob).parent().parent().parent().remove();
	shopWildbSaveSettings();
}

function shopWildbRemoveStockRule(ob){
	$(ob).parent().remove();
	shopWildbSaveSettings();
}

function shopWildbRenderConditionsInputs($select){
	
	let mode = $select.find(':selected').val();
	let $rule_condition = $select.parent();
	let $button_remove_condition = $rule_condition.find('.wb-price-rule_icon');
	
	$rule_condition.find('input').remove();
	$rule_condition.find("select[name='operand']").remove();
	$rule_condition.find('button').remove();
	
	if(mode === 'category_id'){
		$("<input type='text' name='value' style='display:none'>").insertBefore($button_remove_condition);
		let $button = $("<button class='button button--wb button--category_id js-wb-open-rules-categories'>Выбрать</button>");
		$button.click(function(){shopWildbRenderTree($(this))}).insertBefore($button_remove_condition);
	}else{
		let $operand = $("<select name='operand' class='wb-content__select_stock_rule js-wb-save-settings'></select>")
		  .change(function(){shopWildbSaveSettings()}).insertBefore($button_remove_condition);
		$operand.append("<option value='>'>Больше</option>");
		$operand.append("<option value='<'>Меньше</option>");
		$operand.append("<option value='='>Равно</option>");
		
		$("<input type='number' min='0' name='value' class='wb-content__input--small js-wb-save-settings'>")
		  .change(function(){shopWildbSaveSettings()}).insertBefore($button_remove_condition);
	}
}

function shopWildbSaveSettings(){
	
	let settings = {};
	
	if($(this).hasClass('js-select-stocks')){
		shopWildbShowStocksId(this);
	}
	
	$('.js-wb-save-settings').each(function (i,e){
		
		let name = $(e).attr('name');
		
		if(name == 'operand' || name == 'right_argument' || name == 'result' || name == 'connector' || name == 'feature' || name == 'value' || name == 'type'){
			return;
		}
		
		if($(e).attr('type') === 'checkbox'){
			settings[$(e).attr('name')] = $(e).is(':checked') ? 1 : 0;
		} else if($(e).attr('type') === 'radio'){
			if($(e).is(':checked')){
				settings[$(e).attr('name')] = $(e).val();
			}
		} else {
			settings[$(e).attr('name')] = $(e).val();
		}
	});
	
	stock_rules = [];
	
	$('.js-wb-stock-rule').each(function (i,e) {
		stock_rules.push({
			operand: $(e).find("[name='operand']").find(':selected').val(),
			right_argument: $(e).find("[name='right_argument']").val(),
			result: $(e).find("[name='result']").val(),
		});
	});
	
	if($('.js-wb-stock-rules').length === 1){
		settings['stock_rules'] = stock_rules.length === 0 ? stock_rules.toString() : stock_rules;
	}
		
	wb_price_rules = {wb_price_rules:[],wb_old_price_rules:[]};
	
	$('.js-wb-price-rule').each(function (i,e) {
		
		let rule_type = $(e).parent().attr('data-price-rules');
		let rule_connector = $(e).find("[name='connector']").find(':selected').val();
		
		rule_conditions = {};
		rule_conditions[rule_connector] = [];
		
		$(e).find(".js-rule-condition").each(function (ii,ee) {
			
			condition_value = $(ee).find("input[name='value']").val();
			
			condition = {
				feature: $(ee).find("[name='feature']").find(':selected').val(),
				value: condition_value ? condition_value : '',
			};
			
			if(condition.feature !== 'category_id'){
				operand = $(ee).find("[name='operand']").find(':selected').val(),
				condition.operand = operand ? operand : '';
			}
			
			rule_conditions[rule_connector].push(condition);
		});
		
		rule = {conditions: rule_conditions};
		
		if($(e).find("[data-markup='markup1']").length > 0 && $(e).find("[data-markup='markup2']").length > 0){
			rule.markup1 = {
				type: $(e).find("[data-markup='markup1']").find("[name='type']").find(':selected').val(),
				value: $(e).find("[data-markup='markup1']").find("[name='value']").val(),
			};
			rule.markup2 = {
				type: $(e).find("[data-markup='markup2']").find("[name='type']").find(':selected').val(),
				value: $(e).find("[data-markup='markup2']").find("[name='value']").val(),
			};
		}else if($(e).find("input[name='discount']").length > 0){
			rule.discount = $(e).find("input[name='discount']").val();
		}
		
		wb_price_rules[rule_type].push(rule);
	});
	
	if($('.js-wb-price-rules').length === 1) {
		settings['wb_price_rules'] = wb_price_rules['wb_price_rules'].length === 0 ? wb_price_rules['wb_price_rules'].toString() : wb_price_rules['wb_price_rules'];
		settings['wb_old_price_rules'] = wb_price_rules['wb_old_price_rules'].length === 0 ? wb_price_rules['wb_old_price_rules'].toString() : wb_price_rules['wb_old_price_rules'];
	}
	
	$.ajax({
		type: 'post',
		dateType: 'json',
		url: '?module=plugins&id=wildb&action=save',
		data: {
			shop_wildb: settings
		}
	});
}

function shopWildbShowStocksId(ob){
	if($(ob).find(':selected').val() == 1){
		$('.js-field-stocks-id').hide();
		$('.js-select-stocks-id').find(':selected').removeAttr('selected');
	}else if ($(ob).find(':selected').val() == 2) {
		$('.js-field-stocks-id').show();
	}
}

function shopWildbShowMenu(item){

	$('.wb-menu__item--active').removeClass('wb-menu__item--active');
	$('.wb-content').html('<div class="wb-content__content wb-content__content--loading"><i class="icon16 loading"></i></div>').load("?plugin=wildb&module=settings&action=" + item, function () {
		shopWildbInitBlock();
	});
}

function shopWildbShowApiLog(task, page){
	
	let load = "?plugin=wildb&module=settings&action=apiLog&task=" + task;
	
	if(page) load += "&page=" + page;
	
	$('.js-api-log-rows').html('<div class="wb-content__content wb-content__content--loading"><i class="icon16 loading"></i></div>').load(load, function () {
		$('.js-api-change-page').click(function (e) {
			e.preventDefault()
			shopWildbShowApiLog($(this).data('task'), $(this).data('page'))
		})
	});
}
