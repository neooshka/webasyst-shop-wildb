$('.js-wb-export').click(function () {
		
	let $button = $(this);
	let $action = $button.data('action');
	let $message = $("<div class='wb-content__setting'></div>");
	let $message_place = $button.parent().parent().find('.wb-content__field');
	$message_place.html("");
	
	$message.html('<i class="icon16 loading"></i> Создается задание на экспорт...').appendTo($message_place);

	$.ajax({
		type: 'post',
		url: '?plugin=wildb&action=export&data='+$action,
		success: function (data) {
			if (data && data.data.status) {
				
				$button.attr("disabled", true);
				$('.js-wb-task').not($button.parent().parent()).slideUp(500);
				$message.html("Задание на экспорт запущено").appendTo($message_place);
				
				setTimeout(function () {
					
					shopWildbShowMenu('exportCards')
					
					let timer = setInterval(function () {
						$.ajax({
							type: 'post',
							url: '?plugin=wildb&action=checkExport&task='+data.data.task,
							success: function (data) {
								if (data && data.data.status) {
									shopWildbShowMenu('exportCards')
									clearInterval(timer)
								}
							},
						})
					},60*1000)
				},3000)
			} else if (data && data.data.error) {
				$message.html("При запуске задания произошла ошибка: <b>" + data.data.error + "</b>").appendTo($message_place);
			} else {
				$message.html("При запуске задания произошла ошибка: <b>server error</b>").appendTo($message_place);
			}
		},
		error: function (jqXHR, textStatus, error) {
			$message.html("При запуске задания произошла ошибка: <b>" + textStatus + " - " + error + "</b>").appendTo($message_place);
		}
	});
});

$('.js-wb-delete-task').click(function () {
	
	let $task_row = $(this).parent().parent()
	let task_id = $(this).data('task')
	let $message = $task_row.parent().find('.wb-content__description')
	
	$.ajax({
		type: 'post',
		url: '?plugin=wildb&action=export&data=cansel&task=' + task_id,
		success: function (data) {
			if (data && data.data.status) {
				$task_row.remove()
				shopWildbShowMenu('exportCards')
			} else if (data && data.data.error) {
				$message.html("При отмене задания произошла ошибка: <b>" + data.data.error + "</b>")
			} else {
				$message.html("При отмене задания произошла ошибка: <b>server error</b>")
			}
		},
		error: function (jqXHR, textStatus, error) {
			$message.html("При отмене задания произошла ошибка: <b>" + textStatus + " - " + error + "</b>")
		}
	})
})