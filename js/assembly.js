function shopWildbOpenAssemblies(e) {
	e.preventDefault()
	$.get('?plugin=wildb&module=assembly',(result) => {
		
		$("body").css("overflow", "hidden")
		
		let $waDialog = $(result).waDialog()
		
		$waDialog.find(".dialog-background, .js-wb-assembly-close").click(() => {
			$waDialog.find('[data-accordion]').accordion("activate", -1)
			$waDialog.find('[data-accordion]').accordion("destroy")
			$waDialog.trigger("close")
			$waDialog.remove()
			$("body").css("overflow", "auto")
		})
		
		$waDialog.find(".dialog-window").css('padding','50px 0 55px').css('top','10%').css('bottom','10%')
		$waDialog.find(".dialog-buttons").remove()
		$waDialog.find(".wb-assembly-menu__tab:first").addClass("wb-assembly-menu__tab--selected")
		$waDialog.find(".js-wb-item-check-all").click(shopWildbSelectAllTasks)
		$waDialog.find(".js-wb-item-check").click(shopWildbSelectTask)
		$waDialog.find(".js-wb-assembly-tab").click(shopWildbChangeMenu)
	})
}

function shopWildbShowAssembly(result){
	$(".js-wb-assembly-items").html(result)
	$(".js-wb-item-check-all").click(shopWildbSelectAllTasks)
	$(".js-wb-item-check").click(shopWildbSelectTask)
	initTabs()
	$(".js-wb-supply-close").click(shopWildbCloseSupply)
	$(".js-wb-supply-open").click(shopWildbOpenSupply)
	$(".js-wb-supply-check").click(shopWildbSelectSupply)
	shopWildbUpdateMenuPanel()
	$(".js-wb-assembly-custom-button").remove()
	createAccordions()
	
	$('.js-wb-tab').click(function (e) {
		
		$('.js-wb-item-check:checked').attr('checked', false)
		$(".js-wb-assembly-custom-button").remove()
		
		if ($(result).find('[data-type]').data('type') === 'confirm') {
			shopWildbAddTrbxButton(e)
		}
	})
	
	$('.wb-spoiler-item__header').click(shopWildbTrbxButtonsEvent)
}

function shopWildbTrbxButtonsEvent(e){
	shopWildbAddButtonToBottomPanel({
		action: 'deleteTrbx',
		name: 'Удалить короб',
		need: $(e.target).hasClass('open'),
		onClick: shopWildbDeleteTrbx,
	})
	
	shopWildbAddButtonToBottomPanel({
		action: 'getTrbxSticker',
		name: 'Этикетка короба',
		need: $(e.target).hasClass('open') && $('[data-accordion]').find('.js-wb-item-check').length > 0,
		onClick: shopWildbGetTrbxSticker,
	})
	
	shopWildbAddButtonToBottomPanel({
		action: 'addOrderTrbx',
		name: 'Добавить заказы',
		need: $(e.target).hasClass('open'),
		onClick: shopWildbOpenOrderSelectForm,
	})
}

function shopWildbChangeMenu(e){
	
	e.preventDefault()
	
	$(".wb-assembly-menu__tab--selected").removeClass("wb-assembly-menu__tab--selected")
	$(e.target).addClass("wb-assembly-menu__tab--selected")
	shopWildbCloseSupply(e)
}

function shopWildbAddTrbxButton(e){
	shopWildbAddButtonToBottomPanel({
		action: 'addTrbx',
		name: 'Добавить короб',
		need: $(e.target).data('tab') === 'trbx' && $('.js-wb-item-check').length > 0,
		onClick: shopWildbCreateTrbx,
	})
}

function shopWildbCreateTrbx(){
	
	$.post('?plugin=wildb&module=backend&action=createTrbx',{supply_id: $('[data-supply-id]').data('supply-id')}, (result) => {
		if(result.data.result){
			shopWildbAppendTrbxList(result.data.trbx)
		}
	})
}

function shopWildbAppendTrbxList(trbx){
	
	for(let i = 0; i < trbx.length; i++){
		
		let $spolerItem = $('<div class="wb-spoiler-item"></div>')
		$spolerItem.append('<div data-accordion-id="'+trbx[i]+'" class="wb-spoiler-item__header"><span class="wb-spoiler-item__title">Короб '+trbx[i]+'</span></div>')
		$spolerItem.append('<div class="wb-spoiler-item__panel"><div class="wb-spoiler-item__panel-inner"></div></div>')
		$spolerItem.appendTo('[data-accordion]')
	}
	
	// refresh accordion
	$('[data-accordion]').accordion("activate", -1)
	$('[data-accordion]').accordion("destroy")
	let $tab = $('[data-accordion]').parent()
	let accordion = $tab.html()
	$('[data-accordion]').remove()
	$tab.html(accordion)
	$tab.find('.open').removeClass('open')
	$(".js-wb-assembly-bottom-panel").find('*[data-action="getTrbxSticker"]').remove()
	$(".js-wb-assembly-bottom-panel").find('*[data-action="deleteTrbx"]').remove()
	$tab.find('.js-wb-item-check').click(shopWildbSelectTask)
	createAccordions()
	$tab.find('.wb-spoiler-item__header').click(shopWildbTrbxButtonsEvent)
}

function shopWildbOpenSupply(e){
	
	e.preventDefault()
	
	let supply = $(e.target).data('supply')
	let type = $(e.target).data('type')
	$(".js-wb-assembly-items").html('<div class="wb-item-loading"><i class="icon16 loading"></i></div>')
	$.get('?plugin=wildb&module=assembly&action=supplyViewer&type=' + type + '&supply=' + supply, shopWildbShowAssembly)
}

function shopWildbCloseSupply(e){
	
	e.preventDefault()
	
	let type = $(e.target).data('type')
	$(".js-wb-assembly-items").html('<div class="wb-item-loading"><i class="icon16 loading"></i></div>')
	$.get('?plugin=wildb&module=assembly&action=assemblies&type=' + type, shopWildbShowAssembly)
}

function shopWildbSelectAllTasks(e){
	
	let select_all = $(e.target).is(":checked")
	$(".js-wb-item-check").attr("checked", select_all)
	shopWildbSelectTask()
}

function shopWildbSelectTask(){
	
	if($('.js-wb-supply-close').length > 0){
		
		let type = $('.js-wb-supply-close').data('type')
		
		if(type === 'confirm'){
			if($('.tabs-header__item.active').find('a').data('tab') === 'trbx'){
				shopWildbAddButtonToBottomPanel({
					action: 'removeFromTrbx',
					name: 'Убрать из короба',
					need: $(".js-wb-item-check:checked").length > 0,
					onClick: shopWildbRemoveFromTrbx,
				})
			}else{
				shopWildbAddButtonToBottomPanel({
					action: 'recompleted',
					name: 'Перенести',
					need: $(".js-wb-item-check:checked").length > 0,
					onClick: shopWildbOpenSuppliesManager,
				})
			}
		}
		
		shopWildbAddButtonToBottomPanel({
			action: 'getOrderSticker',
			name: 'Этикетка',
			need: $(".js-wb-item-check:checked").length > 0,
			onClick: shopWildbGetOrdersSticker,
		})
	}else{
		shopWildbAddButtonToBottomPanel({
			action: 'completed',
			name: 'Собрать в поставку',
			need: $(".js-wb-item-check:checked").length > 0,
			onClick: shopWildbOpenSuppliesManager,
		})
		
		shopWildbAddButtonToBottomPanel({
			action: 'canseled',
			name: 'Отменить заказ',
			need: $(".js-wb-item-check:checked").length > 0,
			onClick: shopWildbCanselAssembly,
		})
	}
}

function shopWildbUpdateMenuPanel(){
	
	let $activeTab = $('.wb-assembly-menu__tab--selected')
	let tabs = {new: "Новые ",confirm: "На сборке ", complete: "В доставке ", archive: "В архиве "}
	let type = $activeTab.data('type')
	
	if(type === "new"){
		$activeTab.text(tabs[type] + $(".js-wb-item-check").length)
	}else if(type === "archive"){
		$activeTab.text(tabs[type] + $("[data-assembly-id]").length)
	}else{
		let count = 0;
		let $supplies = $('.js-wb-supply-check')
		
		for(let i = 0; i < $supplies.length; i++){
			let assemblies = parseInt($supplies.eq(i).parent().parent().children().last().children().first().text())
			count += assemblies
		}
		
		$activeTab.text(tabs[type] + count)
	}
	
	if(type === "new" && $(".js-wb-item-check").length === 0){
		$activeTab.removeClass("wb-assembly-menu__tab--new-tasks")
	}
}

function shopWildbCanselAssembly(){
	$.post('?plugin=wildb&module=backend&action=canselAssembly',{order_id: $(".js-wb-item-check:checked").data('order-id')}, (result) => {
		if(result.data.result){
			$(".js-wb-item-check:checked").parent().parent().remove()
			shopWildbAddButtonToBottomPanel({action: 'completed', need: false})
			shopWildbAddButtonToBottomPanel({action: 'canseled', need: false})
			shopWildbUpdateMenuPanel()
		}
	})
}

function shopWildbGetOrdersSticker(e){
	
	e.preventDefault()
	
	let orders = []
	
	for(let i = 0; i < $('.js-wb-item-check:checked').length; i++){
		orders.push($('.js-wb-item-check:checked').eq(i).data('order-id'))
	}
	
	window.open("?plugin=wildb&module=assembly&action=getOrderSticker&order_id=" + orders.join(','), '_blank')
}

function shopWildbGetSupplySticker(e){
	
	e.preventDefault()
	
	window.open("?plugin=wildb&module=assembly&action=getSupplySticker&supply_id=" + $('.js-wb-supply-check:checked').data('supply-id'), '_blank')
}

function shopWildbGetTrbxSticker(e){
	
	e.preventDefault()
	
	let trbx_id = $('.wb-spoiler-item__header.open').data('accordion-id')
	let supply_id = $('[data-accordion]').data('supply-id')
	
	window.open("?plugin=wildb&module=assembly&action=getTrbxSticker&trbx_id=" + trbx_id + "&supply_id=" + supply_id, '_blank')
}

function shopWildbSelectSupply(e){
	
	if($(e.target).data('type') === 'complete'){
		
		shopWildbAddButtonToBottomPanel({
			action: 'getSupplySticker',
			name: 'QR код поставки',
			need: $(".js-wb-supply-check:checked").length > 0,
			onClick: shopWildbGetSupplySticker,
		})
	}
	
	let count = parseInt($(e.target).parent().parent().children().last().find('span').text())
	
	if($(e.target).data('type') === 'confirm'){
		
		shopWildbAddButtonToBottomPanel({
			action: 'completeSupply',
			name: 'Передать в доставку',
			need: $(".js-wb-supply-check:checked").length > 0 && count > 0,
			onClick: shopWildbCompleteSupply,
		})
		
		if($(".js-wb-supply-manager-delete").length === 0){
			shopWildbAddButtonToBottomPanel({
				action: 'completed',
				name: 'Удалить поставку',
				need: true,
				onClick: shopWildbDeleteSupply,
				class: 'js-wb-supply-manager-delete',
			})
		}
	}
	
	
	$(".js-wb-supply-check").not(e.target).attr("checked", false)
	$(".js-wb-supply-manager-move").attr("disabled", $(".js-wb-supply-check:checked").length === 0)
	$(".js-wb-supply-manager-delete").attr("disabled", $(".js-wb-supply-check:checked").length === 0 || count > 0)
}

function shopWildbCompleteSupply(e){
	$.post('?plugin=wildb&module=backend&action=completeSupply',{supply_id: $(".js-wb-supply-check:checked").data('supply-id')}, shopWildbRemoveSupplyFromDialog)
}

function shopWildbRemoveSupplyFromDialog(result){
	if(result.data.result){
		$(".js-wb-supply-check:checked").parent().parent().remove()
		$(".js-wb-supply-manager-move").attr("disabled", true)
		$(".js-wb-supply-manager-delete").attr("disabled", true)
	}
}

function shopWildbRemoveTrbxFromDialog(result){
	if(result.data.result){
		$('.wb-spoiler-item__header.open').parent().remove()
		$('[data-action="getTrbxSticker"], [data-action="addOrderTrbx"], [data-action="deleteTrbx"]').remove()
	}
}

function shopWildbAddButtonToBottomPanel(options){
	
	let $elems = $(".js-wb-assembly-bottom-panel").find('*[data-action="' + options.action + '"]')
	
	if($elems.length > 0 || !options.need){
		if(!options.need){
			$elems.remove()
		}
		return
	}
	
	let $button = $('<button data-owner="assembly" data-action='+options.action+' class="button button--wb js-wb-assembly-custom-button">'+options.name+'</button>')
	$button.click(options.onClick)
	
	if(options.class){
		$button.addClass(options.class)
	}
	
	$(".js-wb-assembly-bottom-panel").append($button)
}

function shopWildbAddOrdersSupply($supplyManager, owner) {
	
	let supply_id = $supplyManager.find('.js-wb-supply-check:checked').data('supply-id');
	let $checked_items = owner === 'backendOrder' ? $('.js-wb-order-item-check:checked') : $(".js-wb-item-check:checked")
	let orders = [];
	
	for (let i = 0; i < $checked_items.length; i++){
		orders.push($($checked_items[i]).data("order-id"))
	}
	$.post('?plugin=wildb&module=backend&action=assembliesComplete',{orders: orders, supply_id: supply_id}, (result) => {
		if(result.data.result){
			
			$supplyManager.trigger("close")
			$supplyManager.remove()
			
			if(owner === 'backendOrder'){
				shopWildbLoadInfoSection()
				return
			}
			
			let type = $(".wb-assembly-menu__tab--selected").data('type')
			
			$(".js-wb-assembly-items").html('<div class="wb-item-loading"><i class="icon16 loading"></i></div>')
			$.get('?plugin=wildb&module=assembly&action=assemblies&type=' + type, shopWildbShowAssembly)
		}
	})
}

function shopWildbAppendSupplyList(supply){
	
	let $row = $('<div class="table-emulate__row"></div>')
	let $cell = $('<div class="table-emulate__cell"></div>')
	let $value = $('<span class="table-emulate__field-value"></span>')
	
	$('<input type="checkbox" class="wb-item-checkbox js-wb-supply-check" data-supply-id="' + supply.id + '">').click(shopWildbSelectSupply).appendTo($cell.clone().appendTo($row))
	$value.clone().text(supply.name).appendTo($cell.clone().appendTo($row))
	$value.clone().text(supply.createdAt).appendTo($cell.clone().appendTo($row))
	$value.clone().text(supply.id).appendTo($cell.clone().appendTo($row))
	$value.clone().text('0').appendTo($cell.clone().appendTo($row))
	
	$('.js-wb-supply-manager-list').append($row)
}

function shopWildbCreateSupply(e){
	
	$.post('?plugin=wildb&module=backend&action=createSupply',{name: $('#wb-supply-create-name').val()}, (result) => {
		if(result.data.result){
			$(e.target).parent().parent().parent().remove()
			shopWildbAppendSupplyList(result.data.supply)
		}
	})
}

function shopWildbDeleteSupply(e){
	$.post('?plugin=wildb&module=backend&action=deleteSupply',{supply_id: $(".js-wb-supply-check:checked").data('supply-id')}, shopWildbRemoveSupplyFromDialog)
}

function shopWildbDeleteTrbx(e){
	
	let data = {
		trbx_id: $('.wb-spoiler-item__header.open').data('accordion-id'),
		supply_id: $('[data-accordion]').data('supply-id'),
	}
	
	$.post('?plugin=wildb&module=backend&action=deleteTrbx', data, shopWildbRemoveTrbxFromDialog)
}

function shopWildbCreateSupplyDialog(){
	
	let $bg = $('<div class="wb-assembly-menu__supply-name-dialog__background"></div>')
	
	let $buttons = $('<div class="wb-assembly-menu__supply-name-dialog__buttons"></div>')
	$('<button class="button button--wb">Создать</button>').click(shopWildbCreateSupply).appendTo($buttons)
	$('<button class="button button--wb">Закрыть</button>').click(() => {$bg.remove()}).appendTo($buttons)
	
	let $dialog = $('<div class="wb-assembly-menu__supply-name-dialog">')
	$dialog.append('<label class="wb-assembly-menu__supply-name-dialog__header" for="wb-supply-create-name">Введите название поставки</label>')
	$dialog.append('<input type="text" id="wb-supply-create-name" value="Поставка на ' + (new Date()).toLocaleDateString() + '">')
	$dialog.append($buttons)
	
	$dialog.appendTo($bg)
	
	$bg.appendTo('body')
}

function shopWildbOpenSuppliesManager(e){
	
	e.preventDefault()
	
	$.get('?plugin=wildb&module=assembly&action=suppliesManager',(result) => {
		
		let $supplyManager = $(result).waDialog()
		
		$supplyManager.find(".dialog-window").css('padding','50px 0 55px').css('top','10%').css('bottom','10%')
		
		$supplyManager.find(".dialog-background, .js-wb-supply-manager-close").click(() => {
			$supplyManager.trigger("close")
			$supplyManager.remove()
		})
		
		$supplyManager.find(".js-wb-supply-manager-move").click(() => {
			shopWildbAddOrdersSupply($supplyManager, $(e.target).data('owner'))
		})
		
		$supplyManager.find(".dialog-buttons").remove()
		$supplyManager.find(".js-wb-supply-manager-create").click(shopWildbCreateSupplyDialog)
		$supplyManager.find(".js-wb-supply-manager-delete").click(shopWildbDeleteSupply)
		$supplyManager.find(".js-wb-supply-check").click(shopWildbSelectSupply)
	})
}

function shopWildbOpenOrderSelectForm(){
	
	$.get('?plugin=wildb&module=assembly&action=orderSelectForm&supply_id='+$('[data-accordion]').data('supply-id'),(result) => {
		
		let $orderSelector = $(result).waDialog()
		
		$orderSelector.find(".dialog-window").css('padding','50px 0 55px').css('top','10%').css('bottom','10%')
		
		$orderSelector.find(".dialog-background, .js-wb-order-selector-close").click(() => {
			$orderSelector.trigger("close")
			$orderSelector.remove()
		})
		
		$orderSelector.find(".js-wb-order-selector-add").click(() => {
			shopWildbAddAssemblyTrbx($orderSelector)
		})
		
		$orderSelector.find(".dialog-buttons").remove()
		$orderSelector.find(".js-wb-order-select-check").click(shopWildbSelectOrder)
		$orderSelector.find(".js-wb-order-select-check-all").click(shopWildbSelectAllOrders)
	})
}

function shopWildbRemoveFromTrbx(){
	
	let $checked_items = $(".js-wb-item-check:checked")
	let orders = [];
	
	for (let i = 0; i < $checked_items.length; i++){
		orders.push($($checked_items[i]).data("order-id"))
	}
	
	$.post('?plugin=wildb&module=backend&action=removeFromTrbx',{orders: orders}, (result) => {
		if(result.data.result){
			$checked_items.parent().parent().remove()
			$('[data-action="removeFromTrbx"], [data-action="getOrderSticker"]').remove()
			
			if($(".js-wb-item-check:checked").length === 0){
				$('[data-action="getTrbxSticker"]').remove()
			}
		}
	})
}

function shopWildbAddAssemblyTrbx($orderSelector){
	
	let $trbx = $('.wb-spoiler-item__header.open')
	let supply_id = $trbx.parent().parent().data('supply-id');
	let trbx_id = $trbx.data('accordion-id');
	let $checked_items = $(".js-wb-order-select-check:checked")
	let orders = [];
	
	for (let i = 0; i < $checked_items.length; i++){
		orders.push($($checked_items[i]).data("order-id"))
	}
	$.post('?plugin=wildb&module=backend&action=addOrdersTrbx',{orders: orders, trbx_id: trbx_id, supply_id: supply_id}, (result) => {
		if(result.data.result){
			for(let i = 0; i < $checked_items.length; i++){
				
				let $row = $($checked_items[i]).parent().parent()
				
				let order_id = $row.find('.table-emulate__cell').eq(1).find('.table-emulate__field-value').text()
				let shop_order_id = $row.find('.table-emulate__cell').eq(2).find('.table-emulate__field-value').html()
				let date_order = $row.find('.table-emulate__cell').eq(3).find('.table-emulate__field-value').text()
				let $product = $row.find('.table-emulate__cell').eq(4).find('.table-emulate__field-value').eq(0).find('a')
				let sku = $row.find('.table-emulate__cell').eq(4).find('.table-emulate__field-value').eq(1).text()
				let sum = $row.find('.table-emulate__cell').eq(5).find('.table-emulate__field-value').text()
				
				$product.text($product.text() + ' ' + sku)
				
				let $trbx_order = $('<div class="wb-trbx-order"></div>')
				let $checkbox = $('<input type="checkbox" class="wb-item-checkbox js-wb-item-check" data-order-id="'+order_id+'">')
				$checkbox.click(shopWildbSelectTask)
				
				$('<div class="wb-trbx-order__checkbox"></div>').append($checkbox).appendTo($trbx_order)
				$('<div class="table-emulate__row wb-trbx-order__title"></div>')
				  .append(shop_order_id)
				  .append('<span>WB '+order_id+'</span>')
				  .append('<span>'+date_order+'</span>')
				  .appendTo($trbx_order)
				$('<div class="table-emulate__row wb-trbx-order__item"></div>')
				  .append($('<span class="wb-trbx-order__product"></span>').append($product))
				  .append('<span class="wb-trbx-order__sum">'+sum+'</span>')
				  .appendTo($trbx_order)
				
				let $spolerItem = $trbx.parent()
				$spolerItem.find('.wb-spoiler-item__panel-inner').append($trbx_order)
			}
			
			shopWildbTrbxButtonsEvent({target: $trbx})
			
			$orderSelector.trigger("close")
			$orderSelector.remove()
		}
	})
}

function shopWildbSelectAllOrders(e){
	
	let select_all = $(e.target).is(":checked")
	$(".js-wb-order-select-check").attr("checked", select_all)
	shopWildbSelectOrder()
}

function shopWildbSelectOrder(){
	$(".js-wb-order-selector-add").attr("disabled", $(".js-wb-order-select-check:checked").length === 0)
}

$(document).ready(function(){
    $('.js-wb-assembly').click(shopWildbOpenAssemblies)
});
