<?php

	class shopWildbPluginAssemblyActions extends waViewActions {
		
		const TEMPLATE_DIR = "plugins/" . shopWildbPlugin::PLUGIN_ID . "/templates/actions/assembly/";
		
		private $data = [];
		
		public function setTemplate($template, $is_relative = false){
			$template = wa('shop')->getAppPath(self::TEMPLATE_DIR . $template . ".html");
			parent::setTemplate($template);
		}
		
		public function execute($action, $params = null) {
			
			$this->data['plugin_url'] = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getPluginStaticUrl(false);
			$this->data['backed_url'] = wa()->getConfig()->getBackendUrl(true);
			$this->data['type'] = waRequest::get("type", "new");
			$this->data['states'] = shopWildbOrders::getStatuses('wb_status');
			
			$orders_model = new shopWildbOrdersModel;
			$this->data['orders'] = $orders_model->getOrders();
			
			parent::execute($action, $params);
			
			$this->view->assign($this->data);
		}
		
		public function defaultAction(){
			$this->fillOrders();
		}
		
		public function supplyViewerAction(){
			
			$this->setTemplate('AssemblySupplyViewer');
			
			$supply_model = new shopWildbSuppliesModel;
			$this->data['supply'] = $supply_model->getById(waRequest::get("supply"));
			$this->data['supply_orders'] = [];

			foreach ($this->data['orders'] as $order){
				if($order['supply_id'] === $this->data['supply']['id']){
					
					$this->data['supply_orders'][] = $order;
					
					if($order['trbx_id']){
						$this->data['trbxs'][$order['trbx_id']][] = $order;
					}
				}
			}
		}
		
		public function suppliesManagerAction(){
			
			$this->setTemplate('AssemblySuppliesManager');
			
			$this->data['type'] = 'confirm';
			
			$this->fillOrders();
			$this->fillSuppliesOrders();
		}
		
		public function orderSelectFormAction(){
			
			$this->setTemplate('AssemblyOrderSelectForm');
			$this->data['orders_out'] = [];
			
			$supply_id = waRequest::get("supply_id");
			
			if($supply_id){
				foreach ($this->data['orders'] as $order){
					if($order['supply_id'] === $supply_id && is_null($order['trbx_id'])){
						$this->data['orders_out'][] = $order;
					}
				}
			}
		}
		
		public function trbxSelectFormAction(){
			
			$this->setTemplate('AssemblyTrbxSelectForm');
			$supply_id = waRequest::get("supply_id");
			
			if($supply_id){
				$supplies_model = new shopWildbSuppliesModel;
				$this->data['supply'] = $supplies_model->getById($supply_id);
			}
		}
		
		public function assembliesAction(){
			
			$this->setTemplate('Assembly' . ucfirst($this->data['type']));
			
			$this->fillOrders();
			$this->fillSuppliesOrders();
		}
		
		private function fillOrders(){
			
			$this->data['tasks']['new'] = ['name' => 'Новые', 'tasks' => []];
			$this->data['tasks']['confirm'] = ['name' => 'На сборке', 'tasks' => [], 'supplies' => []];
			$this->data['tasks']['complete'] = ['name' => 'В доставке', 'tasks' => [], 'supplies' => []];
			$this->data['tasks']['archive'] = ['name' => 'В архиве', 'tasks' => [], 'supplies' => []];
			
			foreach ($this->data['orders'] as $order){
				$this->data['tasks'][$order['type']]['tasks'][] = $order;
			}
		}
		
		private function fillSuppliesOrders(){
			
			$supplies_model = new shopWildbSuppliesModel;
			$this->data['supplies'] = $this->data['type'] === 'confirm' ? $supplies_model->getByField('done', 0, 'id') : $supplies_model->getCompleteSupplies();
			
			foreach ($this->data['orders'] as $order) {
				if(isset($this->data['supplies'][$order['supply_id']])){
					$this->data['supplies'][$order['supply_id']]['orders'][] = $order;
				}
			}
		}
		
		public function getOrderStickerAction(){
			
			$this->setTemplate('AssemblyStickers');
			$this->view->assign('plugin_name',wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getName());
			
			if($order_id = waRequest::get('order_id')){
				if($stickers = shopWildbOrders::getStickers(explode(',', $order_id))) {
					
					$orders_model = new shopWildbOrdersModel;
					$this->view->assign('selection',$orders_model->getOrdersSelection(array_keys($stickers)));
					$this->view->assign('stickers',$stickers);
					
				}else{
					$this->view->assign('error','Ошибка получения этикетки. Подробная ошибка должна быть записана в логи');
				}
			}else{
				$this->view->assign('error','Ошибка получения этикетки. Получен некорректный номер заказа');
			}
		}
		
		public function getSupplyStickerAction(){
			
			$this->setTemplate('SupplySticker');
			$this->view->assign('plugin_name',wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getName());
			
			if($supply_id = waRequest::get('supply_id')){
				if($sticker = shopWildbSupplies::getSticker($supply_id)) {
					$this->view->assign('title',$supply_id);
					$this->view->assign('stickers',[$sticker]);
				}else{
					$this->view->assign('error','Ошибка получения этикетки. Подробная ошибка должна быть записана в логи');
				}
			}else{
				$this->view->assign('error','Ошибка получения этикетки. Получен некорректный номер поставки');
			}
		}
		
		public function getTrbxStickerAction(){
			
			$this->setTemplate('SupplySticker');
			$this->view->assign('plugin_name',wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getName());
			
			$trbx_id = waRequest::get('trbx_id');
			$supply_id = waRequest::get('supply_id');
			
			if($trbx_id && $supply_id){
				if($sticker = shopWildbTrbx::getSticker($supply_id, explode(',', $trbx_id))) {
					$this->view->assign('title',$trbx_id);
					$this->view->assign('stickers',$sticker['stickers']);
				}else{
					$this->view->assign('error','Ошибка получения этикетки. Подробная ошибка должна быть записана в логи');
				}
			}else{
				$this->view->assign('error','Ошибка получения этикетки. Получен некорректный номер поставки');
			}
		}
	}