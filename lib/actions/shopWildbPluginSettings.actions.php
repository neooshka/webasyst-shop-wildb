<?php
	
	class shopWildbPluginSettingsActions extends waViewActions {
		
		const TEMPLATE_DIR = "plugins/" . shopWildbPlugin::PLUGIN_ID . "/templates/actions/settings/";
		
		private $settings;
		
		/** @var shopWildbPlugin */
		private $plugin;
		
		/** @var shopCategoryModel */
		private $category_model;
		
		/** @var shopWildbFeaturesModel */
		private $wb_features_model;
		
		/** @var shopFeatureModel */
		private $feature_model;
		
		/** @var shopWildbSyncFeaturesModel */
		private $wb_features_sync_model;
		
		public function setTemplate($template, $is_relative = false){
			$template = wa('shop')->getAppPath(self::TEMPLATE_DIR . $template . ".html");
			parent::setTemplate($template);
		}
		
		public function execute($action, $params = null) {
			
			$this->plugin = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID);
			$this->settings = $this->plugin->getSettings();
			$this->category_model = new shopCategoryModel();
			$this->wb_features_model = new shopWildbFeaturesModel;
			$this->feature_model = new shopFeatureModel;
			$this->wb_features_sync_model = new shopWildbSyncFeaturesModel;
			
			if($action !== 'default'){
				$this->setTemplate("SettingsBlock" . ucfirst($action));
				
				switch ($action){
					case "apiLog":
						if($task = waRequest::get('task')){
							$this->setTemplate("SettingsBlockApiLogRows");
						}else{
							$task = 'prices';
						}
						
						$page = waRequest::get('page', 0);
						$limit = waRequest::get('limit', 10);
						$offset = $page * $limit;
						
						$log_model = new shopWildbApiLogModel();
						$tasks = [
							'prices' => 'Цены и скидки',
							'stocks' => 'Остатки',
							'orders' => 'Заказы',
							'orders_statuses' => 'Статусы заказов',
						];
						$this->view->assign('page_limit', $limit);
						$this->view->assign('page', $page);
						$this->view->assign('request_task', $task);
						$this->view->assign('tasks', $tasks);
						$this->view->assign('api_log', $log_model->getLog($task, $offset, $limit));
						break;
					case "goods":
						$this->view->assign('variants', $this->getOptionsForMergeBy());
						$this->view->assign('sets', $this->getOptions((new shopSetModel())->getByField('type', 0, true)));
						$this->view->assign('categories_tree', $this->getShopTree());
						break;
					case "categories":
						$tree = $this->getShopTree(true);
						$this->view->assign('categories_tree', $tree);
						$this->view->assign('wb_categories', json_encode($this->getWbCategories(),JSON_UNESCAPED_UNICODE));
						$this->view->assign('wb_sync_categories', (new shopWildbSyncCategoriesModel)->getSyncCategories());
						$this->view->assign('shop_categories', json_encode($tree,JSON_UNESCAPED_UNICODE));
						break;
					case "features":
						$this->view->assign('shop_features', $this->getOptionsForFeatures());
						$this->view->assign('wb_features', $this->wb_features_model->getFeaturesBySyncCategories());
						break;
					case "directory":
						
						$wb_directory = waRequest::request('wb_directory_page',null,waRequest::TYPE_STRING);
						
						$this->view->assign('directory_page',$wb_directory);
						
						if(!is_null($wb_directory)){
							$shop_feature = $this->getShopFeatureValuesByDirectory($wb_directory);
							$this->view->assign('shop_feature_values',$shop_feature['values']);
							$this->view->assign('wb_sync_directories',(new shopWildbSyncDirectoryModel)->getByFeatureId($shop_feature['id']));
							$this->view->assign('shop_feature_id',$shop_feature['id']);
							$this->view->assign('wb_feature_values',json_encode((new shopWildbDirectoryModel)->getDirectories($wb_directory),JSON_UNESCAPED_UNICODE));
						}else{
							$this->view->assign('wb_directories',$this->getFeatureNames());
						}
						
						break;
					case "exportCards":
						$tasksModel = new shopWildbTasksModel();
						$tasks = $tasksModel->getByField("status","0",true);
						$last_tasks = $tasksModel->getLastTasks();
						$task_names = [
							'new_cards' => 'Экспорт новых карточек',
							'update_cards' => 'Обновление карточек',
							'upload_images' => 'Экспорт картинок',
							'upload_missing_images' => 'Экспорт картинок',
							'download_cards' => 'Сопоставление карточек',
						];
						$this->view->assign('tasks', $tasks);
						$this->view->assign('task_names', $task_names);
						$this->view->assign('last_tasks', $last_tasks);
						
						if($this->settings['wb_debug']){
							waLog::dump(['tasks' => $tasks],shopWildbPlugin::SLUG . '/WbSettings.log');
						}
					case "prices":
						$this->view->assign('price_fields', $this->getPriceFields());
						$this->view->assign('features', $this->getFeaturesOnlyOptions());
						break;
					case "categoriesTree":
						$this->setTemplate("CategoriesTree");
						$this->view->assign('categories_tree', $this->getShopTree());
						$this->view->assign('condition_categories', waRequest::request('categories',[],waRequest::TYPE_ARRAY_INT));
						break;
					case "stocks":
						$this->view->assign('stocks', $this->getOptions((new shopStockModel())->getAll()));
						break;
					case "orders":
						$this->view->assign([
							'workflow_actions_single' => $this->getWorkflowActionsForShopOrders(),
							'workflow_actions_multi' => $this->getWorkflowActions(),
							'workflow_states' => $this->getWorkflowStates(),
							'wb_states' => $this->getWBStates(),
							'shippings' => $this->getPluginsMethods(shopPluginModel::TYPE_SHIPPING),
							'payments' => $this->getPluginsMethods(shopPluginModel::TYPE_PAYMENT),
						]);
						break;
				}
			}

			$this->view->assign([
				'flock' => 'flock -n ' . wa('shop')->getConfig()->getRootPath() . DIRECTORY_SEPARATOR,
				'cron' => 'php ' . wa('shop')->getConfig()->getRootPath() . DIRECTORY_SEPARATOR . 'cli.php shop',
				'settings' => $this->settings,
				'plugin_url' => $this->plugin->getPluginStaticUrl(false),
			]);
		}
		
		private function getFeatureNames(){
			
			$result = [];
			
			$names = shopWildbDirectoryesHelper::getAllFeatureNames();
			
			foreach ($names as $code => $name){
				
				if($wb_feature = $this->wb_features_model->getByField('name',$name)){
					if($this->wb_features_sync_model->getByField('wb_feature_id',$wb_feature['id'])){
						$result[$code] = $name;
					}
				}
			}
			
			return $result;
		}
		
		private function getShopFeatureValuesByDirectory(string $directory = null){
			
			if(is_null($directory)) return [];
			
			$wb_feature_name = shopWildbDirectoryesHelper::getFeatureNameByDirectory($directory);
			$wb_feature_id = $this->wb_features_model->getByField('name',$wb_feature_name)['id'];
			$shop_feature_id = $this->wb_features_sync_model->getByField('wb_feature_id',$wb_feature_id)['shop_feature_id'];
			
			return $this->feature_model->getValues([$this->feature_model->getById($shop_feature_id)],true)[0];
		}
		
		private function getCategoriesFilter(){
			
			$result = [];
			
			if($this->settings['shop_products'] == 2){
				if($this->settings['shop_products_type'] == 1){
					if(isset($this->settings['shop_products_categories']) && is_array($this->settings['shop_products_categories']) && !empty($this->settings['shop_products_categories'])) {
						$result = $this->settings['shop_products_categories'];
					}
				}elseif(isset($this->settings['shop_products_sets']) && is_array($this->settings['shop_products_sets'])){
					
					$sets = [];
					
					foreach ($this->settings['shop_products_sets'] as $set_id){
						$sets[] = $this->category_model->escape($set_id);
					}
					
					$result = array_keys($this->category_model->query("
						select distinct(case when p.category_id is null then 0 else p.category_id end) as category_id from shop_set_products sp
						join shop_product p on sp.product_id = p.id
						where sp.set_id in (s:sets)
					", ['sets' => $sets])->fetchAll('category_id'));
				}
			}
			
			return $result;
		}
		
		private function getParentCategories(array $tree, array $cat){
			
			if($cat['depth'] > 1){
				
				$result = $this->getParentCategories($tree,$tree[$cat['parent_id']]);
				$result[$cat['parent_id']] = $tree[$cat['parent_id']];
				
				return $result;
				
			}if($cat['depth'] === "1"){
				return [$cat['parent_id'] => $tree[$cat['parent_id']]];
			}else{
				return null;
			}
		}
		
		private function getShopTree($by_filter = false){
			
			$cats = $this->category_model->getTree(0, null);
			$cats[0] = [
				'id' => 0,
				'left_key' => 0,
				'right_key' => 0,
				'depth' => 0,
				'parent_id' => 0,
				'name' => "Категория отсутствует",
			];
			
			if($by_filter){
				
				$filtered = [];
				$filter = $this->getCategoriesFilter();
				
				foreach ($cats as $c){
					if(array_search($c['id'],$filter) !== false){
						if($c['parent_id'] > 0){
							$filtered += $this->getParentCategories($cats,$c);
						}
						
						$filtered[$c['id']] = $c;
					}
				}
				
				$cats = $filtered;
			}
			
			$stack = [];
			$result = [];
			
			foreach ($cats as $c) {
				
				$c['categories'] = [];

				$l = count($stack);
				
				while ($l > 0 && $stack[$l - 1]['depth'] >= $c['depth']) {
					array_pop($stack);
					$l--;
				}
				
				if ($l == 0) {
					$i = count($result);
					$result[$i] = $c;
					$stack[] = & $result[$i];
				} else {
					$i = count($stack[$l - 1]['categories']);
					$stack[$l - 1]['categories'][$i] = $c;
					$stack[] = & $stack[$l - 1]['categories'][$i];
				}
			}
			
			return $result;
		}
		
		private function getFeaturesOnlyOptions(){
			
			$result = $this->getOptions($this->feature_model->getAll());
			
			array_unshift($result, ['value' => '', 'title' => '']);
			
			return $result;
		}
		
		private function getPluginsMethods(string $type){
			
			$result = $this->getOptions((new shopPluginModel())->listPlugins($type, ['all' => true]));
			
			array_unshift($result, ['value' => '0', 'title' => '']);
			
			return $result;
		}
		
		public function getPriceFields(){
			
			$fields = [
				[
					'group_name' => 'Основные поля товара',
					'group_values' => $this->getOptions([
						['id' => 'price', 'name' => 'Цена'],
						['id' => 'compare_price', 'name' => 'Зачеркнутая цена'],
						['id' => 'purchase_price', 'name' => 'Закупочная цена'],
					]),
				],
			];
			
			// price plugins++
			foreach (shopWildbPlugin::getPricePlugins() as $plugin){
				
				/** @var shopWildbPricePluginController $price_plugin */
				$price_plugin = new $plugin;
				
				if($price_plugin->pluginInstalled()) {
					$fields[] = [
						'group_name' => 'Плагин ' . $price_plugin->getPluginName(),
						'group_values' => $this->getOptions($price_plugin->getPriceFields()),
					];
				}elseif($this->settings['wb_debug']){
					waLog::log("Ошибка подключения плагина цен {$plugin}. Плагин не установлен или отключен", shopWildbPlugin::SLUG . '/WbSettings.log');
				}
			}
			// price plugins--
			
			$fields[] = [
				'group_name' => 'Характеристики',
				'group_values' => $this->getOptions($this->feature_model->getAll()),
			];
			
			return $fields;
		}
		
		private function getOptions($values){
			
			$result = [];
			
			foreach ($values as $value){
				
				$key = $value['code'] ?? $value['id'];
				
				$result[$key] = [
					'value' => $value['id'],
					'title' => isset($value['code']) ? "{$value['name']} ({$value['code']})" : $value['name'],
				];
			}
			
			return $result;
		}
		
		private function getOptionsForFeatures(){
			
			$result = [
				[
					'group_name' => 'Основные поля товара',
					'group_values' => [
						'name' => ['value' => 'name', 'title' => 'Наименование'],
						'description' => ['value' => 'description', 'title' => 'Описание'],
						'summary' => ['value' => 'summary', 'title' => 'Краткое описание'],
					],
				],
				[
					'group_name' => 'Характеристики',
					'group_values' => $this->getOptions($this->feature_model->getAll()),
				],
			];
			
			return $result;
		}
		
		private function getOptionsForMergeBy(){
			
			$result = $this->getOptionsForFeatures();
			$result[0]['group_values'] = ['product_id' => ['value' => 'product_id', 'title' => 'ID товара']] + $result[0]['group_values'];
			$result[0]['group_values']['category_id'] = ['value' => 'category_id', 'title' => 'Основная категории'];
			$result[0]['group_values']['type_id'] = ['value' => 'type_id', 'title' => 'Тип товара'];
			$result[0]['group_values']['sku'] = ['value' => 'sku', 'title' => 'Код артикула'];
			$result[0]['group_values']['sku_name'] = ['value' => 'sku_name', 'title' => 'Наименование артикула'];
			
			return $result;
		}
		
		public function getWorkflowActionsForShopOrders() {
			
			$result = $this->getWorkflowActions();
			
			array_unshift($result, [
				'value' => '',
				'title' => _wp('Нет действия'),
			]);
			
			return $result;
		}
		
		public function getWorkflowActions() {
			
			$actions = (new shopWorkflow())->getAllActions();
			$result = [];
			
			foreach ($actions as $action) {
				if($action->getId() != 'create') {
					$result[] = [
						'value' => $action->getId(),
						'title' => $action->getName(),
					];
				}
			}
			
			return $result;
		}
		
		public function getWBStates() {
			
			$actions = shopWildbOrders::getStatuses('wb_status');
			$result = [];
			
			foreach ($actions as $key => $action) {
				$result[] = [
					'value' => $key,
					'title' => $action,
				];
			}
			
			return $result;
		}
		
		public function getWorkflowStates() {
			
			$actions = (new shopWorkflow())->getAllStates();
			$result = [];
			
			foreach ($actions as $action) {
				if($action->getId() != 'create') {
					$result[] = [
						'value' => $action->getId(),
						'title' => $action->getName(),
					];
				}
			}
			
			return $result;
		}
		
		private function getWbCategories(){
			
			$wb_categories_model = new shopWildbCategoriesModel;
			$wb_api = shopWildbApiContent::getInstance();
			
			$deadline = (new DateTime(ifempty($this->settings['wb_categories_date_update'],"2024-01-01 0:00:00")))->modify("+1 month");
			
			$wb_categories = $wb_categories_model->getAll();
			
			if(!$wb_categories || (new DateTime() > $deadline)) {
					
				$data = $wb_api->getAllCategories();
				
				if($data){
					foreach ($data as &$item){
						$item['id'] = $item['subjectID'];
						$item['parent_id'] = $item['parentID'];
						$item['name'] = $item['subjectName'];
						$item['parent_name'] = $item['parentName'];
					}
					
					unset($item);
					
					$wb_categories_model->multipleInsert($data);
					$wb_categories = $wb_categories_model->getAll();
					$this->plugin->saveSettings(['wb_categories_date_update' => (new DateTime())->format("Y-m-d H:i:s")]);
				}
			}
			
			return $wb_categories;
		}
	}