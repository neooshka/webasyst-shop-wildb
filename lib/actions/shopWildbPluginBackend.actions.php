<?php

	class shopWildbPluginBackendActions extends waJsonActions {
		
		public function syncCategoryAction() {
			
			$wb_category_id = waRequest::post('wb_category_id',0,waRequest::TYPE_INT);
			$shop_category_id = waRequest::post('shop_category_id',0,waRequest::TYPE_INT);
			
			$wb_sync_categories_model = new shopWildbSyncCategoriesModel;
			
			if($wb_category_id) {
				$wb_sync_categories_model->insert(['shop_category_id' => $shop_category_id, 'wb_category_id' => $wb_category_id], true);
				$this->downloadFeaturesFromWb($wb_category_id);
			}else{
				$wb_sync_categories_model->deleteByField('shop_category_id', $shop_category_id);
			}
			
			$this->response['status'] = true;
		}
		
		public function syncCategoriesAction(){
			
			$wb_category_id = waRequest::post('wb_category_id',0,waRequest::TYPE_INT);
			$shop_categories = waRequest::post('shop_categories',[],waRequest::TYPE_ARRAY_INT);
			
			if($shop_categories){
				
				$data = [];
				$update = ['wb_category_id' => $wb_category_id];
				
				foreach ($shop_categories as $category){
					$data[] = ['shop_category_id' => $category, 'wb_category_id' => $wb_category_id];
				}
				
				$wb_sync_categories_model = new shopWildbSyncCategoriesModel;
				
				if($wb_category_id) {
					$wb_sync_categories_model->multipleInsert($data, $update);
					$this->downloadFeaturesFromWb($wb_category_id);
				}else{
					$wb_sync_categories_model->deleteByField(['shop_category_id' => $shop_categories]);
				}
			}
			
			$this->response['status'] = true;
		}
		
		private function downloadFeaturesFromWb(int $id){
			
			$wb_features_categories_model = new shopWildbFeaturesCategoriesModel;
			$wb_features_model = new shopWildbFeaturesModel;
			$wb_api = shopWildbApiContent::getInstance();
			
			$wb_features = $wb_api->getFeatures($id);
			
			if(!$wb_features){
				waLog::dump("Не удалось скачать характеристики для категории {$wb_category_name}",shopWildbPlugin::SLUG . '/wbSettings.log');
				return;
			}
			
			$wb_features_categories_model->deleteByField(['wb_category_id' => $id]);
			
			foreach ($wb_features['data'] as $wb_feature) {
				
				$wb_features_model->insert([
				  'id' => $wb_feature['charcID'],
				  'name' => $wb_feature['name'],
				  'required' => $this->getRequiredAttribute($wb_feature),
				  'unit' => $wb_feature['unitName'],
				  'max_count' => $wb_feature['maxCount'],
				  'popular' => $wb_feature['popular'],
				  'charc_type' => $wb_feature['charcType'],
				  'strlen' => $this->getStrlenAttribute($wb_feature),
				], true);
				
				$wb_features_categories_model->insert(['wb_feature_id' => $wb_feature['charcID'], 'wb_category_id' => $id], 2);
			}
		}
		
		private function getRequiredAttribute(array $wb_feature): bool {
			
			$required = false;
			
			if(in_array($wb_feature['name'],['Бренд'])){
				$required = true;
			}
				
			return $wb_feature['required'] ? : $required;
		}
		
		private function getStrlenAttribute(array $wb_feature): int {
			
			$strlen = 100;
			
			if($wb_feature['name'] === 'Наименование'){
				$strlen = 60;
			}
			
			if($wb_feature['name'] === 'Описание'){
				$strlen = 2000;
			}
			
			return $strlen;
		}
		
		public function syncFeatureAction() {
			
			$wb_feature_id = waRequest::post('wb_feature_id',0,waRequest::TYPE_INT);
			$shop_feature_id = waRequest::post('shop_feature_id',0,waRequest::TYPE_STRING);
			
			if($wb_feature_id){
				
				$wb_sync_features_model = new shopWildbSyncFeaturesModel;
				
				if($shop_feature_id) {
					$wb_sync_features_model->insert(['wb_feature_id' => $wb_feature_id, 'shop_feature_id' => $shop_feature_id], true);
				}else{
					$wb_sync_features_model->deleteByField('wb_feature_id', $wb_feature_id);
				}
			}
			
			$this->response['status'] = true;
		}
		
		public function loadWbDirectoryAction(){
			
			$api = shopWildbApiContent::getInstance();
			$shop_wildb_directory_model = new shopWildbDirectoryModel;
			
			$directory = waRequest::request('directory',null,waRequest::TYPE_STRING);
			
			if(!is_null($directory)){
				if($result = $api->getDirectory($directory)) {
					
					foreach ($result as &$item){
						
						if(is_string($item)){
							$item = ['name' => $item];
						}
						
						$item['code'] = $directory;
					}
					
					$shop_wildb_directory_model->multipleInsert($result, waModel::INSERT_IGNORE);
					$this->response['source'] = $shop_wildb_directory_model->getDirectories($directory);
					$this->response['status'] = true;
				}else{
					waLog::log("При загрузке справочника {$directory} произошла ошибка",shopWildbPlugin::SLUG . '/wbSettings.log');
					$this->response['status'] = false;
					$this->response['error'] = "При загрузке справочника произошла ошибка";
				}
			}
		}
		
		public function syncDirectoryAction() {
			
			$shop_value_id = waRequest::post('shop_value_id',0,waRequest::TYPE_INT);
			$wb_value_id = waRequest::post('wb_value_id',0,waRequest::TYPE_INT);
			$shop_feature_id = waRequest::post('shop_feature_id',0,waRequest::TYPE_INT);
			
			if($shop_value_id){
				
				$wb_sync_directory_model = new shopWildbSyncDirectoryModel;
				
				if($wb_value_id) {
					$wb_sync_directory_model->insert(['shop_value_id' => $shop_value_id, 'wb_value_id' => $wb_value_id, 'shop_feature_id' => $shop_feature_id], true);
				}else{
					$wb_sync_directory_model->deleteByField(['shop_value_id' => $shop_value_id, 'shop_feature_id' => $shop_feature_id]);
				}
			}
			
			$this->response['status'] = true;
		}
		
		public function identifyOrdersAction(){
			
			$orders = [];
			$orderlist = waRequest::post('orders');
			
			if (!empty($orderlist)) {
				
				$orderParamsModel = new shopOrderParamsModel();
				
				$orders = $orderParamsModel->query(
				  "SELECT `order_id`,`value` FROM `shop_order_params` WHERE `name` = 'wb_order_id' AND `order_id` IN (s:ids);",
				  ['ids' => $orderlist]
				)->fetchAll('order_id');
				
			}
			
			$this->response = ['orders' => $orders];
		}
		
		public function createSupplyAction(){
			
			$this->response['result'] = false;
			
			$name = waRequest::post("name");
			
			if($supply_id = shopWildbSupplies::createSupply($name)){
				$this->response['result'] = true;
				$this->response = [
					'supply' => ['id' => $supply_id, 'name' => $name, 'createdAt' => date("d.m.Y H:i:s")],
					'result' => true,
				];
			}
		}
		
		public function deleteSupplyAction(){
			
			$this->response['result'] = false;
			
			$supply_id = waRequest::post("supply_id");
			
			if($supply_id && shopWildbSupplies::deleteSupply($supply_id)){
				$this->response['result'] = true;
			}
		}
		
		public function completeSupplyAction(){
			
			$this->response['result'] = false;
			
			$supply_id = waRequest::post("supply_id");
			
			if($supply_id && shopWildbSupplies::completeSupply($supply_id)){
				$this->response['result'] = true;
			}
		}
		
		public function createTrbxAction(){
			
			$this->response['result'] = false;
			
			$supply_id = waRequest::post("supply_id");
			
			if($trbx = shopWildbTrbx::createTrbx($supply_id)){
				$this->response = [
				  'trbx' => $trbx,
				  'result' => true,
				];
			}
		}
		
		public function deleteTrbxAction(){
			
			$this->response['result'] = false;
			
			$trbx_id = waRequest::post("trbx_id");
			$supply_id = waRequest::post("supply_id");
			
			if($trbx_id && $supply_id && shopWildbTrbx::deleteTrbx($trbx_id, $supply_id)){
				$this->response['result'] = true;
			}
		}
		
		public function removeFromTrbxAction(){
			
			$orders = waRequest::post("orders", []);
			
			foreach ($orders as $order){
				shopWildbOrders::removeFromTrbx($order);
			}
			
			$this->response['result'] = true;
		}
		
		public function canselAssemblyAction(){
			
			if($order_id = waRequest::post("order_id")){
				shopWildbOrders::canselOrder($order_id);
				$this->response['result'] = true;
			}
		}
		
		public function assembliesCompleteAction(){
			
			$orders = waRequest::post("orders", []);
			$supply_id = waRequest::post("supply_id");
			
			if($orders && $supply_id){
				foreach ($orders as $order){
					shopWildbOrders::addOrderSupply($order, $supply_id);
				}
			}
			
			$this->response['result'] = true;
		}
		
		public function addOrdersTrbxAction(){
			
			$orders = waRequest::post("orders", [], waRequest::TYPE_ARRAY_INT);
			$supply_id = waRequest::post("supply_id");
			$trbx_id = waRequest::post("trbx_id");
			
			if($orders && $supply_id && $trbx_id){
				shopWildbOrders::addOrderTrbx($orders, $supply_id, $trbx_id);
				$this->response['result'] = true;
			}
		}
		
		public function sendApiAction(){
			
			$type = waRequest::post('type',null);
			
			if(is_null($type)){
				$this->response['error'] = 'Неправильный запрос';
				$this->response['status'] = false;
				return;
			}
			
			$cli_class = "shopWildbUpdate" . ucfirst($type) . "Cli";
			
			try {
				$cli = new $cli_class;
				$cli->execute();
				$this->response['status'] = true;
			} catch (Exception $e){
				$this->response['error'] = $e->getMessage();
				$this->response['status'] = false;
			}
		}
		
		public function exportAction(){
			
			$action = waRequest::get('data', null);
			
			$tasksModel = new shopWildbTasksModel();
			
			if($action !== 'cansel' && $task = $tasksModel->getByField("status","0")){
				$this->response['error'] = 'Задание на экспорт уже запущено!';
				$this->response['status'] = false;
				return;
			}
			
			
			switch ($action){
				case 'export_cards':
					$result = $tasksModel->insert(['task' => 'new_cards']);
					break;
				case 'update_cards':
					$result = $tasksModel->insert(['task' => 'update_cards']);
					break;
				case 'export_all_images':
					$wb_products = (new shopWildbProductsModel())->getAll();
					$task_upload_images = new shopWildbUploadImagesTask();
					$task_upload_images->setItems($wb_products);
					$result = $task_upload_images->write();
					if($result) $result = $task_upload_images->getID();
					break;
				case 'export_missing_images':
					$result = $tasksModel->insert(['task' => 'upload_missing_images']);
					break;
				case 'download_cards':
					$result = $tasksModel->insert(['task' => 'download_cards']);
					break;
				case 'cansel':
					$result = false;
					$task_id = waRequest::get('task', null);
					
					if($task_id && $result = $tasksModel->deleteById($task_id)) {
						$result = $task_id;
					}
					
					break;
				default:
					$this->response['error'] = 'Некорректное задание';
					$this->response['status'] = false;
					return;
			}
			
			$this->response['status'] = $result !== false;
			$this->response['task'] = $result;
		}
		
		public function checkExportAction(){
			
			$this->response['status'] = false;
			
			$task_id = waRequest::get('task', null);
			
			$tasksModel = new shopWildbTasksModel();
			
			if($task_id && $task = $tasksModel->getById($task_id)){
				$this->response['status'] = boolval($task['status']);
			}
		}
		
		public function getOrderInfoSectionAction(){
			
			$this->response['result'] = false;
			
			$order_id = waRequest::get('order_id');
			
			$shop_order = new shopOrder($order_id);
			
			if($shop_order->getId()){
				$this->response['result'] = true;
				$this->response['order_info'] = shopWildbOrders::getOrderInfoSection($shop_order->getData());
			}
		}
	}