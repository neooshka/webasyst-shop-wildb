<?php
	
	class shopWildbSyncCategoriesModel extends waModel {
		
		protected $table = "shop_wildb_categories_sync";
		
		public function getSyncCategories() {
			return $this->query('
				select shop_category_id, wb_category_id, name, parent_name
				from shop_wildb_categories_sync t1 join shop_wildb_categories t2 on t1.wb_category_id = t2.id
			')->fetchAll('shop_category_id');
		}
		
		public function getSyncCategory(int $shop_category_id){
			return $this->query('
				select wb_category_id, name, parent_name
				from shop_wildb_categories_sync t1 join shop_wildb_categories t2 on t1.wb_category_id = t2.id
                where shop_category_id = i:shop_category_id
			', ['shop_category_id' => $shop_category_id]
			)->fetchAssoc();
		}
		
		public function getSyncCategoriesByFeatureName(string $name){
			return $this->query("
				select distinct wb_category_id FROM shop_wildb_categories_sync cs
				join shop_wildb_features_categories fc using(wb_category_id)
				join shop_wildb_features f on f.id = fc.wb_feature_id
				where f.name = s:name
			", ['name' => $name]
			)->fetchAll();
		}
	}