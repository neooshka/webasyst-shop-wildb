<?php
	
	class shopWildbSyncFeaturesModel extends waModel {
		
		protected $table = "shop_wildb_features_sync";
		
		public function getShopFeatureByWbName(string $name){
			
			return $this->query("
					select t1.wb_feature_id,t3.id,t3.code,t3.name,t3.type from {$this->getTableName()} t1
					join shop_wildb_features t2 on t2.id = t1.wb_feature_id
                    join shop_feature t3 on t3.id = t1.shop_feature_id
					where t2.name = s:name
				",
				['name' => $name],
			)->fetchAssoc();
		}
		
		public function getWbFeaturesByCategoryId(int $wb_category_id){
			
			return $this->query("
				select f.name,fs.shop_feature_id,f.strlen,f.charc_type,f.max_count from shop_wildb_features f
				join shop_wildb_features_categories fc on fc.wb_feature_id = f.id
				join shop_wildb_features_sync fs on fs.wb_feature_id = f.id
				where fc.wb_category_id = i:wb_category_id
			",
				['wb_category_id' => $wb_category_id],
			)->fetchAll();
		}
		
		public function getSyncFeatures(){
			
			$rows = $this->query("
				select fc.wb_category_id,f.name,f.id as wb_feature_id,fs.shop_feature_id,f.strlen,f.charc_type,f.max_count, f.unit from shop_wildb_features f
				join shop_wildb_features_categories fc on fc.wb_feature_id = f.id
				join shop_wildb_features_sync fs on fs.wb_feature_id = f.id
			")->fetchAll();
			
			$result = [];
			
			foreach ($rows as $row){
				$result[$row['wb_category_id']][] = $row;
			}
			
			return $result;
		}
	}