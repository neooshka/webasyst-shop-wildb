<?php
	
	class shopWildbFeaturesCategoriesModel extends waModel {
		
		protected $table = "shop_wildb_features_categories";
		
		public function isRequiredCharacteristic(int $wb_feature_id, int $shop_category_id): bool {
			return $this->query("
				SELECT * FROM {$this->getTableName()} fc
				join shop_wildb_categories_sync cs using(wb_category_id)
				where wb_feature_id = i:wb_feature_id and shop_category_id = i:shop_category_id
			", ['wb_feature_id' => $wb_feature_id, 'shop_category_id' => $shop_category_id]
			)->fetch() ? true : false;
		}
		
		public function getCategoriesByFeatureId(int $feature_id): array {
			
			$rows = $this->getByField('wb_feature_id', $feature_id, true);
			
			$result = [];
			
			foreach ($rows as $row){
				$result[] = $row['wb_category_id'];
			}
			
			return $result;
		}
	}