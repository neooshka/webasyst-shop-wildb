<?php
	
	class shopWildbOrdersModel extends waModel {
		
		protected $table = "shop_wildb_orders";
		
		public function getOrders(){
			return $this->query("
				select wbo.*, p.name as product, s.name as sku, s.product_id, case
				    when wbo.wb_status <> 'waiting' then 'archive'
				    when wbs.closedAt is not null then 'complete'
				    else wbo.seller_status
				    end as type
				from shop_wildb_orders wbo
				left join shop_wildb_supplies wbs on wbo.supply_id = wbs.id
				left join shop_product_skus s on s.id = wbo.item
                left join shop_product p on p.id = s.product_id
                order by type = 'new' desc, type = 'confirm' desc, type = 'complete' desc, type = 'archive' desc, wbo.shop_order_id desc
				limit 1000
			")->fetchAll('id');
		}
		
		public function getOrdersSelection(array $orders){
			return $this->query("
				select wbo.*, p.name as product, s.name as sku, s.product_id
				from shop_wildb_orders wbo
				left join shop_product_skus s on s.id = wbo.item
                left join shop_product p on p.id = s.product_id
				where wbo.id in (s:orders)
			", ['orders' => $orders])->fetchAll('id');
		}
	}