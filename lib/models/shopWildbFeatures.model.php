<?php
	
	class shopWildbFeaturesModel extends waModel {
		
		protected $table = "shop_wildb_features";
		
		public function getFeaturesBySyncCategories(){
			
			$items = $this->query("
				SELECT distinct c.id as category_id,c.name as category_name,f.id as feature_id,f.name as feature_name,f.required,fs.shop_feature_id,f.strlen FROM shop_wildb_categories_sync cs
				join shop_wildb_categories c on c.id = cs.wb_category_id
                join shop_wildb_features_categories fc on fc.wb_category_id = cs.wb_category_id
                join shop_wildb_features f on f.id = fc.wb_feature_id
                left join shop_wildb_features_sync fs on fs.wb_feature_id = f.id
				order by required desc, feature_name
			")->fetchAll();
			
			$result = [];
			
			foreach ($items as $item){
				$result[$item['feature_id']]['name'] = $item['feature_name'];
				$result[$item['feature_id']]['shop_feature_id'] = $item['shop_feature_id'];
				$result[$item['feature_id']]['required'] = $item['required'] ? true : false;
				$result[$item['feature_id']]['strlen'] = $item['strlen'];
				$result[$item['feature_id']]['categories'][$item['category_id']] = $item['category_name'];
			}
			
			return $result;
		}
	}