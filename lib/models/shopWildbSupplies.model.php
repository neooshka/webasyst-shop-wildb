<?php
	
	class shopWildbSuppliesModel extends waModel {
		
		protected $table = "shop_wildb_supplies";
		
		public function getSupplyIDByDate(DateTime $date_supply){
			return $this->query("select id from {$this->getTableName()} where date = ? and done = 0",$date_supply->format('Y-m-d'))->fetchField('id');
		}
		
		public function deleteExcept(array $supplies){
			return $this->exec("delete from {$this->getTableName()} where id not in (s:supplies)", ['supplies' => $supplies]);
		}
		
		public function getCompleteSupplies(){
			return $this->query("
				select * from {$this->getTableName()}
				where closedAt is not null
				  and closedAt <> ''
				  and (scanDt = '' or scanDt is null)
				  and closedAt > date_add(now(), interval -120 hour)
			")->fetchAll('id');
		}
		
		public function getById($value) {
			
			$supply = parent::getById($value);
			
			if($supply){
				$supply['trbx_ids'] = !is_null($supply['trbx_ids']) ? explode(',',$supply['trbx_ids']) : [];
			}
			
			return $supply;
		}
		
		public function updateById($id, $data, $options = null, $return_object = false) {
			
			$data['trbx_ids'] = $data['trbx_ids'] ? implode(',',$data['trbx_ids']) : null;
			
			return parent::updateById($id, $data, $options, $return_object);
		}
	}