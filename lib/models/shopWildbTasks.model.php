<?php
	
	class shopWildbTasksModel extends waModel {
		
		protected $table = "shop_wildb_tasks";
		
		public function getLastTasks(){
			return $this->query(<<<SQL
				select t.id, t.task, t.create_time, t.finish_time, count(i.id) as items from shop_wildb_tasks t
				left join shop_wildb_task_noms i on i.task_id = t.id
				where t.status = 1 and (i.status = 1 or i.status is null)
				group by t.id, t.task, t.create_time, t.finish_time
				order by t.id desc limit 10
			SQL)->fetchAll();
		}
	}