<?php
	
	class shopWildbSyncDirectoryModel extends waModel {
		
		protected $table = "shop_wildb_directory_sync";
		
		public function getByFeatureId(int $feature_id){
			
			$shop_feature_model = new shopFeatureModel;
			$wb_feature_model = new shopWildbDirectoryModel;
						
			$values = $this->getByField('shop_feature_id',$feature_id,true);
			
			$result = [];
			
			foreach ($values as $value){
				$result[$value['shop_value_id']] = $value;
				$result[$value['shop_value_id']]['shop_value'] = $shop_feature_model->getValues([$shop_feature_model->getById($feature_id)])[0]['values'][$value['shop_value_id']];
				$result[$value['shop_value_id']]['wb_value'] = $wb_feature_model->getById($value['wb_value_id'])['name'];
			}
			
			return $result;
		}
		
		public function getSyncValue(int $feature_id, int $shop_value_id){
			
			$wb_feature_model = new shopWildbDirectoryModel;
			
			if($value = $this->getByField(['shop_feature_id' => $feature_id, 'shop_value_id' => $shop_value_id])){
				return $wb_feature_model->getById($value['wb_value_id'])['name'];
			}else{
				return false;
			}
		}
		
		public function getSyncValues(){
			
			$rows = $this->query("
				select ds.shop_feature_id, ds.shop_value_id, d.name from {$this->getTableName()} ds
				join shop_wildb_directory d on d.id = ds.wb_value_id
			")->fetchAll();
			
			$result = [];
			
			foreach ($rows as $row){
				$result[$row['shop_feature_id']][$row['shop_value_id']] = $row['name'];
			}
			
			return $result;
		}
	}