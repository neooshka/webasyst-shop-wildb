<?php
	
	class shopWildbDirectoryModel extends waModel {
		
		protected $table = "shop_wildb_directory";
		
		public function getDirectories(string $directory = null){
			
			$rows = $this->getAll();
			$result = [];
			
			foreach ($rows as $row){
				$result[$row['code']][] = $row;
			}
			
			return is_null($directory) ? $result : $result[$directory];
		}
	}