<?php
	
	class shopWildbApiLogModel extends waModel {
		
		protected $table = "shop_wildb_api_log";
		
		public function getLog(string $task, int $offset = 0, int $limit = 100){
			
			$rows = $this->query('select * from ' . $this->getTableName() . ' where task = s:task order by date_start desc limit i:offset,i:limit',
				[
					'task' => $task,
					'offset' => $offset,
					'limit' => $limit,
				]
			)->fetchAll('id');
			
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::LICENSE_PREMIUM){
			
				$api_log_items_model = new shopWildbApiLogItemsModel;
				$items = $api_log_items_model->getByField('task_id', array_keys($rows), true);
				
				if($items){
					foreach ($items as $item){
						if(!is_array($rows[$item['task_id']]['items'])) $rows[$item['task_id']]['items'] = [];
						$rows[$item['task_id']]['items'][$item['item_id']][$item['name']] = $item['value'];
					}
				}
			}
			
			return $rows;
		}
		
		public function addLog(string $task, $data, $items = null){
			
			$this->cleanLog();
			
			if(is_array($data)){
				$data['task'] = $task;
			}elseif(is_string($data)){
				$data = [
					'task' => $task,
					'message' => $data,
				];
				
			}
			
			if($items && is_array($items)){
				
				if(shopWildbPlugin::chkLcs() === shopWildbPlugin::LICENSE_PREMIUM){
					
					if($task_id = $this->insert($data)){
					
						$items_insert = [];
						
						foreach($items as $item_id => $values){
							foreach ($values as $name => $value){
								$items_insert[] = [
								  'task_id' => $task_id,
								  'item_id' => $item_id,
								  'name' => $name,
								  'value' => $value,
								];
							}
						}
						
						$api_log_items_model = new shopWildbApiLogItemsModel;
						$api_log_items_model->multipleInsert($items_insert);
					}
					
					return $task_id;
				}else{
					$data['items'] = count($items);
				}
			}
			
			return $this->insert($data);
		}
		
		private function cleanLog(){
			
			$deadline = date('Y-m-d H:i:s', time() - 60*60*24*7);
			
			$rows = $this->query("select id from {$this->getTableName()} where date_start < s:deadline", ['deadline' => $deadline])->fetchAll('id');
			$this->deleteById(array_keys($rows));
			
			$api_log_items_model = new shopWildbApiLogItemsModel;
			$api_log_items_model->deleteByField('task_id', array_keys($rows));
			
		}
	}