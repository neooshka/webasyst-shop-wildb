<?php
	
	class shopWildbCategoriesModel extends waModel {
		
		protected $table = "shop_wildb_categories";
		
		public function getSyncCategories(){
			
			$result = $this->query("
				SELECT distinct c.id,c.name FROM shop_wildb_categories_sync cs
				join shop_wildb_categories c on c.id = cs.wb_category_id
			")->fetchAll("id");
			
			foreach ($result as $id => &$item){
				$item = $item['name'];
			}
			
			return $result;
		}
	}