<?php
	
	class shopWildbOrdersCli extends waCliController {
		
		public function execute() {
			
			$settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$api = shopWildbApiMarketplace::getInstance();
			$model = new waModel;
			$workflow = new shopWorkflow();
			$api_log_model = new shopWildbApiLogModel();
			$wb_orders_model = new shopWildbOrdersModel();
			
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::NO_LICENSE){
				$api_log_model->addLog('orders','Отсутствует/истекла лицензия на плагин');
				waLog::log('Отсутствует/истекла лицензия на плагин!',shopWildbPlugin::SLUG . '/wbOrders.log');
				throw new waException('Отсутствует/истекла лицензия на плагин!');
			}
			
			$orders = $api->getOrders();
			
			$log_items = [];
			
			if($orders && !empty($orders['orders'])){
				
				$stockModel = new shopProductStocksModel();
				$productSkusModel = new shopProductSkusModel();
				
				foreach ($orders['orders'] as $order){
					
					$sku_field = ifempty($settings['chrtID_by'],'id');
					$wb_order = $model->query("SELECT * FROM `shop_order_params` WHERE `name` = 'wb_order_id' AND `value` = ? ORDER BY `order_id` DESC LIMIT 1", $order['id'])->fetchAssoc();
					
					$log_items[$order['id']] = [
						'id' => $order['id'],
						'price' => $order['convertedPrice'] / 100,
						'createdAt' => $order['createdAt']->format("d.m.Y H:i:s"),
					];
					
					if(empty($wb_order)) {
						
						foreach ($order['skus'] as $wb_sku){
							$sku = $productSkusModel->getByField($sku_field, $wb_sku);
							if($sku) break;
						}
						
						if(!$sku) continue;
						
						$log_items[$order['id']]['sku_id'] = $sku['id'];
						
						$stocks = $stockModel->getBySkuId($sku['id']);
						$stock_id = 0;
						
						if(isset($stocks[$sku['id']])){
							$stock_id = array_key_first($stocks[$sku['id']]);
						}
						
						$orderData = [
							'currency' => 'RUB',
							'items' => [
								[
									'product_id' => $sku['product_id'],
									'sku_id' => $sku['id'],
									'type' => 'product',
									'price' => $order['convertedPrice'] / 100,
									'quantity' => 1,
									'goodsItemIndex' => 0,
									'currency' => 'RUB',
									'stock_id' => $stock_id,
									'total_discount' => $sku['price'] - $order['convertedPrice'] / 100,
								],
							],
							'params' => [
								'sales_channel' => 'Wildberries',
								'storefront' => 'Wildberries',
								'referer_host' => 'wildberries.ru',
								'wb_order_id' => $order['id'],
								'tracking_number' => $order['id'],
								'shipping_currency' => 'RUB',
								'shipping_currency_rate' => '1',
								'shipping_name' => 'Доставка Wildberries',
								'shipping_rate_id' => 0,
							],
							'comment' => 'Wildberries #' . $order['id'] . " от " . $order['createdAt']->format("d.m.Y H:i:s"),
							'discount' => 0,
							'contact_id' => ifset($settings,'contact_id', 0),
							'shipping' => 0,
						];
						
						$plugin_model = new shopPluginModel();
						
						if(!empty($settings['shipping_new'])){
							
							$shipping = $plugin_model->getPlugin($settings['shipping_new'], shopPluginModel::TYPE_SHIPPING);
							
							if($shipping){
								$orderData['params']['shipping_plugin'] = $shipping['plugin'];
								$orderData['params']['shipping_id'] = $shipping['id'];
								$orderData['params']['shipping_name'] = $shipping['name'];
							}
						}
						if(!empty($settings['payment_new'])){
							
							$payment = $plugin_model->getPlugin($settings['payment_new'], shopPluginModel::TYPE_PAYMENT);
							
							if($payment){
								$orderData['params']['payment_plugin'] =$payment['plugin'];
								$orderData['params']['payment_id'] = $payment['id'];
								$orderData['params']['payment_name'] = $payment['name'];
							}
						}
						
						$curError = ini_get('error_reporting');
						ini_set('error_reporting', E_ALL & ~E_NOTICE);
						
						$shop_order = new shopOrder($orderData);
						
						try {
							
							$waSettingsModel = new waAppSettingsModel;
							
							$backup_settings = $waSettingsModel->get('shop', 'ignore_stock_count');
							$waSettingsModel->set('shop', 'ignore_stock_count', 1);
							
							$order_id = $shop_order->save();
							
							$waSettingsModel->set('shop', 'ignore_stock_count', $backup_settings);
							
						} catch (waException $ex) {
							waLog::dump(['orderData' => $orderData, 'errors' => $shop_order->errors(), 'exception' => $ex->getMessage()], shopWildbPlugin::SLUG . '/wbOrders.log');
						}
						
						if($order_id) {
							$orderLog = new shopOrderLogModel();
							$orderLog->insert([
								'order_id' => $shop_order['id'],
								'action_id' => '',
								'datetime' => date('Y-m-d H:i:s'),
								'before_state_id' => '',
								'after_state_id' => '',
								'text' => '<i class="icon16 wb"></i> Номер заказа WB <b>' . $order['id'] . '</b>',
							]);
							
							$wb_orders_model->insert([
								'id' => $order['id'],
								'shop_order_id' => $shop_order['id'],
								'date_order' => $order['createdAt']->format("Y-m-d H:i:s"),
								'item' => $sku['id'],
								'sum' => $order['convertedPrice'] / 100,
								'seller_status' => 'new',
								'wb_status' => 'waiting',
							]);
						}
						// Действие после создание заказа
						if(!empty($order_id) && !empty($settings['workflow_new'])) {
							$workflow->getActionById($settings['workflow_new'])->run($shop_order['id']);
						}
						
						ini_set('error_reporting', $curError);
					}
				}
			}
			
			$api_log_model->addLog('orders',$api->getExchangeReport(), $log_items);
			
			// Обновим статусы заказов
			$sql = "
				select o.id, o.state_id, op.value, wbo.seller_status, wbo.wb_status, wbo.date_status from shop_order o
				join shop_order_params op on op.order_id = o.id and op.name = 'wb_order_id'
                left join shop_wildb_orders wbo on wbo.shop_order_id = o.id and wbo.id = op.value
				where op.value is not null
			";
			
			$params = [];
			
			if(is_array($settings['check_orders_witchout_statuses']) && !empty($settings['check_orders_witchout_statuses'])){
				$sql .= " and o.state_id not in (s:statuses)";
				$params['statuses'] = $settings['check_orders_witchout_statuses'];
			}
			
			$sql .= "
				order by 1 desc
				limit 1000
			";
			
			$orders = $model->query($sql, $params)->fetchAll('value');
			
			$log_items = [];
			
			if($orders) {
				
				$wb_orders = [];
				$shop_orders = [];
				
				foreach ($orders as $o) {
					$wb_orders[] = (int)$o['value'];
					$shop_orders[(int)$o['value']] = $o;
				}
				
				$statuses = $api->getOrdersStatuses(['orders' => $wb_orders]);
				
				
				if($statuses) {
					
					$data = [];
					
					$wb_states = shopWildbOrders::getWbStatuses('wbStatus');
					$sup_states = shopWildbOrders::getWbStatuses('supplierStatus');
					
					foreach ($statuses['orders'] as $wb_order) {
						
						$order_action = false;
						
						if(
							isset($wb_states[$wb_order['wbStatus']])
							&& !empty($settings[$wb_states[$wb_order['wbStatus']]])
							&& $wb_order['wbStatus'] !== $shop_orders[$wb_order['id']]['wb_status']
						){
							$order_action = $workflow->getActionById($settings[$wb_states[$wb_order['wbStatus']]]);
						}elseif(
							isset($sup_states[$wb_order['supplierStatus']])
							&& !empty($settings[$sup_states[$wb_order['supplierStatus']]])
							&& $wb_order['supplierStatus'] !== $shop_orders[$wb_order['id']]['seller_status']
						){
							$order_action = $workflow->getActionById($settings[$sup_states[$wb_order['supplierStatus']]]);
						}
						
						// change status for shop_orders
						if($order_action && $shop_orders[$wb_order['id']]['state_id'] !== shopWorkflow::getConfig()['actions'][$order_action->getId()]["state"]){
							$order_action->run($shop_orders[$wb_order['id']]['id']);
						}
						
						// change status for wb_orders
						if($wb_order['wbStatus'] !== $shop_orders[$wb_order['id']]['wb_status'] || $wb_order['supplierStatus'] !== $shop_orders[$wb_order['id']]['seller_status']){
							$data[] = [
								'id' => $wb_order['id'],
								'shop_order_id' => $shop_orders[$wb_order['id']]['id'],
								'seller_status' => $wb_order['supplierStatus'],
								'wb_status' => $wb_order['wbStatus'],
								'date_status' => date('Y-m-d H:i:s'),
							];
						}
						
						$log_items[$wb_order['id']] = [
							'id' => $wb_order['id'],
							'shop_order_id' => $shop_orders[$wb_order['id']]['id'],
							'seller_status' => $wb_order['supplierStatus'],
							'wb_status' => $wb_order['wbStatus'],
						];
					}
					
					$wb_orders_model->multipleInsert($data,['seller_status','wb_status','date_status']);
				}
			}
			
			$api_log_model->addLog('orders_statuses',$api->getExchangeReport(), $log_items);
			
			// Обновим информацию о поставках
			$supplies = $api->getSupplies();
			if($supplies){
				
				$supplies_model = new shopWildbSuppliesModel;
				$supplies_model->multipleInsert(array_values($supplies),['done', 'createdAt', 'closedAt', 'scanDt', 'name', 'cargoType']);
				$supplies_model->deleteExcept(array_keys($supplies));
				
				// Обновим информацию о заказах и коробах в незакрытых и непринятых поставках
				$data = [];
				$supplies = array_merge($supplies_model->getByField('done', 0, 'id'), $supplies_model->getCompleteSupplies());
				
				foreach ($supplies as &$supply){
					
					$api_orders = $api->getSupplyOrders($supply['id']);
					$orders = $api_orders ? $api_orders['orders'] : [];
					
					foreach ($orders as $wb_order){
						$data[$wb_order['id']]['supply_id'] = $supply['id'];
					}
					
					$api_trbx = $api->getSupplyTrbx($supply['id']);
					$api_trbx = $api_trbx ? $api_trbx['trbxes'] : [];
					$trbx_ids = [];
					
					foreach ($api_trbx as $box){
						$trbx_ids[] = $box['id'];
						foreach ($box['orders'] as $order){
							$data[$order]['trbx_id'] = $box['id'];
						}
					}
					
					$supply['trbx_ids'] = $trbx_ids ? implode(',',$trbx_ids) : null;
				}
				
				unset($supply);
				
				$orders_model = new shopWildbOrdersModel;
				$orders = $orders_model->getById(array_keys($data));
				
				foreach ($orders as &$order){
					$order['supply_id'] = $data[$order['id']]['supply_id'];
					$order['trbx_id'] = $data[$order['id']]['trbx_id'] ?? null;
				}
				
				unset($order);
				
				$orders_model->multipleInsert(array_values($orders), ['supply_id','trbx_id']);
				$supplies_model->multipleInsert(array_values($supplies),['trbx_ids']);
			}
		}
	}