<?php
	
	class shopWildbUpdatePricesCli extends waCliController {
		
		public function execute() {
			
			$settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$api_log_model = new shopWildbApiLogModel();
			
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::NO_LICENSE){
				$api_log_model->addLog('prices','Отсутствует/истекла лицензия на плагин');
				waLog::log('Отсутствует/истекла лицензия на плагин!',shopWildbPlugin::SLUG . '/wbUpdatePrices.log');
				return;
			}
			
			$api_prices = shopWildbApiPrices::getInstance();
			$cards = $api_prices->getPrices();
			
			if(!$cards) return true;
			
			$wb_products_result = [];
			$wb_products = (new shopWildbProductsModel)->getByField('nmID', array_keys($cards), true);
			
			if($settings['stock_prices_by_filter']){
				$wb_products = shopWildbProductsSync::filterByProducts($wb_products);
			}
			
			$product_ids = [];
			foreach ($wb_products as $nom){
				$product_ids[$nom['product_id']] = $nom['product_id'];
				$wb_products_result[$nom['product_id']][$nom['nmID']] = $nom + $cards[$nom['nmID']];
			}
			
			$products = (new shopProductModel)->getById($product_ids);
			shopWildbFeaturesHelper::addProductFeatures($products);
			$skus = shopWildbSkusHelper::getProductsSkus($products);
			
			$custom_prices = false;
			if($custom_prices = shopWildbSkuPriceHelper::getCustomPrices($skus)){
				$skus = $custom_prices;
				$custom_prices = true;
			}
			
			$wb_prices = [];
			
			foreach ($skus as $sku) {
				
				$result_sku = $sku + $products[$sku['product_id']];
				
				$price = shopWildbSkuPriceHelper::getSkuPrice($result_sku, $custom_prices);
				
				$discount_rule = shopWildbSkuPriceHelper::getPriceRule($result_sku,'wb_price_rules');
				$discount = $discount_rule ? (int) $discount_rule['discount'] : 0;
				
				foreach($wb_products_result[$sku['product_id']] as $nmID => $nom){
					if($nom['price'] <> $price || $nom['discount'] <> $discount) {
						$wb_prices[$nmID] = ['nmID' => $nmID, 'price' => $price, 'discount' => $discount];
					}
				}
			}
			
			if($wb_prices){
				$api_prices->setPrices(array_values($wb_prices));
				$api_log_model->addLog('prices',$api_prices->getExchangeReport(), $wb_prices);
			}else{
				$api_log_model->addLog('prices','Нет данных для обновления');
			}
		}
	}
