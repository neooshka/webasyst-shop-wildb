<?php
	
	class shopWildbUpdateStocksCli extends waCliController {
		
		public function execute() {
			
			$settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$model = new waModel;
			$api_log_model = new shopWildbApiLogModel();
			
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::NO_LICENSE){
				$api_log_model->addLog('stocks','Отсутствует/истекла лицензия на плагин');
				waLog::log('Отсутствует/истекла лицензия на плагин!',shopWildbPlugin::SLUG . '/WbUpdateStocksCli.log');
				return;
			}
			
			if(empty($settings['wb_stock_id'])){
				$api_log_model->addLog('stocks','Не указан ID склада WB');
				waLog::log('В настройках не указан ID склада WB, выгрузка остатков невозможна!',shopWildbPlugin::SLUG . '/WbUpdateStocksCli.log');
				return;
			}
			
			if($settings['wb_debug']){
				waLog::log('shopWildbUpdateStocksCli',shopWildbPlugin::SLUG . '/WbUpdateStocksCli.log');
			}
			
			$cards = shopWildbApiContent::getInstance()->getCards(-1, true);
			
			if(!$cards['cards']) return true;
			
			$wb_stocks = [];
			$log_items = [];
			
			$sku_field = $settings['chrtID_by'];
			$sku_field = $model->escape('sk.' . $sku_field);
			$stock_type = (int) $settings['shop_stocks'];
			$stock_rules = $settings['stock_rules'];
			
			if($settings['stock_prices_by_filter']){
				$cards['cards'] = shopWildbProductsSync::filterBySkus($cards['cards']);
			}
			
			$params = [
				'skus' => $cards['cards'],
			];
			
			if($stock_type === 1){
				$sql = "
					select sk.id, {$sku_field} as sku_id, sk.count as count from shop_product_skus sk
					where {$sku_field} in (s:skus)";
			}else{
				$sql = "
					select sk.id, {$sku_field} as sku_id, sum(st.count) as count from shop_product_stocks st
					join shop_product_skus sk on sk.id = st.sku_id
					where {$sku_field} in (s:skus)";
				if(is_array($settings['shop_stocks_id']) && !empty($settings['shop_stocks_id'])) {
					$sql .= " and st.stock_id in (i:stocks_id)";
					$params['stocks_id'] = $settings['shop_stocks_id'];
				}
				
				$sql .= " group by {$sku_field}";
			}
			
			$skus_stock = $model->query($sql, $params)->fetchAll();
			
			if($settings['wb_debug']){
				waLog::dump(['sql' => $sql, 'count_skus' => count($skus_stock)],shopWildbPlugin::SLUG . '/WbUpdateStocksCli.log');
			}
			
			foreach ($skus_stock as $sku_stock) {
				
				$amount = (int) $sku_stock['count'];
				
				if(is_null($sku_stock['count']) || $sku_stock['count'] == "null"){
					$amount = (int) $settings['stock_null_count'];
				}
				
				if($stock_rules){
					foreach ($stock_rules as $rule){
						if($rule['operand'] === ">" && $amount > $rule['right_argument']){
							$amount = (int) $rule['result'];
							break;
						}elseif($rule['operand'] === "<" && $amount < $rule['right_argument']){
							$amount = (int) $rule['result'];
							break;
						}elseif($rule['operand'] === "=" && $amount == $rule['right_argument']){
							$amount = (int) $rule['result'];
							break;
						}
					}
				}
				
				if($amount < 0) $amount = 0;
				
				$wb_stocks[] = ['sku' => $sku_stock['sku_id'], 'amount' => $amount];
				$log_items[$sku_stock['id']] = ['sku' => $sku_stock['id'], 'amount' => $amount];
			}
			$api_stocks = shopWildbApiMarketplace::getInstance();
			$api_stocks->setStocks($wb_stocks);
			$api_log_model->addLog('stocks',$api_stocks->getExchangeReport(), $log_items);
		}
	}