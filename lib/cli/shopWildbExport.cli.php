<?php
	
	class shopWildbExportCli extends waCliController {
		
		private $settings;
		
		/** @var shopWildbTasksModel */
		private $task_model;
		
		public function execute() {
			
			$this->settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::NO_LICENSE){
				
				if($this->settings['wb_debug']){
					waLog::log("Приложение не лицензировано",shopWildbPlugin::SLUG . '/WbExportCli.log');
				}
				
				return;
			}
			
			if(empty($this->settings['api_token'])){
				
				if($this->settings['wb_debug']){
					waLog::log("Не заполнен api_token",shopWildbPlugin::SLUG . '/WbExportCli.log');
				}
				
				return;
			}
			
			$this->task_model = new shopWildbTasksModel();
			
			$task = $this->getActiveTask();
			
			if(!$task || $task['locked']){
				return;
			}
			
			$Task = array_map('ucfirst', explode('_', $task['task']));
			$Task = 'shopWildb'.implode('',$Task).'Task';
			
			if(!class_exists($Task)){
				waLog::log("Ошибка запуска задачи. Неизвестная задача: " . $task['task'],shopWildbPlugin::SLUG . '/WbExportCli.log');
				return;
			}
			
			/** @var shopWildbTaskController $task */
			$task = new $Task($task);
			
			if(!($task instanceof shopWildbTaskInterface)){
				waLog::log("Ошибка запуска задачи. Задача некорректна",shopWildbPlugin::SLUG . '/WbExportCli.log');
				return;
			}
			
			if($this->lockTask($task->getID())){
				$task->start();
				$this->unlockTask($task->getID());
			}
		}
		
		private function getActiveTask(){
			
			$tasks = $this->task_model->getByField("status","0","id");
			
			if($this->settings['wb_debug']){
				waLog::dump(['tasks' => $tasks],shopWildbPlugin::SLUG . '/WbExportCli.log');
			}
			
			return reset($tasks);
		}
		
		private function lockTask(int $id){
			return $this->task_model->updateById($id, ['locked' => 1]);
		}
		
		private function unlockTask(int $id){
			return $this->task_model->updateById($id, ['locked' => 0]);
		}
	}