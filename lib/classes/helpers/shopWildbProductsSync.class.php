<?php
	
	class shopWildbProductsSync {
		
		public static function filterByProducts(array $products){
			
			$ids = [];
			
			foreach ($products as $product){
				if(isset($product['product_id'])){
					$ids[$product['product_id']] = $product['product_id'];
				}
			}
			
			if(!$ids) return [];
			
			$settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$params = ['ids' => $ids];
			
			if($settings['shop_products'] == 2 && $settings['shop_products_type'] == 2 &&
			  isset($settings['shop_products_sets']) && is_array($settings['shop_products_sets']) && !empty($settings['shop_products_sets'])){
				$sql = "select p.id from shop_product p join shop_set_products s on s.product_id = p.id where p.id in (s:ids)";
			}else {
				$sql = "select p.id from shop_product p join shop_category_products cp on cp.product_id = p.id where p.id in (s:ids)";
			}
			
			if($settings['shop_products'] == 2){
				if($settings['shop_products_type'] == 1){
					if(isset($settings['shop_products_categories']) && is_array($settings['shop_products_categories']) && !empty($settings['shop_products_categories'])) {
						$sql .= " and cp.category_id in (i:shop_products_categories)";
						$params['shop_products_categories'] = $settings['shop_products_categories'];
					}
				}elseif(isset($settings['shop_products_sets']) && is_array($settings['shop_products_sets']) && !empty($settings['shop_products_sets'])){
					$sql .= " and s.set_id in (s:shop_products_sets)";
					$params['shop_products_sets'] = $settings['shop_products_sets'];
				}
			}
			
			$model = new waModel;
			$products_filtered = $model->query($sql, $params)->fetchAll('id');
			
			$result = [];
			
			foreach ($products as $key => $value){
				if(isset($value['product_id']) && isset($products_filtered[$value['product_id']])){
					$result[$key] = $value;
				}
			}
			
			return $result;
		}
		
		
		public static function filterBySkus(array $skus){
			
			$model = new waModel;
			$chrtID_by = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings('chrtID_by');
			$sku_field = $model->escape($chrtID_by);
			
			$products = $model->query("select product_id, {$sku_field} as sku_id from shop_product_skus where {$sku_field} in (s:skus)", ['skus' => $skus])->fetchAll('sku_id');
			$products_filtered = self::filterByProducts($products);
			
			return array_keys($products_filtered);
		}
		
		public static function getProductSkusStock(){
			
			$model = new waModel;
			$settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$sku_field = $model->escape($settings['chrtID_by']);
			$stock_type = (int) $settings['shop_stocks'];
			$params = [];
			
			if($stock_type === 1){
				$sql = "select ps.id, ps.{$sku_field} as sku_id, ps.count from shop_wildb_products wbp
						join shop_product p on p.id = wbp.product_id
						join shop_product_skus ps on ps.product_id = wbp.product_id";
			}else{
				$sql = "select ps.id, ps.{$sku_field} as sku_id, sum(st.count) as count from shop_wildb_products wbp
						join shop_product p on p.id = wbp.product_id
						join shop_product_skus ps on ps.product_id = wbp.product_id
						join shop_product_stocks st on st.sku_id = ps.id";
				if(is_array($settings['shop_stocks_id']) && !empty($settings['shop_stocks_id'])) {
					$sql .= " and st.stock_id in (i:stocks_id)";
					$params['stocks_id'] = $settings['shop_stocks_id'];
				}
			}

			// filter by categories or sets depending settings
			if($settings['stock_prices_by_filter']){
				if($settings['shop_products'] == 2 && $settings['shop_products_type'] == 2 &&
				  isset($settings['shop_products_sets']) && is_array($settings['shop_products_sets']) && !empty($settings['shop_products_sets'])){
					$sql .= "
							join shop_set_products sp on sp.product_id = p.id";
				}else{
					$sql .= "
							join shop_category_products cp on cp.product_id = p.id";
				}
				
				if($settings['shop_products'] == 2){
					if($settings['shop_products_type'] == 1){
						if(isset($settings['shop_products_categories']) && is_array($settings['shop_products_categories']) && !empty($settings['shop_products_categories'])) {
							$sql .= "
									join shop_category_products cp on cp.product_id = p.id
									where cp.category_id in (i:shop_products_categories)";
							$params['shop_products_categories'] = $settings['shop_products_categories'];
						}
					}elseif(isset($settings['shop_products_sets']) && is_array($settings['shop_products_sets']) && !empty($settings['shop_products_sets'])){
						$sql .= "
								join shop_set_products sp on sp.product_id = p.id
								where s.set_id in (s:shop_products_sets)";
						$params['shop_products_sets'] = $settings['shop_products_sets'];
					}
				}
			}
			
			if($stock_type === 2){
				$sql .= "
						group by id, sku_id";
			}
			
			return $model->query($sql, $params)->fetchAll();
		}
	}