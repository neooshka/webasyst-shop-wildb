<?php
	
	
	class shopWildbSkusHelper {
	
		public static function getProductsSkus(array $products){
			
			if(!$products) return [];
			
			$skus = (new shopProductSkusModel())->getByField('product_id', array_keys($products), 'id');
			$rows = (new shopProductFeaturesModel())->getByField('sku_id', array_keys($skus), true);
			$features = (new shopFeatureModel)->getAll('id');
			
			$type_values = [];
			
			foreach ($rows as $row) {
				
				$f = $features[$row['feature_id']];
				$type = preg_replace('/\..*$/', '', $f['type']);
				
				if ($type == shopFeatureModel::TYPE_BOOLEAN) {
					$type_values[$type][] = $row['feature_id'];
				} else {
					$type_values[$type][] = $row['feature_value_id'];
				}
			}
			
			foreach ($type_values as $type => $value_ids) {
				
				$values_model = shopFeatureModel::getValuesModel($type);
				
				if ($type == shopFeatureModel::TYPE_BOOLEAN) {
					$type_values[$type] = $values_model->getValues('feature_id', array_unique($value_ids));
				} else if (is_object($values_model)) {
					$type_values[$type] = $values_model->getValues('id', $value_ids);
				}
			}
			
			$composite_feature_values = [];
			
			foreach ($rows as $row) {
				
				$f = $features[$row['feature_id']];
				$type = preg_replace('/\..*$/', '', $f['type']);
				
				if (!$type_values[$type][$row['feature_id']][$row['feature_value_id']]) {
					continue;
				}
				
				if (null === $f['parent_id']) {
					$skus[$row['sku_id']][$f['code']] = $type_values[$type][$row['feature_id']][$row['feature_value_id']];
					if(isset($products[$skus[$row['sku_id']]['product_id']]['features_selectable'][$f['id']])) {
						$skus[$row['sku_id']]['sku_features_raw'][$f['id']] = $row['feature_value_id'];
					}
				} else {
					$composite_feature_values[$row['sku_id']][$f['parent_id']][$f['code']] = $type_values[$type][$row['feature_id']][$row['feature_value_id']];
					if(isset($products[$skus[$row['sku_id']]['product_id']]['features_selectable'][$f['id']])) {
						$skus[$row['sku_id']]['sku_features_raw'][$f['parent_id']][$f['id']] = $row['feature_value_id'];
					}
				}
			}
			
			foreach($composite_feature_values as $sku_id => $sku_composite_features) {
				foreach($sku_composite_features as $parent_feature_id => $subfeature_values) {
					$count_before = count($subfeature_values);
					$parent_feature_code = rtrim(key($subfeature_values), '0123456789.');
					$value = new shopCompositeValue($parent_feature_code, $subfeature_values);
					if ($count_before != count($subfeature_values)) {
						$skus[$sku_id][$parent_feature_code] = $value;
					}
				}
			}
			
			return $skus;
		}
	}