<?php
	
	class shopWildbDirectoryesHelper {
		
		private static $directories = [
			'kinds' => 'Пол',
			'colors' => 'Цвет',
			'countries' => 'Страна Производства',
			'seasons' => 'Сезон',
			'tnved' => 'ТНВЭД',
			'vat' => 'Ставка НДС',
		];
		
		public static function getDirectories(){
			return array_keys(self::$directories);
		}
		
		public static function getFeatureNameByDirectory(string $directory){
			return self::$directories[$directory];
		}
		
		public static function getAllFeatureNames(){
			return self::$directories;
		}
		
	}