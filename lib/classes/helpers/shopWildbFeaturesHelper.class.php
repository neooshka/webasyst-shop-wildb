<?php
	
	
	class shopWildbFeaturesHelper {
	
		public static function addProductFeatures(array &$products){
			
			if(!$products) return;
			
			$features_model = new shopProductFeaturesModel();
			
			$features = [];
			
			$sql = "SELECT pf.*, f.code, f.type, f.multiple
                FROM {$features_model->getTableName()} pf
                    JOIN shop_feature f
                        ON pf.feature_id = f.id
                WHERE pf.product_id in (i:ids)
                    AND pf.sku_id IS NULL";
			
			$data = $features_model->query($sql, ['ids' => array_keys($products)]);
			
			$storages = [];
			
			foreach ($data as $row) {
				
				$type = preg_replace('/\..*$/', '', $row['type']);
				
				if($type == shopFeatureModel::TYPE_DIVIDER) continue;
				
				$features[$row['code']] = [
				  'type'       => $row['type'],
				  'feature_id' => $row['feature_id'],
				  'multiple'   => $row['multiple'],
				];
				
				$products[$row['product_id']][$row['code']][$row['feature_value_id']] = $row['feature_value_id'];
				$products[$row['product_id']]['product_features_raw'][$row['feature_id']] = $row['feature_value_id'];
				
				switch ($type) {
					case shopFeatureModel::TYPE_BOOLEAN:
						/** @var shopFeatureValuesBooleanModel $model */
						$model = shopFeatureModel::getValuesModel($type);
						$values = $model->getValues('id', $row['feature_value_id']);
						$feature_values = reset($values);
						$products[$row['product_id']][$row['code']] = $feature_values[$row['feature_value_id']];
						
						break;
					default:
						$storages[$type][$row['feature_value_id']] = $row['feature_value_id'];
				}
			}
			
			$feature_values = [];
			
			foreach ($storages as $type => $value_ids) {
				if ($type === shopFeatureModel::TYPE_2D || $type === shopFeatureModel::TYPE_3D) continue;
				
				if ($model = shopFeatureModel::getValuesModel($type)) {
					$feature_values += $model->getValues('id', $value_ids);
				}
			}
			
			foreach ($products as $product_id => $product){
				foreach ($product as $code => $v_ids){
					if (isset($features[$code]) && is_array($v_ids)) {
						
						$f = $features[$code];
						
						foreach ($v_ids as $v_id){
							
							$value = $feature_values[$f['feature_id']][$v_id];
							
							if(empty($f['multiple'])){
								$products[$product_id][$code] = $value;
							}else{
								$products[$product_id][$code][$v_id] = $value;
							}
						}
					}
				}
				
				$composite = array_filter(array_keys($products[$product_id]), wa_lambda('$a', 'return preg_match("/\.[0-2]$/",$a);'));
				$composite = array_unique(preg_filter('#\.[0-3]#', '', $composite));
				
				foreach ($composite as $code) {
					$code = preg_replace('/\.0$/', '', $code);
					$products[$product_id][$code] = new shopCompositeValue($code, $products[$product_id]);
				}
			}
			
			$featres_selectable_model = new shopProductFeaturesSelectableModel();
			$result = $featres_selectable_model->getByField('product_id',array_keys($products), true);
			
			foreach ($result as $item){
				$products[$item['product_id']]['features_selectable'][$item['feature_id']] = $item['feature_id'];
			}
		}
	}