<?php
	
	class shopWildbSkuPriceHelper {
		
		private $settings;
		private $shop_features;
		
		private static $instance = null;
		
		/** @return static */
		public static function getInstance(){
			
			if (is_null(static::$instance)) {
				static::$instance = new static();
			}
			
			return static::$instance;
		}
		
		public function __construct(){
			
			$this->settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$this->shop_features = (new shopFeatureModel)->getAll('id');
		}
		
		public static function getPriceRule(array $sku, string $type) {
			
			$_this = static::getInstance();
			
			$price_rules = ifempty($_this->settings[$type],[]);
			
			foreach ($price_rules as $rule){
				
				$connector = array_key_first($rule['conditions']);
				
				$matches = 0;
				
				foreach ($rule['conditions'][$connector] as $condition){
					
					if($condition['feature'] === 'price'){
						
						if($condition['operand'] === '>' && $sku['price'] > (int) $condition['value']){
							$matches++;
						}elseif($condition['operand'] === '<' && ($sku['price'] <= $condition['value'] || !$condition['value'])){
							$matches++;
						}elseif($condition['operand'] === '=' && $sku['price'] == $condition['value']){
							$matches++;
						}
					}elseif($condition['feature'] === 'weight'){
						
						if(!$_this->settings['wb_price_rules_weight'] || !isset($_this->shop_features[$_this->settings['wb_price_rules_weight']])) continue;
						
						$feature_code = $_this->shop_features[$_this->settings['wb_price_rules_weight']]['code'];
						
						$weight = (int) ($sku[$feature_code]['value'] ?? $sku[$feature_code] ?? null);
						
						if(!$weight) continue;
						
						if($condition['operand'] === '>' && $weight > (int) $condition['value']){
							$matches++;
						}elseif($condition['operand'] === '<' && ($weight <= (int) $condition['value'] || !$condition['value'])){
							$matches++;
						}elseif($condition['operand'] === '=' && $weight == $condition['value']){
							$matches++;
						}
					}elseif($condition['feature'] === 'category_id'){
						
						if(!$condition['value']) continue;
						
						foreach (explode(',',$condition['value']) as $cat){
							if($sku['category_id'] == $cat){
								$matches++;
								break;
							}
						}
					}
					
					if(($connector === 'or' && $matches > 0) || ($connector === 'and' && $matches === count($rule['conditions'][$connector]))) {
						return $rule;
					}
				}
			}
			
			return false;
		}
		
		public static function makeMarkup(int &$price, array $markup) {
			
			if($markup['type'] === 'fix'){
				$price += (int) $markup['value'];
			}elseif($markup['type'] === 'percent'){
				$price *= 1 + (int) $markup['value'] / 100;
			}
		}
		
		public static function getCustomPrices($skus){
			
			foreach (shopWildbPlugin::getPricePlugins() as $plugin){
				
				/** @var shopWildbPricePluginController $price_plugin */
				$price_plugin = new $plugin;
				
				if($price_plugin->pluginInstalled() && $custom_prices = $price_plugin->getPrices($skus, ifempty(static::getInstance()->settings['wb_old_price'],'price'))) {
					return $custom_prices;
				}
			}
			
			return false;
		}
		
		public static function getSkuPrice(&$sku, $custom_prices): int {
			
			$_this = static::getInstance();
			$price_field = ifempty($_this->settings['wb_old_price'],'price');
			
			if(is_numeric($price_field)){
				
				$feature_code = $_this->shop_features[$price_field]['code'];
				$price = $sku[$feature_code]['value'] ?? $sku[$feature_code] ?? null;
				$sku['price'] = is_numeric($price) ? $price : 0;
				
			}elseif(!$custom_prices){
				$sku['price'] = $sku[$price_field];
			}
			
			if(!isset($sku['price']) || !$sku['price']) return false;
			
			$price_rule = static::getPriceRule($sku,'wb_old_price_rules');
			$price = $sku['price'];
			
			if($price_rule){
				static::makeMarkup($price,$price_rule['markup1']);
				static::makeMarkup($price,$price_rule['markup2']);
			}
			
			return (int) $price;
		}
	}