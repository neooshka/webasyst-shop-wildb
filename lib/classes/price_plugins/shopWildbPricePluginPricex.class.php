<?php
	
	class shopWildbPricePluginPricex extends shopWildbPricePluginController{
		
		protected $plugin_id = 'pricex';
		
		public function getPriceFields() {
			
			$context = shopPricexPlugin::getContext();
			
			/** @var shopPricexPriceTypeRepository $price_type_repository */
			$price_type_repository = $context->getPrimaryBean(shopPricexPriceTypeRepository::class);
			
			$prices = $price_type_repository->findAll();
			$fields = [];
			
			foreach ($prices as $price){
				$pr = $price->toArray();
				$fields[] = ['id' => 'pricex_plugin_' . $pr['id'], 'name' => $pr['name']];
			}
			
			return $fields;
		}
		
		public function getPrices($skus, $field) {
			
			if(substr_count($field,'pricex_plugin_') > 0){
				
				$price_id = substr($field,14);
				
				$context = shopPricexPlugin::getContext();
				
				/** @var shopPricexPriceService $price_service */
				$price_service = $context->getPrimaryBean(shopPricexPriceService::class);
				
				/** @var shopPricexPriceTypeRepository $price_type_repository */
				$price_type_repository = $context->getPrimaryBean(shopPricexPriceTypeRepository::class);
				
				/** @var shopPricexPriceType $price_type */
				$price_type = $price_type_repository->findById($price_id);
				$prices = $price_service->getSkuPrices($skus,[],$price_type);
				
				foreach ($prices as $sku_id => $price){
					if(isset($skus[$sku_id]) && $price['price'] > 0){
						$skus[$sku_id]['price'] = $price['price'];
					}
				}
				
				return $skus;
			}
			
			return false;
		}
	}