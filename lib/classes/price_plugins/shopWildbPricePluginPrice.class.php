<?php
	
	class shopWildbPricePluginPrice extends shopWildbPricePluginController{
		
		protected $plugin_id = 'price';
		
		public function getPriceFields() {
			
			$price_model = new shopPricePluginModel();
			$price_fields = $price_model->getAll();
			
			$fields = [];
			
			foreach ($price_fields as $price_field){
				$fields[] = ['id' => 'price_plugin_' . $price_field['id'], 'name' => $price_field['name']];
			}
			
			return $fields;
		}
		
		public function getPrices($skus, $field) {
			
			if(substr_count($field,'price_plugin_') > 0){
				$price_id = substr($field,13);
				return $this->plugin->prepareSkus($skus,null,null,null, $price_id);
			}
			
			return false;
		}
	}