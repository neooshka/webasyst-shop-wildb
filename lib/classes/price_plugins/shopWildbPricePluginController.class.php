<?php
	
	abstract class shopWildbPricePluginController implements shopWildbPricePluginInterface {
		
		protected $plugin_id;
		
		/** @var shopPlugin */
		protected $plugin;
		
		public function __construct() {
			
			if(!$this->plugin_id){
				throw new waException('Ошибка подключения плагина цен. $plugin_id не задан');
			}
			
			$plugins = wa()->getConfig()->getAppConfig('shop')->getPlugins();
			
			if (isset($plugins[$this->plugin_id])) {
				$this->plugin = wa('shop')->getPlugin($this->plugin_id);
			}
		}
		
		public function getPluginName() {
			
			if($this->plugin) {
				return $this->plugin->getName();
			}
			
			return false;
		}
		
		public function pluginInstalled(): bool {
			return !is_null($this->plugin);
		}
	}