<?php
	
	interface shopWildbPricePluginInterface {
		function getPriceFields();
		function getPrices($skus, $field);
		function getPluginName();
		function pluginInstalled(): bool;
	}