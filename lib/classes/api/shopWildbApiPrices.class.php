<?php
	
	class shopWildbApiPrices extends shopWildbApiController {
		
		protected $address = 'discounts-prices-api.wb.ru';
		
		protected $requestPerPeriod = 10;
		protected $period = 6;
		
		private function getSkusPrices(int $nmID){
			
			$result = [];
			
			$filter = [
				'limit' => 1000,
				'offset' => 0,
				'nmID' => $nmID,
			];
			
			$data = '?' . http_build_query($filter);
			
			$prices = $this->get('api/v2/list/goods/size/nm' . $data, ['data']);
			
			if($prices && !empty($prices['data']['listGoods'])){
				foreach ($prices['data']['listGoods'] as $sku){
					$result[$sku['sizeID']] = $sku;
				}
			}
			
			return $result;
		}
		
		public function getPrices(){
			
			$result = [];
			
			$filter = [
				'limit' => 1000,
				'offset' => 0,
			];
			
			do {
				
				$data = '?' . http_build_query($filter);
				
				$prices = $this->get('api/v2/list/goods/filter' . $data, ['data']);
				
				if($prices && !empty($prices['data']['listGoods'])){
					foreach ($prices['data']['listGoods'] as $nom){
						$size = reset($nom['sizes']);
						$result[$nom['nmID']] = ['nmID' => $nom['nmID'], 'price' => $size['price'], 'discount' => $nom['discount']];
					}
					
					$filter['offset'] += $filter['limit'];
				}
				
			} while ($prices && !empty($prices['data']['listGoods']));
			
			return $result;
		}
		
		public function setPrices(array $items){
			
			$chunk_items = array_chunk($items,1000);
			
			foreach ($chunk_items as $prices){
				$result = $this->post('api/v2/upload/task', ['data' => $prices], ['data']);
			}
			
			return $result;
		}
	}