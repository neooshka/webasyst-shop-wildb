<?php
	
	class shopWildbStream {
		
		const HTTP = 'http';
		const HTTPS = 'https';
		
		const TYPE_POST = 'POST';
		const TYPE_PUT = 'PUT';
		const TYPE_PATCH = 'PATCH';
		const TYPE_DELETE = 'DELETE';
		
		private $protocol = self::HTTPS;
		private $error = '';
		private $debug = false;
		private $stream;
		private $method;
		private $type;
		private $response;
		private $url;
		private $code;
		private $result;
		private $curl_info;
		
		public function __construct(array $options) {
			
			$this->debug = (bool) $options['debug'];
			$this->address = $options['address'];
			$this->method = $options['method'];
			$this->type = $options['type'];
			
			if(substr($this->method, 0, 1) !== '/'){
				$this->method = '/' . $this->method;
			}
			
			$this->stream = curl_init($this->protocol . '://' . $this->address . $this->method);
			
			curl_setopt($this->stream, CURLOPT_HTTPHEADER, [
				'Authorization:' . $options['token'],
				'Content-Type:application/json'
			]);
			curl_setopt($this->stream, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($this->stream, CURLOPT_TIMEOUT, 10);
			curl_setopt($this->stream, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->stream, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($this->stream, CURLOPT_HEADER, false);
			
			if(!is_null($this->type)){
				switch ($this->type){
					case self::TYPE_PUT:
						curl_setopt($this->stream, CURLOPT_CUSTOMREQUEST, self::TYPE_PUT);
						break;
					case self::TYPE_DELETE:
						curl_setopt($this->stream, CURLOPT_CUSTOMREQUEST, self::TYPE_DELETE);
						break;
					case self::TYPE_PATCH:
						curl_setopt($this->stream, CURLOPT_CUSTOMREQUEST, self::TYPE_PATCH);
						break;
					case self::TYPE_POST:
						curl_setopt($this->stream, CURLOPT_POST, true);
				}
			}
			
		}
		
		public function sendQuery(array $data = null){
			
			if(!is_null($data)){
				curl_setopt($this->stream, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
			}
			
			$this->response = curl_exec($this->stream);
			$this->result = json_decode($this->response, true);
			$this->code = curl_getinfo($this->stream, CURLINFO_RESPONSE_CODE);
			$this->url = curl_getinfo($this->stream, CURLINFO_EFFECTIVE_URL);
			$this->curl_info = curl_getinfo($this->stream);
			$curl_errno = curl_errno($this->stream);
			$curl_error = curl_error($this->stream);
			
			if($this->debug || $this->code >= 400){
				
				if(mb_strlen($this->response) > 3000){
					$response_raw = mb_substr($this->response,0,3000) . " ...";
				}else{
					$response_raw = $this->response;
				}
				
				waLog::dump(
				  [
					'url' => $this->url,
					'data' => is_array($data) ? $this->trimArr($data) : $data,
					'response_code' => $this->code,
					'response_raw' => $response_raw,
					'response_data' => is_array($this->result) ? $this->trimArr($this->result) : $this->result,
					'curl_error' => $curl_error,
				  ],
				  shopWildbPlugin::SLUG . '/wbResponse.log'
				);
			}
			
			return [
				'code' => $this->code,
				'curl_errno' => $curl_errno,
				'result' => $this->result,
			];
		}
		
		private function trimArr(array $arr){
			
			if(is_array($arr) && count($arr) > 10){
				return "to big array...";
			}else{
				foreach ($arr as &$item){
					if(is_array($item)) $item = $this->trimArr($item);
				}
			}
			
			return $arr;
		}
		
		public function __destruct(){
			curl_close($this->stream);
		}
	}