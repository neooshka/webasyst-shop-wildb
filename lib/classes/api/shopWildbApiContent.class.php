<?php
	
	class shopWildbApiContent extends shopWildbApiController {
		
		protected $address = 'suppliers-api.wildberries.ru';
		
		protected $requestPerPeriod = 100;
		protected $period = 60;
		
		public function updateMedia(array $data){
			return $this->post('content/v3/media/save', $data);
		}
		
		public function getCards(int $with_photo = -1, bool $get_skus_only = false){
			
			$result = [
				'cards' => [],
				'total' => 0,
			];
			
			$data = [
				'settings' => [
					'cursor' => [
						'limit' => 100,
					],
					'filter' => [
						'withPhoto' => $with_photo,
					],
				],
			];
			
			do {
				
				$response = $this->post('content/v2/get/cards/list', $data, ['cards','cursor']);
				
				if(!$response) break;
				
				if($get_skus_only){
					$this->explodeSkus($response['cards'], $result);
				}else{
					$this->explodeCards($response['cards'], $result);
				}
				
				$result['total'] += $response['cursor']['total'];
				
				$data['settings']['cursor']['updatedAt'] = $response['cursor']['updatedAt'];
				$data['settings']['cursor']['nmID'] = $response['cursor']['nmID'];
				
			} while($response['cursor']['total'] === $data['settings']['cursor']['limit']);
			
			return $result;
		}
		
		public function getTrash(){
			
			$result = [
				'cards' => [],
				'total' => 0,
			];
			
			$data = [
				'settings' => [
					'cursor' => [
						'limit' => 100,
					],
				],
			];
			
			do {
				
				$response = $this->post('content/v2/get/cards/trash', $data, ['cards','cursor']);
				
				if(!$response) break;
				
				$this->explodeCards($response['cards'], $result);
				
				$result['total'] += $response['cursor']['total'];
				
				$data['settings']['cursor']['trashedAt'] = ifset($response['cursor']['trashedAt'],'');
				$data['settings']['cursor']['nmID'] = $response['cursor']['nmID'];
				
			} while($response['cursor']['total'] === $data['settings']['cursor']['limit']);
			
			return $result;
		}
		
		private function explodeCards($cards, &$result){
			foreach ($cards as $card){
				$result['cards'][$card['vendorCode']] = $card;
			}
		}
		
		private function explodeSkus($cards, &$result){
			foreach ($cards as $card){
				foreach ($card['sizes'] as $size){
					$result['cards'] = array_merge($result['cards'], $size['skus']);
				}
			}
		}
		
		public function cardsUpdate(array $items){
			
			$chunk_items = array_chunk($items,3000);
			
			foreach ($chunk_items as $cards){
				$result = $this->post('content/v2/cards/update', $cards);
			}
			
			return $result;
		}
		
		public function cardsUpload(array $items){
			
			$chunk_items = array_chunk($items,100);
			
			foreach ($chunk_items as $cards){
				$result = $this->post('content/v2/cards/upload', $cards);
			}
			
			return $result;
		}
		
		public function cardsUploadAdd(array $items){
			return $this->post('content/v2/cards/upload/add', $items);
		}
		
		public function getLimits(){
			return $this->get('content/v2/cards/limits', ['data']);
		}
		
		public function getCategories(array $filter = null){
			
			$data = '';
			
			if(!is_null($filter)) $data = '?' . http_build_query($filter);
			
			return $this->get('content/v2/object/all' . $data, ['data']);
		}
		
		public function getAllCategories(){
			
			$result = [];
			
			$filter = [
				'limit' => 1000,
				'offset' => 0,
			];
			
			do {
				$categories = $this->getCategories($filter);
				
				if($categories && !empty($categories['data'])){
					$result = array_merge($result,$categories['data']);
					$filter['offset'] += $filter['limit'];
				}
				
			} while ($categories && !empty($categories['data']));
			
			return $result;
		}
		
		public function getFeatures(int $category_id){			
			return $this->get('content/v2/object/charcs/' . $category_id, ['data']);
		}
		
		public function getDirectory(string $directory){
			
			$result = [];
			
			if(!in_array($directory, shopWildbDirectoryesHelper::getDirectories())){
				return false;
			}
			
			if($directory === "tnved"){
				
				$wb_feature_name = shopWildbDirectoryesHelper::getFeatureNameByDirectory($directory);
				$wb_categories = (new shopWildbSyncCategoriesModel)->getSyncCategoriesByFeatureName($wb_feature_name);
				
				foreach ($wb_categories as $wb_category){
					
					$method = 'content/v2/directory/'.$directory . '?' . http_build_query(['subjectID' => $wb_category['wb_category_id']]);
					$tnveds = $this->get($method, ['data']);
					
					if($tnveds){
						foreach ($tnveds['data'] as &$tnved){
							$tnved['name'] = $tnved['tnved'];
						}
						
						unset($tnved);
					
						$result['data'] += $tnveds['data'];
					}
				}
			}elseif($directory === "vat"){
				$method = 'content/v2/directory/'.$directory . '?' . http_build_query(['locale' => 'ru']);
				$result = $this->get($method, ['data']);
			}else{
				$result = $this->get('content/v2/directory/'.$directory, ['data']);
			}

			return $result ? $result['data'] : false;
		}
	}