<?php
	
	
	abstract class shopWildbApiController {
		
		protected static $instance = [];
		
		protected $settings = [];
		protected $options = [];
		
		protected $address = 'suppliers-api.wildberries.ru';
		
		protected static $request = [];
		protected static $currentTime = [];
		protected $requestPerPeriod = 100;
		protected $period = 60;
		
		protected $status;
		protected $status_message = [
			200 => 'Успех',
			204 => 'Успех',
			208 => 'Такая загрузка уже есть',
			400 => 'Некорректный запрос',
			401 => 'Не авторизован',
			403 => 'Доступ запрещен',
			404 => 'Запрашиваемый ресурс не найден',
			409 => 'Несоответствие параметров запроса',
			413 => 'Превышен лимит объёма данных в запросе',
			422 => 'Отсутствие в запросе параметра nmId',
			429 => 'Слишком много запросов',
			500 => 'Внутренняя ошибка сервера',
		];
		protected $error;
		protected $error_text;
		
		protected $lock_file;
		
		public function __construct() {
			
			$this->settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			
			$this->options = [
				'token' => ifempty($this->settings, 'api_token', ''),
				'debug' => $this->settings['wb_debug'],
				'address' => $this->address,
			];
			
			if(empty($this->options['token'])) {
				waLog::log("Не задан параметр \"Токен API\"", shopWildbPlugin::SLUG . '/wbApi.log');
				throw new waException("Не задан параметр \"Токен API\"");
			}
			
			$this->lock_file = fopen(wa('shop')->getTempPath('wildb') . '/' . static::class . '.lock', "c");
			
			if(!flock($this->lock_file, LOCK_EX)){
				waLog::log("Не удалось получить доступ к ресурсам API", shopWildbPlugin::SLUG . '/wbApi.log');
				throw new waException("Не удалось получить доступ к ресурсам API");
			}
		}
		
		/** @return static */
		public static function getInstance(){
			
			if (!isset(static::$instance[static::class])) {
				static::$instance[static::class] = new static();
			}
			
			return static::$instance[static::class];
		}
		
		protected function get(string $method, array $expected_data = [], int $expected_code = 200){
			return $this->query($method, null, null, $expected_data, $expected_code);
		}
		
		protected function post(string $method, array $data = null, array $expected_data = [], int $expected_code = 200){
			return $this->query($method, $data, shopWildbStream::TYPE_POST, $expected_data, $expected_code);
		}
		
		protected function put(string $method, array $data = null, array $expected_data = [], int $expected_code = 200){
			return $this->query($method, $data, shopWildbStream::TYPE_PUT, $expected_data, $expected_code);
		}
		
		protected function patch(string $method, array $data = null, array $expected_data = [], int $expected_code = 200){
			return $this->query($method, $data, shopWildbStream::TYPE_PATCH, $expected_data, $expected_code);
		}
		
		protected function delete(string $method, array $data = null, array $expected_data = [], int $expected_code = 200){
			return $this->query($method, $data, shopWildbStream::TYPE_DELETE, $expected_data, $expected_code);
		}
		
		private function query(string $method, array $data = null, string $type = null, array $expected_data = [], int $expected_code = 200){
			
			$this->checkRequests();
			
			$attempt = 0;
			$max_attempt = 3;
			
			$options = $this->options;
			$options['method'] = $method;
			$options['type'] = $type;
			
			if(!empty($options['token'])) {
				
				do {
					$attempt++;
					
					$api = new shopWildbStream($options);
					$result = $api->sendQuery($data);
					unset($api);
					
					if($attempt < $max_attempt && $result['curl_errno'] > 0) usleep(1000);
					
				} while ($attempt < $max_attempt && $result['curl_errno'] > 0);
			}
			
			$this->status = $result['code'];
			$this->error = $result['result']['error'] ?? false;
			$this->error_text = $result['result']['errorText'] ?? "";
			
			if($result['code'] === $expected_code){
				if($expected_data){
					$return = [];
					foreach ($expected_data as $key){
						if(!isset($result['result'][$key])){
							return false;
						}
						$return[$key] = $result['result'][$key]; 
					}
					
					return $return;
				}
				
				return true;
			}
			
			return false;
		}
		
		/** 
		 * Throttling 
		 */
		private function checkRequests(){
			
			if (!isset(static::$currentTime[static::class]) || time() >= static::$currentTime[static::class] + $this->period) {
				static::$request[static::class] = 0;
				static::$currentTime[static::class]  = time();
			}
			
			if (static::$request[static::class] >= $this->requestPerPeriod) {
				sleep(static::$currentTime[static::class] + $this->period - time());
			}
			
			static::$request[static::class]++;
		}
		
		public function getExchangeReport(){
			return [
				'status' => $this->status,
				'message' => isset($this->status_message[$this->status]) ? $this->status_message[$this->status] : "Неизвестный статус",
				'error' => $this->error,
				'error_text' => $this->error_text,
			];
		}
		
		public function __destruct() {
			if($this->lock_file) {
				fclose($this->lock_file);
			}
		}
	}