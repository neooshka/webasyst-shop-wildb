<?php
	
	class shopWildbApiMarketplace extends shopWildbApiController {
		
		protected $address = 'suppliers-api.wildberries.ru';
		
		protected $requestPerPeriod = 300;
		protected $period = 60;
		
		public function getOrderSticker(array $order_ids){
			return $this->post('api/v3/orders/stickers?type=png&width=58&height=40',$order_ids, ['stickers']);
		}
		
		public function getSupplySticker(string $supply_id){
			return $this->get("/api/v3/supplies/{$supply_id}/barcode?type=png", ['barcode','file']);
		}
		
		public function getTrbxSticker(string $supply_id, array $trbx_id){
			return $this->post("/api/v3/supplies/{$supply_id}/trbx/stickers?type=png", ['trbxIds' => $trbx_id], ['stickers']);
		}
		
		public function getOrders(){
			
			$result = $this->get('api/v3/orders/new', ['orders']);
			
			foreach ($result['orders'] as &$order){
				$order['createdAt'] = $this->convertDateTime($order['createdAt']);
			}
			
			return $result;
		}
		
		public function getOrdersStatuses(array $data){
			return $this->post('api/v3/orders/status', $data, ['orders']);
		}
		
		private function convertDateTime($datetime, $format = null) {
			
			if(is_null($datetime)) return "";
			
			$time_zone = new DateTimeZone(date_default_timezone_get());
			$datetime = new DateTime($datetime);
			$datetime->setTimezone($time_zone);
			
			return $format && is_string($format) ? $datetime->format($format) : $datetime;
		}
		
		public function getSupplyOrders(string $supply_id){
			return $this->get("/api/v3/supplies/{$supply_id}/orders", ['orders']);
		}
		
		public function getSupplyTrbx(string $supply_id){
			return $this->get("/api/v3/supplies/{$supply_id}/trbx", ['trbxes']);
		}
		
		public function getSupplies(){
			
			$result = [];
			
			$data = [
				'limit' => 1000,
				'next' => 0,
			];
			
			do {
				
				$response = $this->get("api/v3/supplies?" . http_build_query($data), ['next','supplies']);
				
				if(!$response) break;
				
				foreach ($response['supplies'] as $supply){
					$supply['createdAt'] = $this->convertDateTime($supply['createdAt'], "Y-m-d H:i:s");
					$supply['closedAt'] = $this->convertDateTime($supply['closedAt'], "Y-m-d H:i:s");
					$supply['scanDt'] = $this->convertDateTime($supply['scanDt'], "Y-m-d H:i:s");
					$result[$supply['id']] = $supply;
				}
				
				$data['next'] = $response['next'];
				
			} while(count($response['supplies']) === $data['limit']);
			
			return $result;
		}
		
		public function addSupply($name){
			return $this->post('api/v3/supplies',['name' => $name], ['id'], 201);
		}
		
		public function setSupply($supply_id, $order_id){
			return $this->patch("api/v3/supplies/{$supply_id}/orders/{$order_id}",null, [], 204);
		}
		
		public function deleteSupply($supply_id){
			return $this->delete("api/v3/supplies/{$supply_id}",null, [], 204);
		}
		
		public function completeSupply($supply_id){
			return $this->patch("/api/v3/supplies/{$supply_id}/deliver",null, [], 204);
		}
		
		public function addTrbx($supply_id){
			return $this->post("/api/v3/supplies/{$supply_id}/trbx",['amount' => 1], ['trbxIds'], 201);
		}
		
		public function deleteTrbx($trbx_id, $supply_id){
			return $this->delete("/api/v3/supplies/{$supply_id}/trbx",['trbxIds' => [$trbx_id]], [], 204);
		}
		
		public function removeFromTrbx($supply_id, $trbx_id, $order_id){
			return $this->delete("/api/v3/supplies/{$supply_id}/trbx/{$trbx_id}/orders/{$order_id}", null, [], 204);
		}
		
		public function addOrderTrbx($supply_id, $trbx_id, $orders){
			return $this->patch("/api/v3/supplies/{$supply_id}/trbx/{$trbx_id}", ['orderIds' => $orders], [], 204);
		}
		
		public function canselOrder($order_id){
			return $this->patch("api/v3/orders/{$order_id}/cancel",null, [], 204);
		}
		
		public function setStocks(array $items){
			
			$stock_id = $this->settings['wb_stock_id'];
			if(!$stock_id){
				waLog::log("Не задан параметр \"ID склада WB\"", shopWildbPlugin::SLUG . '/wbApi.log');
				return false;
			}
			
			$chunk_items = array_chunk($items,1000);
			
			foreach ($chunk_items as $stocks){
				$result = $this->put("api/v3/stocks/{$stock_id}", ['stocks' => $stocks], [], 204);
			}
			
			return $result;
		}
	}