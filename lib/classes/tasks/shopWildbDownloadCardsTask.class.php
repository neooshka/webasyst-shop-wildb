<?php
	
	class shopWildbDownloadCardsTask extends shopWildbTaskController {
		
		protected $task = 'download_cards';
		
		private $skus;
		
		public function start() {
			
			$this->data['cards'] = $this->api->getCards(-1);
			
			if(!$this->data['cards']['cards']) $this->finish();
			
			$sku_field = $this->settings['chrtID_by'];
			
			$this->skus = (new shopProductSkusModel)->getAll($sku_field);
			
			foreach ($this->data['cards']['cards'] as $vendor_code => $nom){
				
				if($sku = $this->getSku($nom)){
					$this->items_completed[] = [
						'task_id' => $this->id,
						'vendorCode' => $nom['vendorCode'],
						'nmID' => $nom['nmID'],
						'imtID' => $nom['imtID'],
						'subjectID' => $nom['subjectID'],
						'product_id' => $sku['product_id'],
						'last_updated' => date('Y-m-d H:i:s'),
						'status' => 1,
					];
				}
			}
			
			$this->wb_products_model->deleteByField('vendorCode',array_keys($this->data['cards']['cards']));
			$this->wb_products_model->multipleInsert($this->items_completed, ['nmID','imtID','subjectID','vendorCode','product_id','last_updated']);
			$this->task_items_model->multipleInsert($this->items_completed,['nmID']);
			
			$this->finish();
		}
		
		private function getSku(array $nom){
			foreach ($nom['sizes'] as $size){
				foreach ($size['skus'] as $wb_sku){
					if(isset($this->skus[$wb_sku])){
						return $this->skus[$wb_sku];
					}
				}
			}
			return false;
		}
	}