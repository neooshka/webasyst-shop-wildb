<?php
	
	abstract class shopWildbCardsTaskController extends shopWildbTaskController {
		
		const CARDS_LIMIT = 8000;
		
		protected $custom_prices = false;
		
		abstract protected function getProducts();
		
		protected function downloadWBContent(){
			
			$this->data['cards'] = $this->api->getCards(-1);
			$this->data['trash'] = $this->api->getTrash();
		}
		
		protected function initData(){
			
			$wb_limit = ($this->data['wb_limit']['data']['freeLimits'] + $this->data['wb_limit']['data']['paidLimits'] - $this->data['cards']['total'] - $this->data['trash']['total']);
			
			if($wb_limit < 0) $wb_limit = 0;
			if($wb_limit > self::CARDS_LIMIT) $wb_limit = self::CARDS_LIMIT;
			
			$wb_sync_features_model = new shopWildbSyncFeaturesModel;
			$this->data['sync_values'] = (new shopWildbSyncDirectoryModel)->getSyncValues();
			$this->data['sync_categories'] = (new shopWildbSyncCategoriesModel)->getSyncCategories();
			$this->data['sync_features'] = $wb_sync_features_model->getSyncFeatures();
			$this->data['shop_features'] = (new shopFeatureModel)->getAll('id');
			$this->data['wb_limit'] = $wb_limit;
			$this->data['size_feature'] = $wb_sync_features_model->getShopFeatureByWbName('Размер');
			if($this->data['size_feature']){
				$this->data['size_feature']['wb_categories'] = (new shopWildbFeaturesCategoriesModel)->getCategoriesByFeatureId($this->data['size_feature']['wb_feature_id']);
			}
			$this->data['wb_size_feature'] = $wb_sync_features_model->getShopFeatureByWbName('Рос. размер') ? : $this->data['size_feature'];
		}
		
		protected function prepareSkus(){
			
			$this->downloadWBContent();
			
			$this->data['wb_limit'] = $this->api->getLimits();
			
			if(!$this->data['wb_limit']){
				return false;
			}
			
			$this->initData();
			
			$products = $this->getProducts();
			
			if(!$products) return [];
			
			shopWildbFeaturesHelper::addProductFeatures($products);
			$skus = shopWildbSkusHelper::getProductsSkus($products);
			
			if($custom_prices = shopWildbSkuPriceHelper::getCustomPrices($skus)){
				$skus = $custom_prices;
				$this->custom_prices = true;
			}
			
			$result = [];
			
			foreach ($skus as $s) {
				
				$result_sku = $s + $products[$s['product_id']];
				$result_sku['name'] = $products[$s['product_id']]['name'];
				$result_sku['sku_name'] = $s['name'];
				
				$feature_values_id = [];
				$feature_values = [];
				
				if($products[$s['product_id']]['features_selectable']) {
					foreach ($products[$s['product_id']]['features_selectable'] as $fs_id) {
						
						if($this->data['size_feature'] && $this->data['size_feature']['id'] == $fs_id) continue;
						if($this->data['wb_size_feature'] && $this->data['wb_size_feature']['id'] == $fs_id) continue;
						
						if(isset($s[$this->data['shop_features'][$fs_id]['code']])) {
							$feature_values_id[] = $result_sku['sku_features_raw'][$fs_id];
							$feature_value = $this->parseValue($this->extractValue($result_sku, $fs_id));
							$feature_values[] = $this->parseStringValue(is_array($feature_value) ? reset($feature_value) : $feature_value, 20);
						}
					}
				}
				
				$variant_id = $feature_values_id ? implode('-', $feature_values_id) : 0;
				$variant_name = $feature_values ? implode('-', $feature_values) : '';
				
				$result_sku['variant_id'] = $variant_id;
				$result_sku['variant_name'] = $variant_name;
				$result_sku['vendor_code'] = $this->parseStringValue($this->getVendorCode($result_sku), 72);
				
				$merge_by = $this->parseValue($result_sku[$this->settings['merge_by']] ?? 'undefined_' . count($result));
				
				$result[$merge_by][$result_sku['vendor_code']][] = $result_sku;
			}
			
			return $result;
		}
		
		protected function fillProperties($sku, &$nom){
			
			if($brand = ifempty($this->settings['charcs_brand'])){
				$brand = $this->parseValue($this->extractValue($sku, $brand));
				$nom['brand'] = $this->parseStringValue(is_array($brand) ? reset($brand) : $brand, 100);
			}
			if($dimensions_length = ifempty($this->settings['charcs_dimensions_length'])){
				$value = $this->extractValue($sku, $dimensions_length);
				$nom['dimensions']['length'] = (int) (ifempty($this->settings['convert_values']) ? $this->convertValue($value, 'cm') : $this->parseValue($value));
			}
			if($dimensions_width = ifempty($this->settings['charcs_dimensions_width'])){
				$value = $this->extractValue($sku, $dimensions_width);
				$nom['dimensions']['width'] = (int) (ifempty($this->settings['convert_values']) ? $this->convertValue($value, 'cm') : $this->parseValue($value));
			}
			if($dimensions_height = ifempty($this->settings['charcs_dimensions_height'])){
				$value = $this->extractValue($sku, $dimensions_height);
				$nom['dimensions']['height'] = (int) (ifempty($this->settings['convert_values']) ? $this->convertValue($value, 'cm') : $this->parseValue($value));
			}
		}
		
		protected function createNom(array $sku_sizes) {
			
			$sku = $this->getSku($sku_sizes);
			
			$sizes = $this->createSizes($sku_sizes);
			$charcs = $this->createCharacteristics($sku);
			
			if(!$sizes) return false;
			
			$nom = [
				"vendorCode" => $sku['vendor_code'],
				"characteristics" => $charcs ? : [],
				"sizes" => $sizes,
			];
			
			if($title = ifempty($this->settings['charcs_title'])){
				$title = $this->parseValue($this->extractValue($sku, $title));
				$nom['title'] = $this->parseStringValue(is_array($title) ? reset($title) : $title, 60);
			}
			if($description = ifempty($this->settings['charcs_description'])){
				$description = $this->parseValue($this->extractValue($sku, $description));
				$nom['description'] = $this->parseStringValue(is_array($description) ? reset($description) : $description, 2000);
			}
			
			$this->fillProperties($sku, $nom);
			
			return $nom;
		}
		
		protected function createSizes(array $sku_sizes) {
			
			if(empty($sku_sizes)) return false;
			
			$wb_sizes = [];
			
			$price_field = ifempty($this->settings['wb_old_price'],'compare_price');

			foreach ($sku_sizes as $size){
				
				$needle = true;
				
				$wb_size = [
					"price" => shopWildbSkuPriceHelper::getSkuPrice($size, $this->custom_prices),
					"skus" => [$size[ifempty($this->settings['chrtID_by'],'id')]],
				];
				
				if($this->data['size_feature'] && in_array($size['wb_category_id'],$this->data['size_feature']['wb_categories'])){
					
					if(!isset($size[$this->data['size_feature']['code']]) && !isset($size[$this->data['size_feature']['code']])) return false;
					
					$wb_size["techSize"] = (isset($size[$this->data['size_feature']['code']])) ? $size[$this->data['size_feature']['code']] : $size[$this->data['size_feature']['code']];
					
					if(isset($size[$this->data['wb_size_feature']['code']])){
						$wb_size["wbSize"] = $size[$this->data['wb_size_feature']['code']];
					}elseif(isset($size[$this->data['wb_size_feature']['code']])){
						$wb_size["wbSize"] = $size[$this->data['wb_size_feature']['code']];
					}else{
						$wb_size["wbSize"] = $wb_size["techSize"];
					}
					
					foreach ($wb_sizes as &$ex_size){
						if(isset($ex_size['techSize']) && $wb_size['techSize'] === $ex_size['techSize']){
							$ex_size['skus'] = array_merge($wb_size['skus'], $ex_size['skus']);
							$needle = false;
							break;
						}
					}
					
					unset($ex_size);
				}
				
				if($needle) $wb_sizes[] = $wb_size;
			}
			
			return $wb_sizes;
		}
		
		protected function createCharacteristics(array $sku) {
			
			if(!isset($this->data['sync_features'][$sku['wb_category_id']])) return false;
			
			$wb_features = [];
			
			$sync_features = $this->data['sync_features'][$sku['wb_category_id']];
			
			foreach ($sync_features as $sync_feature){
				
				$feature_value = $this->extractValue($sku, $sync_feature['shop_feature_id']);
				
				if(!$feature_value) continue;
				
				$feature_value_id = $sku['sku_features_raw'][$sync_feature['shop_feature_id']] ?? $sku['product_features_raw'][$sync_feature['shop_feature_id']] ?? null;
				
				if(ifempty($this->settings['convert_values']) && $sync_feature['charc_type'] == 4 && !empty($sync_feature['unit']) && $value = $this->convertValue($feature_value,$sync_feature['unit'])){
					$feature_value = $value;
				}elseif($value = $this->getSyncValue($sync_feature['shop_feature_id'], $feature_value, $feature_value_id)){
					$feature_value = $value;
				}else{
					$feature_value = $this->parseValue($feature_value);
				}
				
				$value = [];
				
				if(is_array($feature_value)){
					foreach ($feature_value as $v){
						
						$value[] = $sync_feature['strlen'] > 0 ? $this->parseStringValue($v, $sync_feature['strlen']) : $v;
						
						if($sync_feature['max_count'] > 0 && count($value) == $sync_feature['max_count']) break;
					}
				}else{
					$value[] = $sync_feature['strlen'] > 0 ? $this->parseStringValue($feature_value, $sync_feature['strlen']) : $feature_value;
				}
				
				if($sync_feature['charc_type'] == 4){
					
					$value = reset($value);
					
					if(is_numeric($value)){
						$value = (int) $value;
					}else{
						continue;
					}
				}
				
				$wb_features[] = ['id' => (int) $sync_feature['wb_feature_id'], 'value' => $value];
			}
			
			return $wb_features;
		}
		
		protected function getSyncValue($feature_id, $feature_value, $feature_value_id){
			
			if(isset($this->data['sync_values'][$feature_id])){
				
				if(!$feature_value_id && isset($feature_value['id'])){
					$feature_value_id = $feature_value['id'];
				}
				
				if(!$feature_value_id){
					$value = $this->parseValue($feature_value);
					$type = preg_replace('/\..*$/', '', $this->data['shop_features'][$feature_id]['type']);
					$feature_value_id = shopFeatureModel::getValuesModel($type)->getValueId($feature_id,$value);
				}
				
				if($feature_value_id && isset($this->data['sync_values'][$feature_id][$feature_value_id])){
					return $this->data['sync_values'][$feature_id][$feature_value_id];
				}
			}
			
			return false;
		}
		
		protected function extractValue($sku, $feature_id){
			
			if(is_numeric($feature_id)){
				
				$feature_code = $this->data['shop_features'][$feature_id]['code'];
				
				if(!$feature_code || ($feature_code == $this->data['size_feature']['code'] || $feature_code == $this->data['wb_size_feature']['code'])) return false;
				
				$composite_code = explode('.',$feature_code);
				
				if(count($composite_code) === 2){
					$feature_value = $sku[$composite_code[0]][$composite_code[1]] ?? $sku['features'][$composite_code[0]][$composite_code[1]] ?? false;
				}elseif(count($composite_code) === 1){
					$feature_value = $sku[$feature_code] ?? $sku['features'][$feature_code] ?? false;
				}
				
				return $feature_value;
			}else{
				return $sku[$feature_id];
			}
		}
		
		protected function parseValue($feature_value){
			
			if($feature_value instanceof shopDimensionValue || $feature_value instanceof shopColorValue || (is_array($feature_value) && isset($feature_value['value']) && isset($feature_value['id']))){
				return $feature_value['value'];
			}elseif(is_array($feature_value)){
				$value = [];
				foreach($feature_value as $fv_id => $fv_value){
					$value[] = $this->parseValue(['id' => $fv_id, 'value' => $fv_value]);
				}
			}else{
				$value = $feature_value;
			}
			
			return $value;
		}
		
		protected function parseStringValue(string $value, int $len): string {
			
			if(!$value) return "";
			
			$value = preg_replace('/[®]/ui', '',$value);
			$value = str_replace('  ', ' ',$value);
			$value = strip_tags(html_entity_decode($value));
			$value = trim($value);
			
			$new_value = mb_substr($value,0,$len);
			
			if(mb_strlen($new_value) < mb_strlen($value)){
				$new_value = mb_substr($new_value,0, mb_strrpos($new_value,' '));
			}
			
			return $new_value;
		}
		
		protected function convertValue($value,$wb_unit) {
			
			if(is_string($value)){
				
				$value = str_replace(",",".",$value);
				
				$unit = $this->transliterateUnit(trim(preg_replace('!\d+(?:\.\d+)?!','',$value)));
				if(preg_match('!\d+(?:\.\d+)?!',$value, $matches) === 1){
					$value = reset($matches);
				}
				
				$wb_type = $this->getDimensionType($wb_unit);
				
				if($wb_type){
					$value = new shopDimensionValue(['value' => $value, 'unit' => $unit, 'type' => $wb_type]);
				}
			}
			
			if($value instanceof shopDimensionValue){
				$wb_unit = $this->transliterateUnit($wb_unit);
				$value = $value->convert($wb_unit);
			}
			
			return is_numeric($value) ? (int) round($value) : false;
		}
		
		private function transliterateUnit(string $unit){
			switch ($unit){
				case 'см': return 'cm';
				case 'кг': return 'kg';
				case 'г': return 'g';
				case 'гр': return 'g';
				case 'мм': return 'mm';
				case 'мл': return 'ml';
			}
			return shopHelper::transliterate($unit);
		}
		
		private function getDimensionType(string $unit){
			switch ($unit){
				case 'мм': return 'length';
				case 'mm': return 'length';
				case 'см': return 'length';
				case 'cm': return 'length';
				case 'кг': return 'weight';
				case 'kg': return 'weight';
				case 'г': return 'weight';
				case 'g': return 'weight';
				case 'гр': return 'weight';
				case 'gr': return 'weight';
				case 'мл': return 'volume';
				case 'ml': return 'volume';
			}
			return false;
		}
		
		protected function finish(){
			
			$this->items_completed = $this->task_items_model->getByField(['task_id' => $this->id, 'status' => 1], 'vendorCode');
			$this->data['cards'] = $this->api->getCards(0);
			
			$items = [];
			
			foreach ($this->data['cards']['cards'] as $vendor_code => $nom){
				if(isset($this->items_completed[$nom['vendorCode']])){
					$items[] = [
						'nmID' => $nom['nmID'],
						'imtID' => $nom['imtID'],
						'subjectID' => $nom['subjectID'],
						'vendorCode' => $nom['vendorCode'],
						'product_id' => $this->items_completed[$nom['vendorCode']]['product_id'],
						'last_updated' => date('Y-m-d H:i:s'),
					];
					$this->items_completed[$nom['vendorCode']]['nmID'] = $nom['nmID'];
				}
			}
			
			if($items){
				// add new wb_products
				$this->wb_products_model->multipleInsert($items, ['nmID','imtID','subjectID','vendorCode','product_id','last_updated']);
				
				// update nmID in current task
				$this->task_items_model->multipleInsert(array_values($this->items_completed),['nmID']);
				
				// add upload_images task
				$task_upload_images = new shopWildbUploadImagesTask(['parent_task' => $this->id]);
				$task_upload_images->setItems($items);
				$task_upload_images->write();
			}
			
			parent::finish();
		}
		
		protected function getSku(array $any){
			if(isset($any['product_id'])) return $any; 
			return is_array($any) && !empty($any) ? $this->getSku(reset($any)) : false;
		}
		
		protected function getVendorCode(array $sku){
			
			switch ($this->settings['nomID_by']){
				case 'sku':
					$vendorCode = $sku['sku'] ?? null;
					break;
				case 'sku_id':
					$vendorCode = $sku['id'] ?? null;
					break;
				case 'product_id':
					$vendorCode = $sku['product_id'] ?? null;
					break;
				case 'product_id-variant_name':
					if(!isset($sku['product_id'])) return null;
					$vendorCode = $sku['variant_name'] ? $sku['product_id'] . "-" . $sku['variant_name'] : (string) $sku['product_id'];
					break;
				case 'product_id-variant_id':
				default:
					if(!isset($sku['product_id'])) return null;
					$vendorCode = $sku['variant_id'] ? $sku['product_id'] . "-" . $sku['variant_id'] : (string) $sku['product_id'];
			}
			
			return $vendorCode;
		}
	}