<?php
	
	class shopWildbNewCardsTask extends shopWildbCardsTaskController {
		
		protected $task = 'new_cards';
		
		public function start() {
			
			if($this->exported){
				$this->finish();
				return;
			}
			
			$merged_skus = $this->prepareSkus();
			
			if(!$merged_skus){
				$this->finish();
				return;
			}
			
			$new_cards = [];
			
			foreach ($merged_skus as $variants){
				if($card = $this->createCard($variants)) {
					
					$new_cards[] = $card;
					$sku = $this->getSku($variants);
					
					foreach ($card['variants'] as $nom){
						$this->items_completed[] = [
						  'task_id' => $this->id,
						  'vendorCode' => $nom['vendorCode'],
						  'product_id' => $sku['product_id'],
						  'status' => 1,
						];
					}
				}
			}
			
			if(!$new_cards){
				$this->finish();
				return;
			}
			
			// upload cards
			$this->api->cardsUpload($new_cards);
			
			$this->task_items_model->multipleInsert($this->items_completed, waModel::INSERT_IGNORE);
			
			$this->setStatusExported();
		}
		
		protected function getProducts(){
			
			$min_price = (int) ifempty($this->settings['min_price'],0);
			
			$model = new waModel;
			
			$params = [];
			
			if($this->settings['shop_products'] == 2 && $this->settings['shop_products_type'] == 2 &&
			  isset($this->settings['shop_products_sets']) && is_array($this->settings['shop_products_sets']) && !empty($this->settings['shop_products_sets'])){
				$sql = "
					select p.type_id, p.id, p.name, p.description, p.summary, (case when p.category_id is null then 0 else p.category_id end) as category_id, wc.wb_category_id from shop_product p
					join shop_set_products s on s.product_id = p.id
					join shop_wildb_categories_sync wc on wc.shop_category_id = (case when p.category_id is null then 0 else p.category_id end)
				";
			}else {
				$sql = "
					select p.type_id, p.id, p.name, p.description, p.summary, cp.category_id, wc.wb_category_id from shop_product p
					join shop_category_products cp on cp.product_id = p.id
					join shop_wildb_categories_sync wc on wc.shop_category_id = cp.category_id
				";
			}
			
			if(!$this->settings['is_images_out']){
				$sql .= "join (select product_id from shop_product_images where width >= i:image_width and height >= i:image_height) pi on pi.product_id = p.id";
				$params['image_width'] = $this->image_width;
				$params['image_height'] = $this->image_height;
			}
			
			$sql .= " where p.price >= f:min_price and p.id not in (select distinct product_id from shop_wildb_products)";
			$params['min_price'] = $min_price;
			
			if($this->settings['stock_is_null'] == 0 && $this->settings['is_stock_out'] == 0){
				$sql .= " and (p.count is not null and p.count > 0)";
			}
			
			
			if($this->settings['stock_is_null'] == 1 && $this->settings['is_stock_out'] == 0){
				$sql .= " and (p.count is null or p.count > 0)";
			}
			
			
			if($this->settings['stock_is_null'] == 0 && $this->settings['is_stock_out'] == 1){
				$sql .= " and p.count is not null";
			}
			
			if($this->settings['shop_products'] == 2){
				if($this->settings['shop_products_type'] == 1){
					if(isset($this->settings['shop_products_categories']) && is_array($this->settings['shop_products_categories']) && !empty($this->settings['shop_products_categories'])) {
						$sql .= " and cp.category_id in (i:shop_products_categories)";
						$params['shop_products_categories'] = $this->settings['shop_products_categories'];
					}
				}elseif(isset($this->settings['shop_products_sets']) && is_array($this->settings['shop_products_sets']) && !empty($this->settings['shop_products_sets'])){
					$sql .= " and s.set_id in (s:shop_products_sets)";
					$params['shop_products_sets'] = $this->settings['shop_products_sets'];
				}
			}
			
			$sql .= " order by p.id desc limit i:wb_limit";
			$params['wb_limit'] = $this->data['wb_limit'];
			
			$products = $model->query($sql, $params)->fetchAll('id');
			
			return $products;
		}
		
		protected function createCard(array $variants) {
			
			if(empty($variants)) return false;
			
			$result = [
				'subjectID' => (int) $this->getSku($variants)['wb_category_id'],
				'variants' => [],
			];
			
			foreach ($variants as $vendor_code => $variant) {
				
				if(empty($variant)) continue;
				
				if($nom = $this->createNom($variant)) {
					$result['variants'][] = $nom;
					if(count($result['variants']) === 30) break;
				}
			}
			
			return $result['variants'] ? $result : false;
		}
	}