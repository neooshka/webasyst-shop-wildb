<?php
	
	class shopWildbUploadMissingImagesTask extends shopWildbImagesTaskController {
		
		protected $task = 'upload_missing_images';
		
		public function start() {
			
			if(!$this->items_completed && !$this->items_processed){
				$this->data['cards'] = $this->api->getCards(0);
				
				if(!$this->data['cards']['cards']) {
					$this->finish();
					return;
				}
				
				$wb_products = $this->wb_products_model->getByField('vendorCode', array_keys($this->data['cards']['cards']), 'nmID');
				
				foreach ($wb_products as &$wb_product) {
					$wb_product['task_id'] = $this->id;
				}
				
				unset($wb_product);
				
				$this->items_processed = $wb_products;
				$this->task_items_model->multipleInsert(array_values($this->items_processed), waModel::INSERT_IGNORE);
			}
			
			$this->uploadImages();
		}
	}