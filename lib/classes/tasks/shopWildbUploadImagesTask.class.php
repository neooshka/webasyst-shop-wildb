<?php
	
	class shopWildbUploadImagesTask extends shopWildbImagesTaskController {
		
		protected $task = 'upload_images';
		
		public function start() {
			$this->uploadImages();
		}
	}