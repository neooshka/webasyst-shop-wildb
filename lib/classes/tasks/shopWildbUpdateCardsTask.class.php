<?php
	
	class shopWildbUpdateCardsTask extends shopWildbCardsTaskController {
		
		protected $task = 'update_cards';
		
		public function start() {
			
			if($this->exported){
				$this->finish();
				return;
			}
			
			if(!$this->settings['wb_names_update'] && !$this->settings['wb_descriptions_update'] && !$this->settings['wb_features_update']){
				waLog::log("Не выбраны параметры обновления в разделе Экспорт товаров -> Товары",shopWildbPlugin::SLUG . '/WbExportCli.log');
				return;
			}
			
			$merged_skus = $this->prepareSkus();
			
			$ex_cards = [];
			$noms_add = [];
			
			$i = 0;
				
			foreach ($merged_skus as $merge_by => $variants){
				
				if(empty($variants)) continue;
				
				$sku = $this->getSku($variants);
				
				if(!$sku || !isset($this->data['sync_categories'][$sku['category_id']])) continue;
				
				foreach ($variants as $vendor_code => $sku_sizes){
					
					if(empty($sku_sizes) || !$vendor_code) continue;
					
					if(isset($this->data['cards']['cards'][$vendor_code])){
						// update nom
						if($nom = $this->updateVariant($this->data['cards']['cards'][$vendor_code], $sku_sizes)){
							
							$ex_cards[]  = $nom;
							
							$this->items_completed[$nom['nmID']] = [
								'task_id' => $this->id,
								'vendorCode' => $nom['vendorCode'],
								'nmID' => $nom['nmID'],
								'product_id' => $sku['product_id'],
								'status' => 1,
							];
						}
					}elseif($this->settings['add_noms_when_update']){
						// add noms to card
						if($nom = $this->createNom($sku_sizes) && $imtID = $this->getImtID($merged_skus[$merge_by])){
							
							$noms_add[$imtID][] = $nom;
							
							$this->items_completed[++$i] = [
								'task_id' => $this->id,
								'vendorCode' => $nom['vendorCode'],
								'product_id' => $sku['product_id'],
								'status' => 1,
							];
						}
					}
				}
			}
			
			// update cards
			if($ex_cards){
				$this->api->cardsUpdate($ex_cards);
			}
			
			// add noms
			if($noms_add){
				foreach ($noms_add as $imtID => $noms) {
					$this->api->cardsUploadAdd(["imtID" => $imtID, "cardsToAdd" => $noms]);
				}
			}
			
			$this->wb_products_model->updateByField("nmID",array_keys($this->items_completed),["last_updated" => (new DateTime())->format("Y-m-d H:i:s")]);
			$this->task_items_model->multipleInsert(array_values($this->items_completed), waModel::INSERT_IGNORE);
			
			$this->setStatusExported();
		}
		
		protected function getProducts(){
			
			$product_model = new shopProductModel();
			
			$products = $product_model->query("
				select wp.vendorCode, wp.nmID, wp.imtID, p.type_id, p.id, p.name, p.description, p.summary, cp.category_id, wc.wb_category_id from shop_wildb_products wp
				join shop_product p on wp.product_id = p.id
				join shop_category_products cp on cp.product_id = p.id
				join shop_wildb_categories_sync wc on wc.shop_category_id = cp.category_id
				where wp.vendorCode in (s:vendor_codes)
			", ['vendor_codes' => array_keys($this->data['cards']['cards'])])->fetchAll('id');
			
			return $products;
		}
		
		protected function updateVariant(array $variant, array $sku_sizes) {
			
			$sku = $this->getSku($sku_sizes);
			
			if($this->settings['wb_features_update']){
				
				$sizes = $this->createSizes($sku_sizes);
				$charcs = $this->createCharacteristics($sku);
				
				if($sizes){
					foreach ($sizes as &$size){
						foreach ($variant['sizes'] as $ex_size){
							
							$barcode_exsists = false;
							
							foreach ($ex_size['skus'] as $ex_sku){
								if($ex_sku == $size['skus'][0]){
									$barcode_exsists = true;
									break;
								}
							}
							
							if($barcode_exsists){
								$size['chrtID'] = $ex_size['chrtID'];
								$size['skus'] = $ex_size['skus'];
								break;
							}elseif(isset($size['techSize']) && isset($ex_size['techSize']) && $size['techSize'] === $ex_size['techSize']){
								$size['chrtID'] = $ex_size['chrtID'];
								$size['skus'] = array_merge($size['skus'], $ex_size['skus']);
								break;
							}
						}
					}
					unset($size);
				}
				
				$variant['sizes'] = $sizes;
				$variant['characteristics'] = $charcs ? : [];
				
				$this->fillProperties($sku, $variant);
			}
			
			if($this->settings['wb_names_update'] && $title = ifempty($this->settings['charcs_title'])){
				$title = $this->parseValue($this->extractValue($sku, $title));
				$variant['title'] = $this->parseStringValue(is_array($title) ? reset($title) : $title, 60);
			}
			
			if($this->settings['wb_descriptions_update'] && $description = ifempty($this->settings['charcs_description'])){
				$description = $this->parseValue($this->extractValue($sku, $description));
				$variant['description'] = $this->parseStringValue(is_array($description) ? reset($description) : $description, 2000);
			}
			
			if(isset($variant['photos'])) unset($variant['photos']);
			if(isset($variant['video'])) unset($variant['video']);
			if(isset($variant['tags'])) unset($variant['tags']);
			
			return $variant;
		}
		
		protected function getImtID(array $variants){
			
			foreach ($variants as $variant){
				
				$sku = $this->getSku($variant);
				
				if(isset($this->data['cards']['cards'][$sku['vendor_code']]['imtID'])){
					return $this->data['cards']['cards'][$sku['vendor_code']]['imtID'];
				}
			}
			
			return false;
		}
	}