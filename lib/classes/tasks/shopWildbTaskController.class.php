<?php
	
	abstract class shopWildbTaskController implements shopWildbTaskInterface {
		
		protected $id;
		protected $items_completed = [];
		protected $items_processed = [];
		protected $exported;
		protected $task;
		protected $create_time;
		protected $status;
		protected $parent_task;
		
		protected $settings;
		
		/** @var shopWildbTasksModel */
		protected $task_model;
		
		/** @var shopWildbTaskNomsModel  */
		protected $task_items_model;
		
		/** @var shopWildbProductsModel  */
		protected $wb_products_model;
		
		/** @var shopWildbApiContent  */
		protected $api;
		
		protected $image_width = 700;
		protected $image_height = 900;
		
		function __construct(array $task = []){
			
			$this->task_model = new shopWildbTasksModel();
			$this->task_items_model = new shopWildbTaskNomsModel();
			$this->wb_products_model = new shopWildbProductsModel();
			$this->settings = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID)->getSettings();
			$this->api = shopWildbApiContent::getInstance();
			
			if((!isset($task['id']) || !$task['id']) && isset($task['parent_task']) && $task['parent_task'] > 0){
				if($result = $this->task_model->getByField('parent_task', $task['parent_task'])){
					$task = $result;
				}
			}
			
			$this->init($task);
		}
		
		protected function init(array $task){
			
			$this->id = ifempty($task, 'id', 0);
			$this->create_time = ifempty($task, 'create_time', '');
			$this->status = ifempty($task, 'status', false);
			$this->exported = ifempty($task, 'exported', false);
			$this->parent_task = ifempty($task, 'parent_task', null);
		}
		
		protected function finish() {
			$this->exported = true;
			$this->status = true;
			$this->task_model->updateById($this->id, [
			  'finish_time' => (new DateTime())->format("Y-m-d H:i:s"),
			  'status' => 1,
			  'exported' => 1,
			]);
		}
		
		protected function setStatusExported() {
			$this->exported = true;
			$this->task_model->updateById($this->id, ['exported' => 1]);
		}
		
		public function getID(){
			return $this->id;
		}
		
		public function write(){
			
			if(!$this->id){
				
				$id = $this->task_model->insert(['task' => $this->task, 'parent_task' => $this->parent_task], 2);
				
				if($id){
					if($this->items_processed){
						foreach ($this->items_processed as &$item){
							$item['task_id'] = $id;
						}
						
						unset($item);
						
						$this->task_items_model->multipleInsert(array_values($this->items_processed), waModel::INSERT_IGNORE);
					}
					
					$this->init($this->task_model->getById($id));
					return true;
				}
			}
			
			return false;
		}
		
		public function setItems(array $items){
			$this->items_processed = $items;
		}
	}