<?php
	
	abstract class shopWildbImagesTaskController extends shopWildbTaskController {
		
		protected function init(array $task) {
			
			parent::init($task);
			
			if($this->id){
				
				$noms = $this->task_items_model->getByField(['task_id' => $this->id], 'nmID');
				
				if($noms){
					
					$wb_products = $this->wb_products_model->getByField('nmID', array_keys($noms), 'nmID');

					foreach ($wb_products as $nmID => $wb_product){
						if($noms[$nmID]['status']){
							$this->items_completed[] = $wb_product;
						}else{
							$this->items_processed[] = $wb_product;
						}
					}
				}
			}
		}
		
		protected function uploadImages(){
			
			if(!$this->items_processed){
				$this->finish();
				return;
			};
			
			$model = new waAppSettingsModel();
			$domain_url = $model->get('webasyst', 'url');
			
			if(!$domain_url){
				if($this->settings['wb_debug']){
					waLog::log("не задан Адрес сайта в настройках системы",shopWildbPlugin::SLUG . '/WbExportCli.log');
				}
				return;
			}
			
			if (substr($domain_url, -1) === '/') {
				$domain_url = substr($domain_url, 0, -1);
			}
			
			$noms = [];
			$products = [];
			
			foreach ($this->items_processed as $item){
				$noms[] = $item['nmID'];
				$products[$item['product_id']][] = $item['nmID'];
				
				if(count($noms) >= 90) break;
			}
			
			$product_images = (new shopProductImagesModel())->getImages(array_keys($products),[waSystem::getInstance()->getConfig()->getOption('image_max_size')],'product_id');
			
			$images_Limit = ifempty($this->settings['images_Limit'],30);
			
			if($images_Limit > 30) $images_Limit = 30;
			if($images_Limit < 1) $images_Limit = 1;
			
			foreach ($product_images as $product_id => $images) {
				
				$images = array_slice($images, 0, $images_Limit);
				$images_data = $this->getImagesData($images, $domain_url);
				
				foreach ($products[$product_id] as $nmID){
					if($images_data){
						$this->api->updateMedia([
							'nmId' => (int) $nmID,
							'data' => $images_data,
						]);
					}
				}
			}
			
			$this->task_items_model->updateByField(['task_id' => $this->id, 'nmID' => $noms], ['status' => 1]);
		}
		
		protected function getImagesData(array $images, string $domain_url){
			
			$result = [];
			
			foreach ($images as $img){

				if($img['height'] < $this->image_height || $img['width'] < $this->image_width) continue;
				
				$result[] = $domain_url . $img['url_0'];
			}
			
			return $result;
		}
	}