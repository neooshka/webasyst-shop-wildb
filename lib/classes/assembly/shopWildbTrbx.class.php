<?php
	
	class shopWildbTrbx {
		
		public static function createTrbx(string $supply_id){
			
			$trbx = shopWildbApiMarketplace::getInstance()->addTrbx($supply_id);
			
			if($trbx) {
				
				$supplies_model = new shopWildbSuppliesModel();
				$supply = $supplies_model->getById($supply_id);
				
				if($supply){
					$supplies_model->updateById($supply_id,['trbx_ids' => array_merge($supply['trbx_ids'], $trbx['trbxIds'])]);
					return $trbx['trbxIds'];
				}
			}
			
			return false;
		}
		
		public static function deleteTrbx(string $trbx_id, string $supply_id){
			
			if(!$trbx_id || !$supply_id) return false;
			
			if(shopWildbApiMarketplace::getInstance()->deleteTrbx($trbx_id, $supply_id)) {
				
				$supplies_model = new shopWildbSuppliesModel();
				$supply = $supplies_model->getById($supply_id);
				
				if($supply){
					foreach ($supply['trbx_ids'] as $i => $trbx){
						if($trbx === $trbx_id){
							unset($supply['trbx_ids'][$i]);
							break;
						}
					}
					
					$supplies_model->updateById($supply_id,['trbx_ids' => $supply['trbx_ids']]);
				}
				
				$orders_model = new shopWildbOrdersModel;
				$orders_model->updateByField('trbx_id', $trbx_id, ['trbx_id' => null]);
				
				return true;
			}
			
			return false;
		}
		
		public static function getSticker(string $supply_id, array $trbx_id){
			
			if($sticker = shopWildbApiMarketplace::getInstance()->getTrbxSticker($supply_id, $trbx_id)){
				return $sticker;
			}else{
				waLog::log('Ошибка получения этикетки с WB', shopWildbPlugin::SLUG . '/wbSupplies.log');
				return false;
			}
		}
	}