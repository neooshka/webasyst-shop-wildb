<?php
	
	class shopWildbOrders {
		
		private static $states = [
			'seller_status' => [
				'new' => 'Новое',
				'confirm' => 'На сборке',
				'complete' => 'В доставке',
				'cancel' => 'Отменено продавцом',
			],
			'wb_status' => [
				'waiting' => 'в работе',
				'sorted' => 'отсортировано',
				'sold' => 'продано',
				'canceled' => 'отмена',
				'canceled_by_client' => 'отказ покупателем',
				'declined_by_client' => 'покупатель отменил заказ',
				'defect' => 'брак',
				'ready_for_pickup' => 'прибыл на ПВЗ',
			],
		];
		
		/** @var array
		 *	wb_status				=> wildb_settings with orderAction 
		 */
		private static $wb_orders_states = [
			'supplierStatus' => [
				'confirm'				=> 'orders_shop_confirm',
				'complete'				=> 'orders_shop_complete',
			],
			'wbStatus' => [
				'sold'					=> 'orders_shop_sold',
				'canceled_by_client'	=> 'orders_shop_cancel',
				'declined_by_client'	=> 'orders_shop_cancel',
			],
		];
		
		public static function getStickers(array $orders){
			
			$orders = array_map(fn($elem): int => (int) $elem, $orders);
			$stickers = [];
			
			if($result = shopWildbApiMarketplace::getInstance()->getOrderSticker(['orders' => $orders])){
				
				foreach ($result['stickers'] as $sticker){
					$stickers[$sticker['orderId']] = $sticker;
				}
				
				return $stickers;
			}else{
				waLog::log('Ошибка получения этикетки с WB', shopWildbPlugin::SLUG . '/wbOrders.log');
				return false;
			}
		}
		
		public static function getStickersByStatus(array $statuses){
			
			$model = new waModel;
			
			$wb_orders = $model->query("
				select o.id, o.state_id, op.value as wb_order_id from shop_order o
				join shop_order_params op on op.order_id = o.id and op.name = 'wb_order_id'
				where o.state_id in (s:statuses) and op.value is not null 
				limit 100
			", ['statuses' => $statuses])->fetchAll('wb_order_id');
			
			$labels = shopWildbApiMarketplace::getInstance()->getOrderSticker(['orders' => array_keys($wb_orders)]);
			$stickers = [];
			
			if($labels){
				foreach ($labels['stickers'] as $label){
					$order = $wb_orders[$label['orderId']];
					$stickers[$order['id']] = $label['file'];
				}
			}
			
			return $stickers;
		}
		
		public static function getStatuses(string $key = null){
			return $key && isset(self::$states[$key]) ? self::$states[$key] : self::$states;
		}
		
		public static function getWbStatuses(string $key = null){
			return $key && isset(self::$wb_orders_states[$key]) ? self::$wb_orders_states[$key] : self::$wb_orders_states;
		}
		
		public static function addOrderSupply($order_id, $supply_id){
			
			if($supply_id){
				if(shopWildbApiMarketplace::getInstance()->setSupply($supply_id,$order_id)){
					self::insertOrderLog($order_id,'<i class="icon16 wb"></i> Заказ <b>' . $order_id . '</b> успешно добавлен к поставке <b>' . $supply_id . '</b>');
					self::updateOrderStatus($order_id, ['seller_status' => 'confirm', 'wb_status' => 'waiting', 'supply_id' => $supply_id]);
				}
			}
		}
		
		public static function addOrderTrbx($orders, $supply_id, $trbx_id){
			
			if($supply_id && $trbx_id){
				if(shopWildbApiMarketplace::getInstance()->addOrderTrbx($supply_id, $trbx_id, $orders)){
					foreach ($orders as $order){
						self::insertOrderLog($order,'<i class="icon16 wb"></i> Заказ <b>' . $order . '</b> успешно добавлен в короб <b>' . $trbx_id . '</b>');
					}
					
					$orders_model = new shopWildbOrdersModel;
					$orders_model->updateById($orders, ['trbx_id' => $trbx_id]);
				}
			}
		}
		
		public static function removeFromTrbx($order_id){
			
			$orders_model = new shopWildbOrdersModel;
			$order = $orders_model->getById($order_id);
			
			if($order && shopWildbApiMarketplace::getInstance()->removeFromTrbx($order['supply_id'], $order['trbx_id'], $order['id'])){
				$orders_model->updateById($order['id'],['trbx_id' => null]);
				self::insertOrderLog($order['id'],'<i class="icon16 wb"></i> Заказ <b>' . $order['id'] . '</b> удален из короба <b>' . $order['trbx_id'] . '</b>');
			}
		}
		
		public static function canselOrder($order_id){
			if(shopWildbApiMarketplace::getInstance()->canselOrder($order_id)){
				$text = '<i class="icon16 wb"></i> Заказ <b>' . $order['params']['wb_order_id'] . '</b> переведен в статус <b>cansel</b> (&laquo;Отменено продавцом&raquo;)';
				self::insertOrderLog($order_id,$text);
			}
		}
		
		public static function insertOrderLog(int $wb_order_id, string $text){
			
			$orders_model = new shopOrderParamsModel;
			$order = $orders_model->getByField(['name' => 'wb_order_id', 'value' => $wb_order_id]);
			
			if($order){
				$orderLog = new shopOrderLogModel();
				$orderLog->insert([
				  'order_id' => $order['order_id'],
				  'action_id' => '',
				  'datetime' => date('Y-m-d H:i:s'),
				  'before_state_id' => '',
				  'after_state_id' => '',
				  'text' => $text,
				]);
			}
		}
		
		public static function updateOrderStatus(int $order_id, array $data){
			
			$data['date_status'] = date('Y-m-d H:i:s');
			
			$orders_model = new shopWildbOrdersModel;
			$orders_model->updateById($order_id, $data);
		}
		
		public static function getOrderInfoSection(array $params){
			
			$model = new shopWildbOrdersModel;
			$params['wb_order'] = $model->getByField('shop_order_id',$params['id']);
			
			if($params['wb_order']){
				
				$suuplies_model = new shopWildbSuppliesModel;
				
				$params['seller_status'] = shopWildbOrders::getStatuses('seller_status')[$params['wb_order']['seller_status']];
				$params['wb_status'] = shopWildbOrders::getStatuses('wb_status')[$params['wb_order']['wb_status']];
				$params['wb_supply'] = $suuplies_model->getById($params['wb_order']['supply_id']);
			}
			
			$view = wa('shop')->getView();
			
			return $view->renderTemplate(wa('shop')->getAppPath("plugins/" . shopWildbPlugin::PLUGIN_ID . "/templates/backendOrder.html"), $params);
		}
	}