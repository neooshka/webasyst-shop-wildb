<?php
	
	class shopWildbSupplies {
		
		public static function getDateSupply(): DateTime {
			
			$plugin = wa('shop')->getPlugin(shopWildbPlugin::PLUGIN_ID);
			
			$date = new DateTime();
			$time = ifempty($plugin->getSettings('time_to_create_new_supply'),'08:00');
			
			if($date > (new DateTime(date("Y-m-d {$time}")))) $date->modify('+1 day');
			
			return $date;
		}
		
		public static function createAutoSupply(DateTime $date_supply) {
			return self::createSupply('Поставка на ' . $date_supply->format('d.m.Y'), $date_supply);
		}
		
		public static function createSupply(string $name, DateTime $date_supply = null){
			
			if(!$name) return false;

			$supply = shopWildbApiMarketplace::getInstance()->addSupply($name);
			
			if($supply) {
				
				$data = [
					'id' => $supply['id'],
					'name' => $name,
					'done' => 0,
				];
				
				if($date_supply) $data['date'] = $date_supply->format('Y-m-d');
				
				$supplies_model = new shopWildbSuppliesModel();
				$supplies_model->insert($data);
				
				return $supply['id'];
			}
			
			return false;
		}
		
		public static function deleteSupply(string $supply_id){
			
			if(!$supply_id) return false;

			if(shopWildbApiMarketplace::getInstance()->deleteSupply($supply_id)) {
				
				$supplies_model = new shopWildbSuppliesModel();
				$supplies_model->deleteById($supply_id);
				
				return true;
			}
			
			return false;
		}
		
		public static function completeSupply(string $supply_id){
			
			if(!$supply_id) return false;

			if(shopWildbApiMarketplace::getInstance()->completeSupply($supply_id)) {
				
				$supplies_model = new shopWildbSuppliesModel();
				$supplies_model->updateById($supply_id, ['done' => 1, 'closedAt' => date('Y-m-d H:i:s')]);
				
				return true;
			}
			
			return false;
		}
		
		public static function getSticker($supply_id){
			
			if($sticker = shopWildbApiMarketplace::getInstance()->getSupplySticker($supply_id)){
				return $sticker;
			}else{
				waLog::log('Ошибка получения этикетки с WB', shopWildbPlugin::SLUG . '/wbSupplies.log');
				return false;
			}
		}
	}