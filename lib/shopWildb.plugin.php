<?php

	class shopWildbPlugin extends shopPlugin {
		
		const PLUGIN_ID = 'wildb';
		const SLUG = 'shop/plugins/wildb';
		const NO_LICENSE = 0;
		const LICENSE_STANDART = 1;
		const LICENSE_PREMIUM = 2;
		
		private static $price_plugins = [
			shopWildbPricePluginPrice::class,
			shopWildbPricePluginPricex::class,
		];
		
		public function saveSettings($settings = []){
			
			if(isset($settings['images_Limit'])){
				if($settings['images_Limit'] > 30) $settings['images_Limit'] = 30;
				if($settings['images_Limit'] < 1) $settings['images_Limit'] = 1;
			}
			
			$settings = array_merge($this->getSettings(),$settings);

			parent::saveSettings($settings);
		}
		
		public static function nocache($file){
			
			$file_path = $_SERVER['DOCUMENT_ROOT'] . $file;
			
			if(file_exists($file_path)) {
				return $file . "?nocache=" . md5_file($file_path);
			}else{
				return $file;
			}
		}
		
		public static function chkLcs(){
			
			if (class_exists('waLicensing')) {
				if (waLicensing::check(self::SLUG)->hasLicense() and waLicensing::check(self::SLUG)->hasPremiumLicense()) {
					return self::LICENSE_PREMIUM;
				} else if (waLicensing::check(self::SLUG)->hasLicense() and waLicensing::check(self::SLUG)->hasStandardLicense()) {
					return self::LICENSE_STANDART;
				} else if (waLicensing::check(self::SLUG)->isPremium() and waLicensing::check(self::SLUG)->isStandard()) {
					return self::NO_LICENSE;
				} else {
					return self::NO_LICENSE;
				}
			} else {
				return self::NO_LICENSE;
			}
		}
		
		public function backendOrders(){
			if(shopWildbPlugin::chkLcs() === shopWildbPlugin::LICENSE_PREMIUM){
				return ['sidebar_section' => '<div class="wb_assembly_sidebar_block"><a class="wb_assembly_sidebar_link js-wb-assembly">Сборочные задания</a></div>'];
			}
		}
		
		public function backendOrder($params){
			
			if(!empty($params['params']['wb_order_id'])){
				return ['info_section' => shopWildbOrders::getOrderInfoSection($params)];
			}
		}
		
		public function backendMenu() {
			$this->addCss('css/product.css');
			$this->addCss('css/assembly.css');
			$this->addCss('css/orders.css');
			$this->addCss('css/settings.css');
			$this->addJs('js/orders.js');
			$this->addJs('js/assembly.js');
		}
		
		public function orderActions($params = []) {
			
			if (!empty($params['order_id']) && !empty($params['action_id']) && $params['action_id'] != 'edit') {
				
				$order = new shopOrder($params['order_id']);
				
				if(!empty($order['params']['wb_order_id'])) {
					
					$settings = $this->getSettings();
					$supplies_model = new shopWildbSuppliesModel();
					
					if (!empty($settings['orders_wb_confirm']) && in_array($params['action_id'], $settings['orders_wb_confirm'])) {
						// Отправляем заказ в поставку
						$date_supply = shopWildbSupplies::getDateSupply();
						$supply_id = $supplies_model->getSupplyIDByDate($date_supply);
						
						if(!$supply_id){
							$supply_id = shopWildbSupplies::createAutoSupply($date_supply);
						}
						
						shopWildbOrders::addOrderSupply($order['params']['wb_order_id'], $supply_id);
					}
					
					if (!empty($settings['orders_wb_cancel']) && in_array($params['action_id'], $settings['orders_wb_cancel'])) {
						// Отмена заказа
						shopWildbOrders::canselOrder($order['params']['wb_order_id']);
					}
				}
			}
		}
		
		public function backendProduct($product){
			
			$model = new shopWildbProductsModel();
			$wb_products = $model->getByField("product_id", $product->getID(), true);
			
			if($wb_products){
				
				$return['toolbar_section'] = "<div class='shopWildbPlugin-backend-product'>Карточка на ВБ:</div>";
				
				foreach ($wb_products as $wb_product){
					$return['toolbar_section'] .= "<div class='shopWildbPlugin-info'>";
					$return['toolbar_section'] .= "<div class='shopWildbPlugin-section'><span>nmID</span><span>";
					$return['toolbar_section'] .= "	<a class='wb-link' title='Посмотреть карточку на WB' target='_blank' href='https://www.wildberries.ru/catalog/{$wb_product['nmID']}/detail.aspx'>";
					$return['toolbar_section'] .= 		$wb_product['nmID'];
					$return['toolbar_section'] .= "	</a>";
					$return['toolbar_section'] .= "</span></div>";
					$return['toolbar_section'] .= "<div class='shopWildbPlugin-section'><span>vendorCode</span><span>{$wb_product['vendorCode']}</span></div>";
					$return['toolbar_section'] .= "</div>";
				}
					
				return $return;
			}
		}
		
		public static function getPricePlugins(): array {
			return static::$price_plugins;
		}
	}