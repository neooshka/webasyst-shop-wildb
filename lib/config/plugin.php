<?php

	return [
		'name' => 'Интеграция с Wildberries',
		'description' => 'Плагин интеграции с маркетплейсом Wildberries',
		'version' => '1.3.1',
		'img' => 'img/wb96.png',
		'vendor' => '1267443',
		'handlers' => [
			'backend_product' => 'backendProduct',
			'backend_order' => 'backendOrder',
			'backend_orders' => 'backendOrders',
			'backend_menu' => 'backendMenu',
			'order_action.*' => 'orderActions',
		],
		'custom_settings' => true,
	];
